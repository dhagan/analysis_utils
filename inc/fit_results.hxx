
#include "common.hxx"

#ifndef fit_results_hxx
#define fit_results_hxx

#include "bound_mgr.hxx"
#include "bound.hxx"
#include <ROOT/RDataFrame.hxx>
#include <RooFitResult.h>

// write a constructor for this that takes the roo fit result etc
extern "C" class fit_results : public TObject {

  public:
    fit_results(){};
    ~fit_results(){};
    double mu_value, mu_error;
    double spp_value, spp_error;
    double sign, sign_error;
    double bckg, bckg_error;
    double data, data_error;
    double sign_fit, sign_fit_error;
    double bckg_fit, bckg_fit_error;
    double data_fit, data_fit_error;
    double rho, chi2_v1, chi2_v2;
    int ndof;
    std::vector< double > sign_weight;
    std::vector< double > sign_weight_error;
    std::vector< double > bckg_weight;
    std::vector< double > bckg_weight_error;

  

    fit_results( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, bool trex=false ){

      char pp_name[10];
      if ( trex ){ sprintf( pp_name, "spp" ); } 
      else { sprintf( pp_name, "ScalingPP" ); }

      // Extract constants
      RooRealVar * mu = ( RooRealVar *) results->floatParsFinal().find( "mu" );
      RooRealVar * pp = ( RooRealVar *) results->floatParsFinal().find( pp_name );

      TH1F * sign_scaled = (TH1F *) sign->Clone( "sign_scaled" );
      TH1F * bckg_scaled = (TH1F *) bckg->Clone( "bckg_scaled" );
      sign_scaled->Scale( mu->getVal() );
      bckg_scaled->Scale( pp->getVal() );
      TH1F * fit = (TH1F *) sign->Clone( "fit" );
      fit->Reset();
      fit->Add( sign_scaled, bckg_scaled );

      // yield and error calculation
      this->mu_value        = mu->getVal();  //b
      this->mu_error        = mu->getError();
      this->spp_value       = pp->getVal();
      this->spp_error       = pp->getError();
      this->sign            = sign->Integral(); //a
      this->sign_error      = sqrt( sign->GetSumOfWeights() ); // this is already squared
      this->bckg            = bckg->Integral();
      this->bckg_error      = sqrt( bckg->GetSumOfWeights() );
      this->data            = data->Integral();
      this->data_error      = sqrt( data->GetSumOfWeights() );
      this->sign_fit        = sign->Integral() * this->mu_value; //y
      this->sign_fit_error  = this->sign_fit * sqrt( (( this->sign_error * this->sign_error)/(this->sign * this->sign))
                             + (( this->mu_error * this->mu_error)/( this->mu_value * this->mu_value)) );
      bckg_fit        = bckg->Integral() * this->spp_value;
      bckg_fit_error  = this->bckg_fit * sqrt( (( this->bckg_error * this->bckg_error)/( this->bckg * this->bckg ))
                             + (( this->spp_error * this->spp_error)/( this->spp_value * this->spp_value)) );
      data_fit        = this->sign_fit + this->bckg_fit;
      rho             = results->correlation( "mu", pp_name );
      this->chi2_v1 = 0;
      this->chi2_v2 = 0;
      this->ndof = 0;
    
      hist_group in_hists = hist_group( sign, bckg, data );
      hist_group fit_hists = hist_group( sign_scaled, bckg_scaled, fit );
      // chi2 calculation
      chi2_calc( in_hists, fit_hists, this->chi2_v1, this->chi2_v2, this->ndof );
    }

    void chi2_calc( hist_group & in_hists, hist_group & fit_hists, double & chi_squared_version1, double & chi_squared_version2, int & degrees_of_freedom ){

      TH1F * sign = in_hists.sign_hist;
      TH1F * bckg = in_hists.bckg_hist;
      TH1F * data = in_hists.data_hist;
      TH1F * sign_scaled = fit_hists.sign_hist;
      TH1F * bckg_scaled = fit_hists.bckg_hist;
      TH1F * fit = fit_hists.data_hist; 

      for (int bin = 1; bin <= data->GetNbinsX(); bin++){

        double sign_bin_count = sign->GetBinContent(bin);
        double bckg_bin_count = bckg->GetBinContent(bin);
        double sign_scaled_bin_count = sign_scaled->GetBinContent(bin);
        double bckg_scaled_bin_count = bckg_scaled->GetBinContent(bin);
        double sign_scaled_bin_error = sign_scaled_bin_count * std::sqrt(sign_bin_count / (sign_bin_count * sign_bin_count) + (mu_error * mu_error) / (mu_value * mu_value));
        double bckg_scaled_bin_error = bckg_scaled_bin_count * std::sqrt(bckg_bin_count / (bckg_bin_count * bckg_bin_count) + (spp_error * spp_error) / (spp_value * spp_value));

        double fit_bin_count = fit->GetBinContent(bin);
        double fit_bin_error = sqrt(sign_scaled_bin_error * sign_scaled_bin_error + bckg_scaled_bin_error * bckg_scaled_bin_error);

        double sign_weight_calc = (fit_bin_count != 0) ? sign_scaled_bin_count / fit_bin_count : 0;
        double bckg_weight_calc = (fit_bin_count != 0) ? bckg_scaled_bin_count / fit_bin_count : 0;

        double sign_weight_error_calc = sign_weight_calc * std::sqrt((sign_scaled_bin_error * sign_scaled_bin_error) / (sign_scaled_bin_count * sign_scaled_bin_count) + (fit_bin_error * fit_bin_error) / (fit_bin_count * fit_bin_count));
        double bckg_weight_error_calc = bckg_weight_calc * std::sqrt((bckg_scaled_bin_error * bckg_scaled_bin_error) / (bckg_scaled_bin_count * bckg_scaled_bin_count) + (fit_bin_error * fit_bin_error) / (fit_bin_count * fit_bin_count));

        sign_weight.push_back(sign_weight_calc);
        sign_weight_error.push_back(sign_weight_error_calc);
        bckg_weight.push_back(bckg_weight_calc);
        bckg_weight_error.push_back(bckg_weight_error_calc);

        double NSig_Events = sign->GetBinContent(bin);
        double NPP_Events = bckg->GetBinContent(bin);
        double NData_Events = data->GetBinContent(bin);
        double NSig_Error = sign->GetBinError(bin);
        double NPP_Error = bckg->GetBinError(bin);
        double NData_Error = data->GetBinError(bin);
        if ((NSig_Events - NSig_Error <= 0) || (NPP_Events - NPP_Error <= 0) || (NData_Events - NData_Error <= 0))
          continue;
        double NSig_Events2 = NSig_Events * NSig_Events;
        double NPP_Events2 = NPP_Events * NPP_Events;
        double NSig_Events_Error2 = NSig_Error * NSig_Error;
        double Npp_Events_Error2 = NPP_Error * NPP_Error;
        double fittedData = this->mu_value * NSig_Events + this->spp_value * NPP_Events;
        double err_fittedData_j = (NPP_Events2 * (this->mu_error * this->mu_error)) + (NSig_Events2 * (this->spp_error * this->spp_error)) + ((this->mu_value * this->mu_value) * NSig_Events_Error2) + ((this->spp_value * this->spp_value) * Npp_Events_Error2) + (2 * NSig_Events * NPP_Events * this->mu_error * this->spp_error * this->rho);
        double numerator_v2 = (NData_Events - fittedData) * (NData_Events - fittedData);
        double denominator_v2 = (NData_Error * NData_Error) + (err_fittedData_j); // err_fittedData_i is already squard!
        double numerator_v1 = (NData_Events - fittedData) * (NData_Events - fittedData);
        double denominator_v1 = NData_Error * NData_Error;
        double chi2_v1_j = numerator_v1 / denominator_v1;
        chi_squared_version1 += chi2_v1_j;
        double chi2_v2_j = numerator_v2 / denominator_v2;
        chi_squared_version2 += chi2_v2_j;

        if (data->GetBinContent(bin) > 0){ degrees_of_freedom++; }
      }
      degrees_of_freedom -= 2;
    }

    fit_results( RooFitResult * results, hist_group hists, bool trex=false) : 
      fit_results( results, hists.sign_hist, hists.bckg_hist, hists.data_hist, trex )
    {};

    hist_group make_fit_hists( TH1F * sign, TH1F * bckg, double mu, double spp, const std::string & name="" ){

      TH1F * sign_scaled = (TH1F *) sign->Clone( "sign_scaled" );
      TH1F * bckg_scaled = (TH1F *) bckg->Clone( "bckg_scaled" );
      sign_scaled->Scale( mu );
      bckg_scaled->Scale( spp );
      TH1F * fit = (TH1F *) sign_scaled->Clone( name.c_str() );
      fit->Reset();
      fit->Add( sign_scaled, bckg_scaled );
      return hist_group( sign_scaled, bckg_scaled, fit );
    }

    fit_results( RooFitResult * results, hist_group sr_hists, hist_group cr_hists ){

      // Extract constants
      RooRealVar * mu = ( RooRealVar *) results->floatParsFinal().find( "mu" );
      RooRealVar * pp = ( RooRealVar *) results->floatParsFinal().find( "spp" );
      hist_group sr_fit = make_fit_hists( sr_hists.sign_hist, sr_hists.bckg_hist, mu->getVal(), pp->getVal(), "fit_sr" );
      hist_group cr_fit = make_fit_hists( cr_hists.sign_hist, cr_hists.bckg_hist, mu->getVal(), pp->getVal(), "fit_cr" );

      this->mu_value        = mu->getVal();  //b
      this->mu_error        = mu->getError();
      this->spp_value       = pp->getVal();
      this->spp_error       = pp->getError();
      this->rho = results->correlation( "mu", "spp" );

      this->sign            = sr_hists.sign_hist->Integral() + cr_hists.sign_hist->Integral();
      this->sign_error      = this->sign * std::sqrt( 
                                  sr_hists.sign_hist->GetSumOfWeights()/( sr_hists.sign_hist->Integral()*sr_hists.sign_hist->Integral() ) 
                                + cr_hists.sign_hist->GetSumOfWeights()/( cr_hists.sign_hist->Integral()*cr_hists.sign_hist->Integral() )
                              );
      this->bckg            = sr_hists.bckg_hist->Integral() + cr_hists.bckg_hist->Integral();
      this->bckg_error      = this->bckg * std::sqrt( 
                                  sr_hists.bckg_hist->GetSumOfWeights()/( sr_hists.bckg_hist->Integral()*sr_hists.bckg_hist->Integral() ) 
                                + cr_hists.bckg_hist->GetSumOfWeights()/( cr_hists.bckg_hist->Integral()*cr_hists.bckg_hist->Integral() ) 
                              );
      this->data            = sr_hists.data_hist->Integral() + cr_hists.data_hist->Integral();
      this->data_error      = this->data * sqrt( 
                                  sr_hists.data_hist->GetSumOfWeights()/(sr_hists.data_hist->Integral()*sr_hists.data_hist->Integral() )
                                + cr_hists.data_hist->GetSumOfWeights()/(cr_hists.data_hist->Integral()*cr_hists.data_hist->Integral() ) 
                              );

      this->sign_fit        = this->sign * this->mu_value; 
      this->sign_fit_error  = this->sign_fit * sqrt( 
                                ( ( this->sign_error * this->sign_error)/(this->sign * this->sign) )
                              + ( ( this->mu_error * this->mu_error)/( this->mu_value * this->mu_value) )
                              );

      this->bckg_fit        = this->bckg * this->spp_value; 
      this->bckg_fit_error  = this->bckg_fit * sqrt( 
                                  ( ( this->bckg_error * this->bckg_error)/( this->bckg * this->bckg ) )
                                + ( ( this->spp_error * this->spp_error)/( this->spp_value * this->spp_value)) );

      this->data_fit        = this->sign_fit + this->bckg_fit;
      this->data_fit_error  = this->data_fit * sqrt( 
                                  ( ( this->sign_fit_error * this->sign_fit_error )/( this->sign_fit * this->sign_fit ) )
                                + ( ( this->bckg_fit_error * this->bckg_fit_error )/( this->bckg_fit * this->bckg_fit ) )
                              );
      
      double sr_chi2_v1 = 0;
      double sr_chi2_v2 = 0;
      int sr_dof = 0;
      double cr_chi2_v1 = 0;
      double cr_chi2_v2 = 0;
      int cr_dof = 0;

      chi2_calc( sr_hists, sr_fit, sr_chi2_v1, sr_chi2_v2, sr_dof );
      chi2_calc( cr_hists, cr_fit, cr_chi2_v1, cr_chi2_v2, cr_dof );

      this->chi2_v1 = sr_chi2_v1 + cr_chi2_v1;
      this->chi2_v2 = sr_chi2_v2 + cr_chi2_v2;
      this->ndof = sr_dof + cr_dof;

    }

    ClassDef( fit_results, 4 )
};

#endif
