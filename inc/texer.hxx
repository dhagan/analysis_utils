
#ifndef texer_hxx
#define texer_hxx

#include "common.hxx"
#include "process_utils.hxx"

extern "C" class texer {

  private:

    bool landscape, initialised, table_mode;
    std::string filename;
    std::vector< std::string > lines, packages;
    std::ofstream output_stream;


  public:

    texer(){}
    texer( std::string & filename, bool landscape = false ){
      this->filename = filename;
      if ( filename.find( ".tex" ) != std::string::npos){ this->filename += ".tex"; }
      this->landscape = landscape;
    }

    void set_filename( std::string & filename );
    void set_orientation( bool landscape = false );
    void set_packages( std::vector< std::string > & packages );
    void set_packages( std::vector< std::string > && packages );
    void add_line( std::string & line );
    void add_line( std::string && line );
    void start_table( std::string & table_format, std::vector< std::string > & columns );
    void add_row( std::string & row_string );
    void end_table();
    void add_table( std::string & table_layout, std::string & entry_format, std::vector< TH1 * > & columns );
    void add_table( std::string & table_format, std::string & entry_format, std::vector< std::vector< double > > & values, std::vector< std::string> & columns );
    void initialise();
    void write( bool reset = true );
    void clear();

};

#endif
