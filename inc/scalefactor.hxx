
#include "common.hxx"
#include <fileset.hxx>

#ifndef scalefactor_hxx
#define scalefactor_hxx

enum dataset { Data15, Data16, Data17, Data18, MC16a, MC16d, MC16e };
enum single_mu_leg { mu4, mu6, mu4_nomucomb, mu6_nomucomb };
enum dimu_trig { bDimu, bDimu_noL2 };
enum eta_range { Barrel, Overlap, EndCap };

static const std::vector< const char * > dataset_c = { "Data15", "Data16", "Data17", "Data18", "MC16a", "MC16d", "MC16e" };
static const std::vector< const char * > single_mu_leg_c = { "mu4", "mu6", "mu4_nomucomb", "mu6_nomucomb" };
static const std::vector< const char * > dimu_trig_c = { "bDimu", "bDimu_noL2" };
static const std::vector< const char * > eta_range_c = { "Barrel", "Overlap", "EndCap" };


class scalefactor {

  int get_bin(double pt, double eta ) const;
  

  TFile * m_fii_id_tight_uncov = nullptr;
  TH2 * m_h_id_tight_uncov   = nullptr;

  double m_ptmin = 10e3, m_ptmax=2e6;
  double m_etamin = 0. , m_etamax = 2.37;
  double m_crackmin = 1.37, m_crackmax = 1.52;
  
  double m_error_bloom = 1.0;
  double m_bloom_ptmin = 10e3, m_bloom_ptmax=2e6;
  double m_bloom_etamin = 0. , m_bloom_etamax = 2.37;

  double m_error_scaling = 0.0;
  double m_central_scaling = 0.0;

  std::string m_singleMuEffFileName;
  std::string m_drCorFileName;
  std::map< dataset, std::map< single_mu_leg, TH2F* > > singleMuSfMap;
  std::map< dataset, std::map< single_mu_leg, TH2F* > > singleMuSfErrMap;
  std::map< dataset, std::map< dimu_trig, std::map< eta_range, TF1 * > > > dimuDrCorMap;
  std::map< dataset, std::map< dimu_trig, std::map< eta_range, TH1F * > > > dimuDrCorMap_err;


  public:

    scalefactor(){};

    double bounds_bloom( double pt, double eta ) const;
    double get_scaling( double pt  ) const;
    double get_central_scaling( double pt  ) const;
    void load_muon_SF( const std::string & singleMuEffFileName="singleMu_SF_data1516_etabins.root", const std::string & drCorFileName = "c_dr_fixed.root" );
    void load_photon_SF( const std::string & photon_efficiency_sf_path="" );
    void set_bounds_error_bloom( double bloom );
    void set_bloom_bounds( double pt_min=10e3, double pt_max=2e6, double eta_min=0.0, double eta_max=2.37 );
    void set_linear_scaling( double scaling );
    void set_central_linear_scaling( double scaling );

    double photon_SF( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_linear( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_upper( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_lower( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_upper_linear( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_lower_linear( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_upper_linear_central( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_lower_linear_central( double pt, double eta ) const; // pt_in_MeV
    double photon_SF_uncertainty( double pt, double eta) const; // pt_in_MeV
    double photon_SF_uncertainty_linear( double pt, double eta) const; // pt_in_MeV
    

    double single_muon_SF( double pT, double qEta, dataset datasetData, single_mu_leg muleg );
    double single_muon_SF_error( double pT, double qEta, dataset datasetData, single_mu_leg muleg );
    double muon_dr_corr( double dR, double y, dataset dataset, dimu_trig dimutrig );
    double muon_dr_corr_error( double dR, double y, dataset dataset, dimu_trig dimutrig );
    double muon_dr_SF( double dR, double y, dataset ds, dimu_trig dimutrig );

    TH1F * error_photon_SF( TTree * predefined_tree, variable_set & variables, const std::string & variable, const std::string & mass );
    TH1F * error_muon_SF( TTree * predefined_tree, variable_set & variables, const std::string & variable, const std::string & mass );


};

#endif
