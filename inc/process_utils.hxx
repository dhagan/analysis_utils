
#include "common.hxx"

#ifndef process_utils_hxx
#define process_utils_hxx

#include "bound_mgr.hxx"
#include "bound.hxx"
#include <ROOT/RDataFrame.hxx>
#include <RooFitResult.h>

extern "C" struct hist_group{
  TH1F * sign_hist, * bckg_hist, * data_hist, * bbbg_hist;
  hist_group() : sign_hist(nullptr), bckg_hist(nullptr), data_hist(nullptr), bbbg_hist(nullptr){}
  hist_group( TH1F * sign, TH1F * bckg, TH1F * data ){
    sign_hist = sign;
    bckg_hist = bckg;
    data_hist = data;
    bbbg_hist = nullptr;
  }
  hist_group( bound & variable_bound ){
    sign_hist = variable_bound.get_hist();
    bckg_hist = variable_bound.get_hist();
    data_hist = variable_bound.get_hist();
  }
  ~hist_group(){};
  void erase(){ 
    if ( sign_hist != nullptr ){ delete sign_hist; sign_hist = nullptr; }
    if ( bckg_hist != nullptr ){ delete bckg_hist; bckg_hist = nullptr; }
    if ( data_hist != nullptr ){ delete data_hist; data_hist = nullptr; }
    if ( bbbg_hist != nullptr ){ delete bbbg_hist; bbbg_hist = nullptr; }
  }

};

extern "C" struct hist_group_2d{
  TH2F * sign_hist, * bckg_hist, * data_hist, * bbbg_hist;
};

extern "C" void split_strings(std::vector<std::string> & vec_split, const std::string & str_split, std::string delim );
hist_group combine_differential_fit( final_fileset & input, variable_set & variables );
extern "C" void recombine_spectator( TFile * ana_file, const int & ana_bins, const std::string & bin_name, const std::string & type, bool hf_step, TH1F * combined_hist );
extern "C" int type_to_num( const std::string & type );
extern "C" void abs_hist( TH1 * hist );
extern "C" TGraphAsymmErrors * err_graph_calc( TH1F * base, TH1F * sys, bool absolute=true );
extern "C" void make_hf_fit_histogram( char bin_name[150], char err_name[150], TFile * source_file, TH1F * reco_hist, int err_bin);
extern "C" void rc_hist( std::string hist_name, std::string err_name, TFile * source_file, TH1F * reco_hist, int err_bin);
extern "C" void combine_sys_group( std::vector< TH1F *> & group_systematics, TH1F * group_hist, TH1F * base );

extern "C" void align_sg( TF1 * sg_func, TH1F * hist, bool limit=false );
extern "C" void align_dg( TF1 * sg_func, TH1F * hist, bool limit=false );
extern "C" TF1 * prep_dg( float min = -100, float max = 100 );
extern "C" TF1 * prep_sg( float min = -100, float max = 100 );
extern "C" TF1 * prep_line( float min = -100, float max = 100 );
extern "C" void split_dg( TF1 * dg, TF1 * inner, TF1 * outer );
extern "C" void pos_hists( TH1F * data, TH1F * sign, TH1F * bckg, bool trex=false );
extern "C" void merge_tree( std::string filepath_1, std::string filepath_2, std::string & output_filepath );
extern "C" void combine_hists_in_files( std::string filepath_1, std::string filepath_2, std::string output_filepath, std::vector<std::string> skip );
extern "C++" std::vector< double > hist_to_vector( TH1 * hist );
extern "C++" TH1F * vector_to_hist( std::vector< double >, std::string && name="" );
extern "C++" TH1F * combine_hist_vector( std::vector <TH1F *> hist_vector, std::string name );

#endif
