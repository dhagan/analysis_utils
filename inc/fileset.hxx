#include "common.hxx"

#ifndef fileset_hxx
#define fileset_hxx

#include "bound_mgr.hxx"
#include "bound.hxx"
#include "variable_set.hxx"
#include "fit_results.hxx"
#include <ROOT/RDataFrame.hxx>
#include <RooFitResult.h>

enum fileset_type{
  none=-1, multi, predefined, final, subtraction, 
  selection, sf_muon, sf_photon, trex_res, trex, 
  stats, efficiency, fiducial
};

enum photon_sf_type{
  base=0, up=1, down=2
};

const static std::vector< const char * > sf_char = { "photon_id_sf", "photon_sf_upper", "photon_sf_lower" };

extern "C" class basic_fileset {

  std::string unique;
  const char * unique_c;
  
  void modify_type( fileset_type new_type );

  public:

    std::string filepath;
    TFile * file;
    TFile * sign_file, * bckg_file, * data_file, * bbbg_file;
    TTree * sign_tree, * bckg_tree, * data_tree, * bbbg_tree;

    TFile * efficiency_file;
    TTree * eff_reco_tree, * eff_truth_tree;
    TFile * weighted_file, * differential_file, * statistics_file;
    TTree * weighted_tree;

    TFile * predefined_file;
    TTree * predefined_tree;

    TFile * trex_file;
    TTree * trex_tree;

    fileset_type type;
    photon_sf_type sf_type;
    std::vector< fileset_type > multi_types;
    bool predefined;

    const char * trex_results_path;
    std::vector< std::string > trex_results_cuts, trex_results_dirs;
    
    bool typecheck( fileset_type check_type );
    void set_scalefactor_type( photon_sf_type sf_type ){ this->sf_type = sf_type; }
    const char * get_phot_sf_weight( ){ return sf_char[sf_type]; }

    void set_unique( const std::string & unique );
    const char * get_unique();

    void load_trex_results( const std::string & filepath, const std::string & unique, variable_set & variables );
    void process_trex_results( bool stat_only=false );
    //void process_trex_yields( const std::string & unique, variable_set & variables );

    void load_efficiency_fileset( const std::string & efficiency_path="", const std::string & unique="", bool fiducial=false );
    void load_final_fileset( std::string & fileset_path, std::string & efficiency_path, std::string & unique, bool efficiency_absolute=true );
    void load_subtraction_fileset( std::string & fileset_path, std::string & unique, bool split=false );
    void load_predefined_fileset( const std::string & predefined_filepath, const std::string & unique, fileset_type type ); 
    void load_stats_fileset( const std::string & filepath, const std::string & unique );
    void load_trex_fileset( const std::string & filepath, const std::string & unique );
    void load_trex_yield_fileset( const std::string & unique );


    void close_subtraction_fileset();
    void apply_sign_efficiency( TH1F * sign, variable_set & variables );
    void apply_scalefactor_correction( TH1F * sign, variable_set & variables, fileset_type sf_correction_type, const std::string & variable="" );
    TH1F * get_error( variable_set & variables, const std::string & variable );

    hist_group_2d recreate_map_histograms_weights( variable_set & variables, bool normalise=false, std::string x_variable="", std::string y_variable="" );
    hist_group_2d recreate_map_histograms_subtraction( variable_set & variables, bool normalise=false, std::string x_variable="", std::string y_variable="" );
    hist_group recreate_histograms_subtracted( variable_set & variables, bool efficiency_correct_signal=false, bool normalise=false, std::string variable="", bool scalefactor=false );
    hist_group recreate_histograms_weights( variable_set & variables, bool efficiency_correct_signal=false, bool normalise=false, std::string variable="" );
    hist_group recreate_histograms_differential( variable_set & variables, bool efficiency_correct_signal=false, bool normalise=false );
    hist_group recreate_histograms_statistical( variable_set & variables, bool efficiency_correct_signal=false, bool normalise=false );
    hist_group recreate_histograms_trex( variable_set & variables, bool efficiency_correct_signal=false, bool normalise=false );

    TH1F * get_efficiency( variable_set & variables, const std::string & extra_cut="" );
    std::vector< TH1F * > get_efficiency_hists( variable_set & variables, const std::string & extra_cut="" );
    TH1F * get_subtracted_data( variable_set & variables, bool normalise=false );
    TH1F * get_weighted_histogram( sample_type type, variable_set & variables, const std::string & variable="", bool efficiency_correct_signal=false, bool normalise="", const std::string & name="" );
    TH1F * get_subtracted_histogram( sample_type type, variable_set & variables, const std::string & variable="", bool efficiency_correct_signal=false, bool normalise="", const std::string & name="" );

    TTree * get_tree( sample_type type );
    TFile * get_file( sample_type type );

    hist_group recreate_prefit_histograms( variable_set & variables, int bin=0, const std::string & cut="", const std::string & weight="", region signal_region=SR);

    basic_fileset(){};
    basic_fileset( const std::string & unique ){ this->set_unique( unique ); };
    basic_fileset( const char * unique ){ this->set_unique( unique ); };

};

#endif
