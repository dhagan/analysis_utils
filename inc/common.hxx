// simple include header covering everything i usually need.

#ifndef common_hxx
#define common_hxx

#include "TSystem.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include "TROOT.h"

#include <RooFitResult.h>
#include <RooRealVar.h>

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>
#include <fstream>
#include <sstream>
#include <cassert>
#include <numeric>

#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

#include "nlohmann/json.hpp"

class bound;
class bound_mgr; 
class style;
class style_mgr;
class fit_results;
class stage_fileset; 
class variable_set;
class final_fileset; 
struct hist_group;

static const Int_t dark_purple    = TColor::GetColor( "#331832" );

static const Int_t vermillion     = TColor::GetColor( "#F0544F" );
static const Int_t ash_gray       = TColor::GetColor( "#C6D8D3" );
static const Int_t papaya_whip    = TColor::GetColor( "#FDF0D5" );
static const Int_t licac          = TColor::GetColor( "#BC9EC1" );
static const Int_t paynes_gray    = TColor::GetColor( "#596475" );
static const Int_t raisin_black   = TColor::GetColor( "#1F2232" );
static const Int_t african_violet = TColor::GetColor( "#A323BF" );

// this is your sequence
static const Int_t tyrian     = TColor::GetColor("#F3A712");
static const Int_t carmine    = TColor::GetColor("#9A031E");
static const Int_t rapsberry  = TColor::GetColor( "#D81E5B" );
static const Int_t violet     = TColor::GetColor("#A323BF");
static const Int_t tiffany    = TColor::GetColor("#75DDDD");


enum sample_type{
  other=-1, data=0, sign=1, bckg=2, bbbg=3 
};
static const std::vector< const char * > type_c{ "data", "sign", "bckg", "bbbg" };
static const std::vector< const char * > type_str{ "data", "sign", "bckg", "bbbg" };

enum region {
  NR=-1, SR=0, CR=1, NRA=2, SRA=3, CRA=4, NRB=5, SRB=6, CRB=7,  REGION_LAST
};
static const std::vector< const char * > region_c{   "sr", "cr", "nra", "sra", "cra", "nrb", "srb", "crb" };
static const std::vector< const char * > region_str{ "sr", "cr", "nra", "sra", "cra", "nrb", "srb", "crb" };

static const std::vector <const char * > region_cuts{ 
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.5*1.5) + ( (AbsdY)*(AbsdY) )/(2.5*2.5) ) < 1",
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.5*1.5) + ( (AbsdY)*(AbsdY) )/(2.5*2.5) ) > 1",
                "1.0",
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.2*1.2) + ( (AbsdY)*(AbsdY) )/(2.0*2.0) ) < 1",
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.2*1.2) + ( (AbsdY)*(AbsdY) )/(2.0*2.0) ) > 1",
                "1.0",
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.0*1.0) + ( (AbsdY)*(AbsdY) )/(1.5*1.5) ) < 1",
                "( ( abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.0*1.0) + ( (AbsdY)*(AbsdY) )/(1.5*1.5) ) > 1"
};

region region_convert( std::string input_region );
sample_type sample_type_convert( std::string input_type );
//regions.emplace( std::pair< fit_region, const char * >( sr1, "( abs(pi-AbsdPhi) < 1.5) &&(AbsdY < 2.5 )" ) ); 
//regions.emplace( std::pair< fit_region, const char * >( cr1, "( abs(pi-AbsdPhi) > 1.5) &&(AbsdY < 2.5 )" ) );
//regions.emplace( std::pair< fit_region, const char * >( cr2, "( abs(pi-AbsdPhi) < 1.5) &&(AbsdY > 2.5 )" ) );
//regions.emplace( std::pair< fit_region, const char * >( cr3, "( abs(pi-AbsdPhi) > 1.5) &&(AbsdY > 2.5 )" ) );



#endif
