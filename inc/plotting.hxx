
#include "common.hxx"

#ifndef plot_utils_hxx
#define plot_utils_hxx

#include "bound.hxx"
#include "bound_mgr.hxx"

// plotting stuff to be plotting namespaced
  extern "C" void hist_prep_axes( TH1 * hist, bool zero=false, bool centre=false, double centre_val=0 );
  extern "C" void hist_prep_text( TH1 * hist );
  extern "C" void error_prep_axes( TGraphAsymmErrors * err );
  extern "C" void set_err_axis_labels( TGraphAsymmErrors * err, std::string xaxis, std::string yaxis );
  extern "C" void set_2d_axis_labels( TH2F * hist, std::string xaxis, std::string yaxis );
  extern "C" TLegend * below_logo_legend();
  extern "C" TPaveStats * make_stats( TH1 * hist, bool small=false, bool shifted=false );
  extern "C" TPaveStats * make_ratio_stats( TH1 * hist, bool small=false, bool shifted=false );
  extern "C" void add_pad_title( TPad * active_pad, std::string title, bool sci_axis=true );
  extern "C" void add_atlas_decorations( TPad * active_pad, bool wip=true, bool sim=false );
  extern "C" void add_internal( TPad * active_pad );
  extern "C" void add_simulation( TPad * active_pad );
  extern "C" TLegend * create_atlas_legend();
  extern "C" void style_func( TF1 * func, std::vector<float> & style_vec );
  extern "C" TLegend * create_stat_legend();
  extern "C" void set_axis_labels( TH1 * hist, std::string xaxis, std::string yaxis );
  extern "C" void style_hist( TH1 * hist, std::vector< float > & style_vec );

extern "C" TH1F * errorbar_to_hist( TH1F * hist, bool absolute=true );
extern "C" TH1F * hist_to_errorbar( TH1F * base, TH1F * err_hist, bool absolute=true );
extern "C" TH1F * quadrature_error_combination( TH1F * stat, std::vector<TH1F *> systematic, bool sys_only=false);
extern "C" TH1F * single_sys_to_error( TH1F * base, TH1F * sys );
extern "C" TGraphAsymmErrors * diff_graph( TH1F * base, TH1F * sys, bool absolute=true );
extern "C" TH1F * sys_to_error_hist( TH1F * base, TH1F * sys, bool absolute=true );
extern "C" void hist_limits( TH1 * hist, double && min, double && max );
extern "C" TH1F * ratio_pad( TH1 * numerator, TH1 * denominator, TPad * pad);
extern "C" TPaveText * pseudostats( TH1 * hist, bool small=false, bool shifted=false );

//extern "C" void compare_systematic( TH1F * base, TH1F * sys, const std::string & var, const std::string & unique, std::string & selection_path );


#endif
