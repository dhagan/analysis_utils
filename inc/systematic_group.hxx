#include "common.hxx"

#ifndef systematic_group_hxx
#define systematic_group_hxx

#include "bound_mgr.hxx"
#include "bound.hxx"
#include "variable_set.hxx"
#include "fit_results.hxx"
#include "fileset.hxx"
#include <ROOT/RDataFrame.hxx>
#include <RooFitResult.h>


class systematic_group {

  public:
    std::vector< basic_fileset * > systematics_files;
    std::vector< TH1F * > systematics_histograms;
    TH1F * grouped_systematic;
    std::vector< std::string > systematic_names;
    std::string group_name;
    bool group, single, predefined;
  
  systematic_group(){ group_name = ""; single = false; predefined = false; group = true; }
  systematic_group( std::string group_name ){ this->group_name = group_name; }
  void set_single(){ this->single = true; this->predefined = false; this->group = false;}
  void set_predefined(){ this->predefined = true; this->single = false; this->group = false; }
  void set_group(){ this->group = true; this->predefined = false; this->single = false; }
  bool valid_state(){ return ( (this->group + this->predefined + this->single) == 1); } 
  TH1F * get_group_hist(){ return this->grouped_systematic; }

  void add_file( std::string & filepath, std::string unique, fileset_type ftype ){
    basic_fileset * fileset = new basic_fileset();
    fileset->load_predefined_fileset( filepath, unique, ftype );
    systematics_files.push_back( fileset );
    systematic_names.push_back( unique );
  }

  void add_fileset( std::string & fileset_path, std::string & efficiency_path, std::string & unique );
  void prepare_systematics( variable_set & variables, bool differential=true );
  TH1F * evaluate_systematic( variable_set & variables, TH1F * baseline_histogram );
  TH1F * evaluate_single_systematic( TH1F * baseline_histogram );
  TH1F * evaluate_predefined_systematic( variable_set & variables, TH1F * baseline_histogram );
  TH1F * merge_systematic_group( variable_set & variables, TH1F * baseline_histogram );
  

};

#endif
