

#ifndef bound_hxx
#define bound_hxx

#include "common.hxx"

enum bound_limited {
  lower = 0, upper = 1, both = 2
};

// class to contain variables for defining variable bounds in jpsi+gamma analysis
// 'limited' defines if a get_cut() call returns a string that has the cut in it
// by default the get_hist and get_2d_hist will return 
extern "C" class bound {

  public:
    int bins;
    double min, max;
    std::string name, var, ltx, units; 
    bound_limited limited;

    bound(){
      this->name  = "";
      this->var   = "";
      this->bins  = -99;
      this->min   = 0;
      this->max   = 0;
      this->units = "";
      this->ltx   = "";
    }

    bound( std::string name ){
      this->name = name;
      this->var = name;
      this->bins = -99;
      this->min = 0;
      this->max = 0;
      this->units = "";
      this->ltx = "";
    };

    bound( std::string name, std::string var, int bins, double min, double max ){
      this->name = name;
      this->var = var;
      this->bins = bins;
      this->min = min;
      this->max = max;
      this->units = "";
      this->ltx = "";
    };

    bound( std::string name, std::string var, int bins, double min, double max, std::string units, std::string ltx ){
      this->name = name;
      this->var = var;
      this->bins = bins;
      this->min = min;
      this->max = max;
      this->units = units;
      this->ltx = ltx;
    }

    
    // setters
    void set_name( std::string name );
    void set_var( std::string var );
    void set_bins( int bins );
    void set_min( double min );
    void set_max( double max );
    void set_units( std::string units);
    void set_ltx( std::string ltx );

    // getters
    std::string get_name();
    std::string get_var();
    int get_bins();
    double get_min();
    double get_max();
    std::string get_ltx();
    std::string get_units();
    char * get_cut( char * buffer );
    std::string get_cut();
    std::vector< std::string > get_cut_series( int bins=0 );
    std::vector< std::string > get_series_names( int bins=0, bool small=false );
    double get_width();
    double get_bin_width();
    double get_bin_width( int bins );
    TH1F * get_hist( std::string name, int bins = 0 );
    TH1F * get_hist();
    TH2F * get_2d_hist( std::string name, bound & y_axis );
    TH2F * get_2d_hist( bound & y_axis );
    std::string get_x_str();
    std::string get_y_str();
    //std::string get_x_str( int bins );
    std::string get_y_str( int bins );

    std::vector< double > get_edges( int bins = 0 );

    bool in_bound( double & value );
    bool in_bound( double && value );
    int find_bin( double & value);
    int find_bin( double && value);



};


#endif
