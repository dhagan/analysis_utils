#ifndef style_hxx
#define style_hxx

#include "common.hxx"
#include <nlohmann/json.hpp>

extern "C" void prep_style();



extern "C" class style_set{

  public:
    int step;
    std::string stage, style_string;
    std::vector< bool > progressive, predefined_progression;
    std::vector< std::vector< float > > predefined_values;
    std::vector< int > step_threshold;
    std::vector< float > step_size, initial_style_vector, current_style_vector;

   
    void update_style( int step ){
      for ( int feature_idx = 0; feature_idx < (int) initial_style_vector.size(); feature_idx++ ){
        if ( !progressive[ feature_idx ] ){ continue; }
        int feature_steps = ( step - ( step % step_threshold[feature_idx] ) );
        if ( predefined_progression[ feature_idx ] ){
          current_style_vector[ feature_idx ] = predefined_values[ feature_idx ][ feature_steps ];
        } else {
          current_style_vector[ feature_idx ] = initial_style_vector[ feature_idx ] + ( feature_steps * step_size[ feature_idx ] );
        }
      }
    }

    void style_histogram( TH1 * histogram ){
      histogram->SetMarkerStyle( current_style_vector.at( 0 ) );
      histogram->SetMarkerColorAlpha( current_style_vector.at( 1 ), current_style_vector.at( 2 ) );
      histogram->SetMarkerSize( current_style_vector.at( 3 ) );
      histogram->SetLineStyle( current_style_vector.at( 4 ) );
      histogram->SetLineColorAlpha( current_style_vector.at( 5 ), current_style_vector.at( 6 ) );
      histogram->SetLineWidth(  current_style_vector.at( 7 ) );
      histogram->SetFillColorAlpha( current_style_vector.at( 8 ), current_style_vector.at( 9 ) );
      histogram->SetFillStyle( current_style_vector.at( 10 ) );
      step += 1; 
      update_style( step );
    }

    void style_function( TF1 * function ){
      function->SetMarkerStyle( current_style_vector.at( 0 ) );
      function->SetMarkerColorAlpha( current_style_vector.at( 1 ), current_style_vector.at( 2 ) );
      function->SetMarkerSize( current_style_vector.at( 3 ) );
      function->SetLineStyle( current_style_vector.at( 4 ) );
      function->SetLineColorAlpha( current_style_vector.at( 5 ), current_style_vector.at( 6 ) );
      function->SetLineWidth(  current_style_vector.at( 7 ) );
      function->SetFillColorAlpha( current_style_vector.at( 8 ), current_style_vector.at( 9 ) );
      function->SetFillStyle( current_style_vector.at( 10 ) );
      step += 1; 
      update_style( step );
    }

    void set_step( int step ){ this->step = step; }
    //void set_progressive(){
    //std::vector< bool > progressive, predefined_progression;
    //std::vector< std::vector< float > > predefined_values;
    //std::vector< int > step_threshold;
    //std::vector< float > step_size, initial_style_vector, current_style_vector;

    style_set(){};
    style_set(std::string & style_string){
      this->step = 0;
      this->style_string = style_string;
    }

};




extern "C" class style_mgr final{

  private:
    std::string style_mgr_filename;
    nlohmann::json style_json;
    std::string stage_prefix;

  public:
    style_mgr(){};
    style_mgr( std::string style_mgr_filename ){
      this->style_mgr_filename = style_mgr_filename; 
      load_styles();
    }
    
    void load_styles();
    void write_styles();
    void set_style_path( std::string style_mgr_filename ){ this->style_mgr_filename = style_mgr_filename; }
    void add_style( std::string style_string, std::vector< float > & style_vector );
    void add_style( std::string style_string, TH1 * histogram, float fill_alpha=1.0, float marker_alpha=1.0, float line_alpha=1.0 );
    void style_histogram( TH1 * histogram, std::string style_string, bool prefix = true );
    void style_function( TF1 * function, std::string style_string, bool prefix = true );
    void set_stage( std::string stage_prefix );
    style_set get_styleset( std::string style_string, bool prefix = true );
    std::vector< float > get_style_vector( std::string style_string, bool prefix=true );

};

#endif
