
#ifndef cutflow_hxx
#define cutflow_hxx

#include "common.hxx"
#include "plotting.hxx"
#include "process_utils.hxx"
#include "style.hxx"
#include "hist_store.hxx"

extern "C" class cutflow {

  public:
    
    // member variables
    int cuts, current_cut;
    std::string var_str;
    std::vector< std::string > cut_names;
    TH1F * cutflow_hist;
    double var;
    bool distribution;
    std::vector< TH1 * > cut_distributions;

    //constructors
    cutflow(){};

      
    cutflow( std::string & cut_names, bool distribution=false, std::string var_name="", double var=0 ){

      split_strings( this->cut_names, cut_names, ":" );
      this->cuts = this->cut_names.size();
      cutflow_hist = new TH1F( "Cutflow", "", cuts, 0, cuts );

      if ( distribution ){
        this->var = var;
        for ( int cut_idx = 1; cut_idx <= cuts; cut_idx++ ){
          cut_distributions.push_back( new TH1F( Form( "%s_cut_%i", var_name.c_str(), cut_idx ), "", 50, -100, 100 ) );
        }
      }
      current_cut = 0;
    }

    cutflow( int cuts, std::vector<std::string> cut_names, bool distribution=false, std::string var_name="", double var=0 ){
      this->cuts = cuts;
      this->cut_names = cut_names;
      cutflow_hist = new TH1F( "Cutflow", "", cuts, 0, cuts );
      if ( distribution ){
        this->var = var;
        for ( int cut_idx = 0; cut_idx < cuts; cut_idx++ ){
          cut_distributions.push_back( new TH1F( Form( "%s_cut_%i", var_name.c_str(), cut_idx ), "", 50, -100, 100 ) );
        }
      }
      current_cut = 0;
    }

    // Fill a cutflow step
    void fill( int cut ){
      cutflow_hist->Fill( cut+0.1 );
      if ( distribution ){ cut_distributions.at( cut )->Fill( var ); }
    }

    void set_cut( int cut, int integral ){
      cutflow_hist->SetBinContent( cut+1, integral );
    }

    void set_dist( int cut, const TH1 * dist ){
      cut_distributions.at( cut ) = (TH1*) dist->Clone();
      this->set_cut( cut, cut_distributions.at( cut )->Integral() );
    }

    // set the variable to produce with cutflow  
    void set_var( double var ){ this->var = var; }
    void set_var_str( const std::string & var_str ){ this->var_str = var_str; }

    void load_cutflow( std::string & filepath, std::string & unique );
    void expand_cutflow( std::vector< std::string > & extra_cuts );

    // write out the cutflow
    void write( std::string unique, bound var_bound );

    void split_cutflow( int splits );

    std::vector< double > get_counts();



};

#endif
