
#include "common.hxx"

#ifndef variable_set_hxx
#define variable_set_hxx

#include "bound_mgr.hxx"
#include "bound.hxx"
#include <ROOT/RDataFrame.hxx>
#include <RooFitResult.h>


extern "C" class variable_set {

  public:
    bound_mgr * bound_manager;
    std::string mass, analysis_variable, spectator_variable;
    bound analysis_bound, spectator_bound;
    std::string extra_variables;
    std::vector< bound > extra_bounds;

    variable_set(){};
    ~variable_set(){};
    variable_set( const std::string & manager_filepath, const std::string & ranges="", const std::string & analysis_variable="", const std::string & spectator_variable="", const std::string & mass="" );
    void set_extra_bounds( std::string & extra_variables, bool replace_current=true );

};

#endif
