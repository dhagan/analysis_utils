#ifdef __MAKECINT__

#include <fit_results.hxx>

#pragma extra_include "fit_results.hxx";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class fit_results+;

#endif 

