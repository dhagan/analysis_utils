cos2theta,costheta*costheta,50,0.0,1.0,,cos^{2}(#theta_{cs})
AbsPhi,AbsPhi,50,0,3.1416,rad,#phi_{cs}
DPhi,DPhi,50,-3.1416,3.1416,rad,#Delta#phi
DY,DY,50,-4,4,,#Delta y
Lambda,Lambda,50,0,200,,#lambda
qtA,qtA,50,-10,20,GeV,q_{T}^{A}
qtB,qtB,50,-20,20,GeV,q_{T}^{B}
DiMuonTau,DiMuonTau,50,-5.0,15.0,ps,#tau
DiMuonMass,DiMuonMass,50,2700,3500,MeV,M_{#mu#mu}
DiMuonPt,DiMuonPt,50,0,30,GeV,p_{T#mu#mu}
PhotonPt,PhotonPt,50,0,30,GeV,p_{T#gamma}
