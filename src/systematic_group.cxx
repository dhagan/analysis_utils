#include <systematic_group.hxx>

void systematic_group::add_fileset( std::string & fileset_path, std::string & efficiency_path, std::string & unique ){
  basic_fileset * fileset = new basic_fileset();
  fileset->load_final_fileset( fileset_path, efficiency_path, unique, true );
  systematics_files.push_back( fileset );
  systematic_names.push_back( unique );
}

void systematic_group::prepare_systematics( variable_set & variables, bool differential ){
  if ( this->predefined ){ return; }
  for ( basic_fileset * single_systematic : systematics_files ){
    hist_group single_systematic_histograms = ( differential ) ? single_systematic->recreate_histograms_differential( variables , true ) : single_systematic->recreate_histograms_weights( variables, true );
    TH1F * systematic_signal = single_systematic_histograms.sign_hist;
    this->systematics_histograms.push_back( systematic_signal );
  }
}


TH1F * systematic_group::evaluate_systematic( variable_set & variables, TH1F * baseline_histogram ){
  if( !valid_state() ){
    std::cout << "Warning: systematic group is not in a valid state, enable group, single, or predefined." << std::endl;
    std::cout << "         Returning empty histogram." << std::endl;
    return new TH1F();
  } else if ( group ){
    return merge_systematic_group( variables, baseline_histogram );
  } else if ( single ){
    return evaluate_single_systematic( baseline_histogram );
  } else {
    return evaluate_predefined_systematic( variables, baseline_histogram );
  }
}

TH1F * systematic_group::evaluate_single_systematic( TH1F * baseline_histogram ){
  this->grouped_systematic = single_sys_to_error( baseline_histogram, this->systematics_histograms.at( 0 ) );
  return this->grouped_systematic;
}

TH1F * systematic_group::evaluate_predefined_systematic( variable_set & variables, TH1F * baseline_histogram ){
  TH1F * predefined_error = systematics_files.at(0)->get_error( variables, variables.analysis_variable );
  this->grouped_systematic = hist_to_errorbar( baseline_histogram, predefined_error, false );
  return grouped_systematic;
}


TH1F * systematic_group::merge_systematic_group( variable_set & variables, TH1F * baseline_histogram ){
    
  TH1F * group_hist = variables.analysis_bound.get_hist( group_name );
  if ( this->single || this->predefined ){
    std::cout << "Warning: systematic group is single or predefined, returning empty histogram." << std::endl;
    return group_hist;
  }

  if( !valid_state() ){
    std::cout << "Warning: systematic group is not in a valid state, enable group, single, or predefined" << std::endl;
    return group_hist;
  }

  int bins = variables.analysis_bound.get_bins();
  std::vector< double > err_max, err_min, combined_sys_err;
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
    err_max.push_back( 0 ); err_min.push_back( DBL_MAX );
    combined_sys_err.push_back( 0 );
  }
  for ( TH1F *& systematic_histogram : systematics_histograms ){
    for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
      if ( systematic_histogram->GetBinContent( bin_idx ) < err_min[ bin_idx - 1 ] ){
        err_min[ bin_idx - 1 ]  = systematic_histogram->GetBinContent( bin_idx );
      }
      if ( systematic_histogram->GetBinContent( bin_idx ) > err_max[ bin_idx - 1 ] ){
        err_max[ bin_idx - 1 ]  = systematic_histogram->GetBinContent( bin_idx );
      }
    }
  }
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
    combined_sys_err[ bin_idx-1 ] = ( err_max[ bin_idx - 1] - err_min[ bin_idx - 1 ] ) / sqrt( 3.0f );
    group_hist->SetBinContent( bin_idx, baseline_histogram->GetBinContent( bin_idx ) );
    group_hist->SetBinError( bin_idx, combined_sys_err[ bin_idx - 1 ] );
  }

  this->grouped_systematic = group_hist;
  return group_hist;
}

