#include <fit_mgr.hxx>

TF1 * fit_mgr::prep_function( function_type type, bound & bound ){
  return fit_mgr::prep_function( type, bound.get_min(), bound.get_max() ); 
}

TF1 * fit_mgr::prep_function( function_type type, float min, float max ){

  const char * function_name = func_c[type];

  if ( !fit_json.contains( function_name ) ){
    std::cout << "No functon of type \"" << function_name << "\". Returning nullptr." << std::endl;
    return nullptr;
  } else {
    std::string name =  std::string( function_name ) + "_" + std::to_string( std::rand() % 10000 + 1 ); 
    std::string equation = fit_json[ function_name ]["equation"].get<std::string>();
    std::vector<std::string> parameters = fit_json[ function_name ]["parameters"].get<std::vector<std::string>>();
    TF1 * function = new TF1( name.c_str(), equation.c_str(), min, max );
    for ( size_t parameter_idx = 0; parameter_idx < parameters.size(); parameter_idx++ ){
      function->SetParName( parameter_idx, parameters[parameter_idx].c_str() ); 
    }
    return function;
  }
}

// load fit json
void fit_mgr::load_fits(){

  std::cout << "Loading fits..." << std::endl;
  if ( fit_mgr_filepath.empty() ){
      this->fit_mgr_filepath = std::getenv( "DEFAULT_FIT_JSON" );
      std::cout << "No fits json provided, selecting default for input: " << fit_mgr_filepath << ".json" << std::endl;
  }
  std::ifstream input_file_stream;
  input_file_stream.open( fit_mgr_filepath + ".json" );
  input_file_stream >> fit_json;
  input_file_stream.close();
  std::cout << "Fit load complete." << std::endl;

}

// store any fits written in the loaded json out to the file.
void fit_mgr::save_fits(){

  std::cout << "Saving fits..." << std::endl;
  std::ofstream output_file_stream;
  if ( fit_mgr_filepath.empty() ){
    this->fit_mgr_filepath = std::getenv( "DEFAULT_FIT_JSON" );
    std::cout << "No fit json provided, selecting default for output: " << fit_mgr_filepath << ".json" << std::endl;
  }
  output_file_stream.open( fit_mgr_filepath + ".json" );
  output_file_stream << std::setw( 4 ) << fit_json << std::endl; 
  output_file_stream.close();
  std::cout << "Fits saved to " << this->fit_mgr_filepath + ".json" << std::endl;
}


// load parameterisation of a function from stored json, skip limits for unlimited parameters.
// "initial" and "final" denotes to load the parameterisation with the variable before performing
// the fit, or with the results of the fit
void fit_mgr::load_parameterisation( TF1 * func, std::string key, std::string function_type, bool initial ){

  // check which initial parameter to use.
  std::string parameter_set = initial ? "_initial" : "_result";

  // checks if entry exists.
  if ( !fit_json.contains( key ) ){
    std::cout << "No entry with parameterisaton key \"" << key << "\" for this file, returning" << std::endl;
    return;
  }
  nlohmann::json selected_fits = fit_json[ key ];

  if ( !selected_fits.contains( function_type ) ){
    std::cout << "No function of type \"" << function_type << "\" for this key \'"<< key << "\', returning" << std::endl;
    return;
  }

  std::string str_mode = (initial) ? "initial mode." : "final mode.";
  std::cout << "Loading fit \"" << function_type <<  "\", with key \"" << key <<  "\", " << str_mode << std::endl;

  nlohmann::json stored_fit = selected_fits[ function_type ];

  int parameter_count = func->GetNpar();
  for ( int parameter = 0; parameter < parameter_count; parameter++ ){

    std::string parameter_name = func->GetParName( parameter );

    bool parameter_limited = stored_fit[ parameter_name + "_limited" ].get<bool>();
    if ( !parameter_limited ){
      std::cout << parameter_name + "is not limited, continuing" << std::endl;
      continue;
    }

    float upper_limit = FLT_MAX, lower_limit = -FLT_MAX, parameter_start = 0, parameter_error = 0;

    if ( stored_fit[ parameter_name + "_upper" ].is_null() && parameter_limited ){
      std::cout << "WARNING: " << parameter_name + "_upper is not stored, setting to FLT_MAX" << std::endl;
    } else {
      upper_limit = stored_fit[ parameter_name + "_upper" ].get<float>();
    }

    if ( stored_fit[ parameter_name + "_lower" ].is_null() && parameter_limited ){
      std::cout << "WARNING: " << parameter_name + "_lower is not stored, setting to FLT_MIN" << std::endl;
    } else {
      lower_limit = stored_fit[ parameter_name + "_lower" ].get<float>();
    }

    if ( stored_fit[ parameter_name + parameter_set ].is_null() ){
      std::cout << parameter_name + parameter_set + " is not stored, ignoring" << std::endl;
    } else {
      parameter_start = stored_fit[ parameter_name + parameter_set ].get<float>();
    }

    if ( stored_fit[ parameter_name + "_error" ].is_null() ){
      std::cout << parameter_name + "_error" + " is not stored, ignoring" << std::endl;
    } else {
      parameter_error = stored_fit[ parameter_name + "_error" ].get<float>();
    }

    if ( parameter_limited ){
      func->SetParLimits( parameter, lower_limit, upper_limit );
    }
    func->SetParameter( parameter, parameter_start );
    if ( !initial ){ func->SetParError( parameter, parameter_error ); }
    
  }

  if ( ( stored_fit[ "range_lower" ].is_null() || stored_fit[ "range_upper" ].is_null() ) || !stored_fit[ "ranged" ] ){
      std::cout << "Function range is not stored, ignoring" << std::endl;
    } else {
      func->SetRange( stored_fit[ "range_lower" ].get<float>(), stored_fit[ "range_upper" ].get<float>() );
    }
   
}

// write out a specific parameterisation to the json that is loaded into the class;
// behaviour. evaluates if the parameter limits are defined based on the returns,
// stores is a parameter or not automatically
void fit_mgr::save_parameterisation( TF1 * func, std::string key, std::string function_type, bool initial, bool update_parameterisation ){

  // check if initial parameter
  std::string parameter_set = initial ? "_initial" : "_result";

  if ( !fit_json.contains( key ) ){
    std::cout << "No entry of key \"" << key << "\" for this file, creating." << std::endl;
  }

  if ( fit_json[ key ].contains( function_type ) && !update_parameterisation ){
    std::cout << "WARNING: Function \"" << function_type << "\" exists for this key \'"<< key << "\'" << std::endl;
    std::cout << "         User should explicitly enable \"update_parameterisation\" to overwrite" << std::endl;
    std::cout << "         Storing this parameterisation with \"_temp\" suffix on key." << std::endl;
    key += "_temp"; 
  } 

  nlohmann::json selected_fits = fit_json[ key ][ function_type ];
  if ( selected_fits.contains( "chi2" ) && selected_fits.contains( "fit_before" ) ){
    if ( selected_fits[ "fit_before" ].get<bool>() && func->GetChisquare() > selected_fits[ "chi2" ] ){
      std::cout << "WARNING: Stored function \"" << function_type << "\" for key \'"<< key << "\' is" << std::endl;
      std::cout << "         of a lower chi2 than that which is overwriting it." << std::endl;
    }
  }

  std::string str_mode = (initial) ? "initial mode." : "final mode.";
  std::cout << "Storing fit \"" << function_type <<  "\", with key \"" << key <<  "\", " << str_mode << std::endl;


  int parameter_count = func->GetNpar();
  for ( int parameter = 0; parameter < parameter_count; parameter++ ){

    std::string parameter_name = func->GetParName( parameter );
    double parameter_value = func->GetParameter( parameter );
    double parameter_error = func->GetParError( parameter );
    double parameter_upper, parameter_lower;
    func->GetParLimits( parameter, parameter_lower, parameter_upper );

    if ( !(parameter_lower == parameter_upper && parameter_lower == 0 ) ){
      selected_fits[ parameter_name + "_limited" ] = true;
      selected_fits[ parameter_name + "_lower" ] = parameter_lower;
      selected_fits[ parameter_name + "_upper" ] = parameter_upper;
    } else {
      selected_fits[ parameter_name + "_limited" ] = false;
      selected_fits[ parameter_name + "_lower" ];
      selected_fits[ parameter_name + "_upper" ];
    }
    selected_fits[ parameter_name + parameter_set ] = parameter_value;
    if ( !initial ){ selected_fits[ parameter_name + "_error" ] = parameter_error; }
  }



  if ( !fit_json[ key ].contains( "fit_types" ) ){
    fit_json[ key ][ "fit_types" ] = { function_type };
  } else if ( fit_json[ key ].contains( "fit_types" )){
    nlohmann::json fit_types = fit_json[ key ][ "fit_types" ];
    bool found = false;
    for ( auto & fit : fit_types ){
      std::string fit_string = fit.get<std::string>();
      if ( fit_string.find( function_type ) != std::string::npos ){ found = true; }
    }
    if ( !found ){ fit_json[ key ][ "fit_types" ].push_back( function_type ); }
  }

  double range_lower, range_upper;
  func->GetRange( range_lower, range_upper );
  if ( !(range_lower == range_lower && range_lower == 0 ) ){
    selected_fits[ "range_lower" ] = range_lower;
    selected_fits[ "range_upper" ] = range_upper;
    selected_fits[ "ranged" ] = true;
  } else {
    selected_fits[ "ranged" ] = false;
    selected_fits[ "range_lower" ] = range_lower;
    selected_fits[ "range_upper" ] = range_upper;
  }

  double func_chi2 = func->GetChisquare();
  if ( func_chi2 == 0 ){ 
    selected_fits[ "fit_before" ] = false;
  } else {
    selected_fits[ "fit_before" ] = true;
  }
  selected_fits[ "chi2" ] = func->GetChisquare();

  fit_json[ key ][ function_type ] = selected_fits;

}
void fit_mgr::set_fit_path( std::string fit_mgr_filepath ){
  this->fit_mgr_filepath = fit_mgr_filepath;
}
