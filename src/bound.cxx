#include <bound.hxx>

// setters
void bound::set_name( std::string name ){ this->name = name; }
void bound::set_var( std::string var ){ this->var = var; }
void bound::set_bins( int bins ){ this->bins = bins; }
void bound::set_min( double min ){ this->min = min; }
void bound::set_max( double max ){ this->max = max; }
void bound::set_ltx( std::string ltx ){ this->ltx = ltx; }
void bound::set_units( std::string units){ this->units = units; }

// getters
std::string bound::get_name(){ return std::string( this->name ); } 
std::string bound::get_var(){ return std::string( this->var ); } 
int bound::get_bins(){ return int( this->bins ); }
double bound::get_min(){ return double( this->min ); }
double bound::get_max(){ return double( this->max); }
std::string bound::get_ltx(){ return std::string( this->ltx ); }
std::string bound::get_units(){ return std::string( this->units ); }

double bound::get_width(){
  return ( this->max - this->min );
}

double bound::get_bin_width( int bins ){
  return this->get_width()/( (double) bins );
}

double bound::get_bin_width(){
  return this->get_bin_width( this->bins );
}

//double bound::get_safe_width(){
//   
//
//
//}


// getter to produce the cut this bound would produce

char * bound::get_cut( char * buffer ){
  sprintf( buffer, 
           "(%s>=%.5f)&&(%s<=%.5f)",
           this->get_var().c_str(), min, 
           this->get_var().c_str(), max );
  return buffer;
}


std::string bound::get_cut(){
  char buffer[150];
  sprintf( buffer, "(%s>=%.5f)&&(%s<=%.5f)", this->get_var().c_str(), min, this->get_var().c_str(), max );
  return std::string( buffer );
}

// getter to produce a series of cuts if this bound is subdivided by a number of bins
std::vector< std::string > bound::get_cut_series( int bins ){
  if ( bins == 0 ){ bins = this->bins; }
  std::vector< std::string > cut_series;
  double width = this->get_bin_width( bins );
  std::string var = this->get_var();
  for ( int cut_idx = 0; cut_idx < bins; cut_idx++ ){
    double lower = this->min + width*cut_idx;
    double upper = lower + width;
    cut_series.push_back( Form( "(%s>%.5f)&&(%s<%.5f)", 
                                var.c_str(), lower, 
                                var.c_str(), upper ) );
  }
  return cut_series;
}

std::vector< std::string > bound::get_series_names( int bins, bool small ){
  
  if ( bins == 0 ){ bins = this->get_bins(); }
  std::vector< std::string > series_names;
  for ( int cut_idx = 1; cut_idx <= bins; cut_idx++ ){
    if ( !small ){
      series_names.push_back( Form( "%s%i-%i", name.c_str(), cut_idx, bins ) ); 
    } else {
      series_names.push_back( Form( "%s-%i", name.c_str(), cut_idx ) ); 
    }
  }
  return series_names;
}

TH1F * bound::get_hist( std::string name, int bins ){
  if ( bins == 0 ){ bins = this->bins; }
  return new TH1F( name.c_str(), "", bins, this->min, this->max );
}

TH1F * bound::get_hist(){
  std::string random_name = "hist_" + std::to_string( std::rand() % 10000 + 1 );
  return get_hist( random_name );
}


TH2F * bound::get_2d_hist( std::string name, bound & y_axis ){
  return new TH2F( name.c_str(), "", this->bins, this->min, this->max,
                   y_axis.get_bins(), y_axis.get_min(), y_axis.get_max() );
}

TH2F * bound::get_2d_hist( bound & y_axis ){
  std::string random_name = "hist_" + std::to_string( std::rand() % 10000 + 1 );
  return get_2d_hist( random_name, y_axis );
}

std::string bound::get_x_str(){
  if ( this->units.empty() ) {
    return this->ltx;
  } else {
    return std::string( this->ltx + " [" + this->units + "]" );
  }
  //return Form( "%s [%s]", this->ltx.c_str(), 
  //            this->units.c_str() );
}

std::string bound::get_y_str( int bins ){
  if ( this->units.empty() ){
   return Form("%.3f", this->get_bin_width( bins ) );
  } else {
  return Form("%.3f [%s^{-1}]", 
              this->get_bin_width( bins ),
              this->units.c_str() );
  }
}

//std::string bound::get_x_str(){
//  return this->get_x_str( this->bins );
//}

std::string bound::get_y_str(){
  return this->get_y_str( this->bins );
}

bool bound::in_bound( double & value ){
  return ( ( value > this->min ) && ( value < this->max ) );
}

bool bound::in_bound( double && value ){
  return ( ( value > this->min ) && ( value < this->max ) );
}


int bound::find_bin( double & value ){
  if ( !this->in_bound( value ) ) return 0;
  return int( ( ( value + ( this->get_bin_width() - std::fmod( value, this->get_bin_width() ) ) ) - this->min )/this->get_bin_width() );
}

int bound::find_bin( double && value ){
  if ( !this->in_bound( value ) ) return 0;
  return int( ( ( value + ( this->get_bin_width() - std::fmod( value, this->get_bin_width() ) ) ) - this->min )/this->get_bin_width() );
}

std::vector< double > bound::get_edges( int bins ){
  if ( bins == 0 ){ bins = this->bins; }
  std::vector< double > edges;
  double bin_width = this->get_bin_width(); 
  double edge = this->min;
  //std::cout << "Name: " << this->get_name() << std::endl;
  //std::cout << "bins: " << bins << std::endl;
  //std::cout << "min: " << this->min << std::endl;
  //std::cout << "max: " << this->max << std::endl;
  //std::cout << "start edge: " << edge << std::endl;
  //std::cout << "width: " << bin_width << std::endl;
  int itr = 0, max_itr = 100;
  while ( edge <= max && ( itr++ < max_itr ) ){
    //std::cout << "edge: " << edge << std::endl;
    //std::cout << "size: " << edges.size() << std::endl;
    if ( abs(edge) < 1e-15 ){ edge = 0.0; }
    edges.push_back( edge );
    edge += bin_width;
  }
  //std::cout << "TERMINATE" << std::endl;
  //for ( double & current_edge : edges ){ std::cout << current_edge << std::endl; }
  return edges; 
}
