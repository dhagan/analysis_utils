#include <process_utils.hxx>

// things you can do
// get error bars from a hist and turn them into their own hist, relative or absolute
// get an error hist, turn those into error bars;
// calculate the absolute or relative error of a systematic ( one-sided )
// combine systematic groups

int type_to_num( const std::string & type ){
  if (type.find("data") != std::string::npos){ return 1; }
  if (type.find("sign") != std::string::npos){ return 2; }
  if (type.find("bckg") != std::string::npos){ return 3; }
  else {
    std::cout << "no type match, returning 0" << std::endl;
    return 0;
  }
}


TH1F * combine_hist_vector( std::vector< TH1F *> hist_vector, std::string name ){

  if ( name.empty() ){ name = hist_vector.at( 0 )->GetName(); }
  TH1F * combined_hist = (TH1F *) hist_vector.at( 0 )->Clone( name.c_str() );
  combined_hist->Reset();
  std::for_each( hist_vector.begin(), hist_vector.end(), [ &combined_hist]( TH1F * add_hist ){ combined_hist->Add( add_hist ); } );
  return combined_hist;

}


std::vector< double > hist_to_vector( TH1 * hist ){
  std::vector< double > vector( hist->GetNbinsX() ); 
  for ( int bin = 0; bin < hist->GetNbinsX(); bin++){ vector.at( bin ) = hist->GetBinContent( bin ); }
  return vector;
}

TH1F * vector_to_hist( std::vector< double > & vector, std::string && name ){
  if ( name.empty() ){ name = std::to_string( std::rand() % 10000 + 1 ); }
  TH1F * hist = new TH1F( name.c_str(), name.c_str(), vector.size(), 0, vector.size() );
  for ( size_t bin = 0; bin < vector.size(); bin++ ){ hist->SetBinContent( bin, vector.at(bin) ); }
  return hist;
}

TH1F * vector_to_hist( std::vector< double > & vector, bound & variable, std::string && name ){
  if ( name.empty() ){ name = std::to_string( std::rand() % 10000 + 1 ); }
  TH1F * hist = new TH1F( name.c_str(), name.c_str(), variable.get_bins(), variable.get_min(), variable.get_max() );
  for ( size_t bin = 0; bin < vector.size(); bin++ ){ hist->SetBinContent( bin, vector.at(bin) ); }
  return hist;
}

void merge_tree( std::string filepath_1, std::string filepath_2, std::string & output_filepath ){

  TChain chain( "tree" );
  std::cout << "merging trees:" << std::endl;
  std::cout << filepath_1.c_str() << std::endl;
  std::cout << filepath_2.c_str() << std::endl;
  chain.Add( filepath_1.c_str() );
  chain.Add( filepath_2.c_str() );
  ROOT::RDataFrame merged_frame( chain );
  merged_frame.Snapshot( "tree", output_filepath.c_str(), ".*" );
}

void combine_hists_in_files( std::string filepath_1, std::string filepath_2, std::string output_filepath, std::vector<std::string> skip ){
  

  TFile * file_1 = new TFile( filepath_1.c_str(), "READ" );
  TFile * file_2 = new TFile( filepath_2.c_str(), "READ" );

  TFile * output_file = new TFile( output_filepath.c_str(), "RECREATE" );
  output_file->cd();
  for ( TObject * key : *file_1->GetListOfKeys() ){
    std::string hist_name = key->GetName();
    if ( std::find( skip.begin(), skip.end(), hist_name ) != skip.end() ){ continue; }


    TH1F * first_hist = (TH1F *) file_1->Get( hist_name.c_str() );
    TH1F * second_hist = (TH1F *) file_2->Get( hist_name.c_str() );

    TH1F * output_hist = (TH1F *) first_hist->Clone();
    output_hist->Add( first_hist, second_hist );
    output_hist->Write(); 
  }

  file_1->Close();
  file_2->Close();
  output_file->Close();

}

// old standard process 
void recombine_spectator( TFile * ana_file, const int & ana_bins, const std::string & bin_name, const std::string & type, bool hf_step, TH1F * combined_hist ){
  std::string hist_name = bin_name;
  if ( hf_step ){ hist_name = type + "_" + bin_name; }
  bool error_available;
  for ( int ana_idx = 1; ana_idx <= ana_bins; ana_idx++ ){
    std::string err_hist_name = "errh" + hist_name + std::to_string( ana_idx );
    std::string int_hist_name = hist_name + std::to_string( ana_idx );
    TH1D * integral_hist = (TH1D *) ana_file->Get( int_hist_name.c_str() );
    TH1D * error_hist = (TH1D *) ana_file->Get( err_hist_name.c_str() );
    combined_hist->SetBinContent( ana_idx, integral_hist->Integral());
    if ( error_hist != NULL ){
        combined_hist->SetBinError( ana_idx , error_hist->GetBinError( type_to_num( type ) ) );
    } else { 
      error_available = false;
    }
  }
  if ( !error_available ){ combined_hist->Sumw2(); }
}

// string splitting mechanism.
void split_strings(std::vector<std::string> & vec_split, const std::string & str_split, std::string delim ){

  std::istringstream str_stream{ str_split };
  std::string token;
  while( getline( str_stream, token, *( delim.c_str() ) ) ){ vec_split.push_back(token); }

}

void abs_hist( TH1 * hist ){
  for ( int idx = 1; idx <= hist->GetNbinsX(); idx++ ){
    hist->SetBinContent( idx, abs( hist->GetBinContent( idx ) ) );
  }
}




void align_sg( TF1 * sg_func, TH1F * hist, bool limit ){
  sg_func->SetParameter( 0, hist->GetMaximum() );
  sg_func->SetParameter( 1, hist->GetMean() );
  sg_func->SetParameter( 2, hist->GetStdDev() );
  if ( limit ){
    sg_func->SetParLimits( 2, hist->GetStdDev()/10.0, hist->GetStdDev()*2.0 );
  }

}

void align_dg( TF1 * dg_func, TH1F * hist, bool limit ){
  dg_func->SetParameter( 0, hist->GetMaximum()*(2./3.) );
  dg_func->SetParameter( 1, hist->GetMean() );
  dg_func->SetParameter( 2, hist->GetStdDev() );
  dg_func->SetParameter( 3, hist->GetMaximum()*(1./3.) );
  dg_func->SetParameter( 4, hist->GetStdDev()*3 );
  if ( limit ){
    dg_func->SetParLimits( 2, hist->GetStdDev()/3.0, hist->GetStdDev() );
    dg_func->SetParLimits( 4, hist->GetStdDev(), hist->GetStdDev()*4.0 );
  }
}




std::map< std::string, std::vector< double > > prep_binnings( std::string input_range ){

  std::map< std::string, std::vector<double> > bins;
  bins["qtA"]           =  std::vector<double>{ 15,  -10,  20};
  bins["BDT"]           =  std::vector<double>{ 10,  -1.0, 1.0};
  bins["abs(qtB)"]      =  std::vector<double>{ 10,  0,    20};
  bins["qtB"]           =  std::vector<double>{ 10,  0,    20};
  bins["Phi"]           =  std::vector<double>{ 8,   0,    M_PI};
  bins["DPhi"]          =  std::vector<double>{ 8,   0,    M_PI};
  bins["DiMuonPt"]      =  std::vector<double>{ 10,  5,    30};
  bins["PhotonPt"]      =  std::vector<double>{ 10,  5,    25};

  if ( !input_range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, input_range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
  }

  return bins;

}


void make_hf_fit_histogram( char bin_name[150], char err_name[150], TFile* source_file, TH1F * reco_hist, int err_bin ){
  int bins = reco_hist->GetNbinsX();
  bool base_error_available{true};
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    TH1F * base_integral_hist   = (TH1F*) source_file->Get( Form( "%s-%i", bin_name, bin_idx ) );
    TH1F * base_error_hist      = (TH1F*) source_file->Get( Form( "%s-%i", err_name, bin_idx ) );
    reco_hist->SetBinContent( bin_idx, base_integral_hist->Integral());
    if ( base_error_hist != NULL ){ 
      reco_hist->SetBinError( bin_idx, base_error_hist->GetBinError( err_bin ) );
    } else { base_error_available = false; }
  }
  if ( !base_error_available ){ reco_hist->Sumw2(); }
}

void rc_hist( std::string bin_name, std::string err_name, TFile* source_file, TH1F * reco_hist, int err_bin ){
  int bins = reco_hist->GetNbinsX();
  bool base_error_available{true};
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    TH1F * base_integral_hist   = (TH1F*) source_file->Get( Form( "%s-%i", bin_name.c_str(), bin_idx ) );
    TH1F * base_error_hist      = (TH1F*) source_file->Get( Form( "%s-%i", err_name.c_str(), bin_idx ) );
    reco_hist->SetBinContent( bin_idx, base_integral_hist->Integral());
    if ( base_error_hist ){ 
      reco_hist->SetBinError( bin_idx, base_error_hist->GetBinError( err_bin ) );
    } else { base_error_available = false; }
  }
  if ( !base_error_available ){ reco_hist->Sumw2(); }
}



// Combine a single error group into an errorbar
void combine_sys_group( std::vector< TH1F *> & group_systematics, TH1F * group_hist, TH1F * base ){
  int bins = group_systematics.at(0)->GetNbinsX();
  std::vector< double > err_max, err_min, combined_sys_err;
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
    err_max.push_back( 0 ); err_min.push_back( DBL_MAX );
    combined_sys_err.push_back( 0 );
  }
  for ( TH1F *& systematic : group_systematics ){
    for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
      if ( systematic->GetBinContent( bin_idx ) < err_min[ bin_idx - 1 ] ){
        err_min[ bin_idx - 1 ]  = systematic->GetBinContent( bin_idx );
      }
      if ( systematic->GetBinContent( bin_idx ) > err_max[ bin_idx - 1 ] ){
        err_max[ bin_idx - 1 ]  = systematic->GetBinContent( bin_idx );
      }
    }
  }
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
    combined_sys_err[ bin_idx-1 ] = ( err_max[ bin_idx - 1] - err_min[ bin_idx - 1 ] ) / sqrt( 3.0f );
    group_hist->SetBinContent( bin_idx, base->GetBinContent( bin_idx ) );
    group_hist->SetBinError( bin_idx, combined_sys_err[ bin_idx - 1 ] );
  }
}


TF1 * prep_sg( float min, float max ){
  int rndnum = std::rand() % 10000 + 1;
  TF1 * sg = new TF1( Form( "sg_%i", rndnum ),
                     "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))",
                     min, max );
  sg->SetParName( 0, "c"); 
  sg->SetParName( 1, "mean" ); 
  sg->SetParName( 2, "#sigma" );
  return sg;
}

TF1 * prep_dg( float min, float max ){
  int rndnum = std::rand() % 10000 + 1;
  TF1 * dg = new TF1( Form( "dg_%i", rndnum ), 
                     "[0]*e^(( -( ([1]-x) * ([1] - x ) ) )/(2*([2]*[2])))"
                     "+ [3]*e^(( -( ([1]-x) * ([1] - x ) ) ) / (2*([4]*[4])))", 
                     min, max );
  dg->SetParName( 0, "c_{1}"); 
  dg->SetParName( 1, "mean" ); 
  dg->SetParName( 2, "#sigma_{1}" );
  dg->SetParName( 3, "c_{2}"); 
  dg->SetParName( 4, "#sigma_{2}" );
  return dg;
}

TF1 * prep_line( float min, float max ){
  int rndnum = std::rand() % 10000 + 1;
  TF1 * line = new TF1( Form( "line_%i", rndnum), "[0]*x + [1]", min, max );
  line->SetParameter( 0, 0 );
  line->SetParameter( 1, 1 );
  line->SetParName( 0, "m" );
  line->SetParName( 1, "c" );
  return line;
}

//TGraphAsymmErrors * diff_to_err( TH1F * diff, bool absolute);
void split_dg( TF1 * dg, TF1 * inner, TF1 * outer ){

  inner->SetParameter( 0, dg->GetParameter( 0 ) );
  outer->SetParameter( 0, dg->GetParameter( 3 ) );

  inner->SetParameter( 1, dg->GetParameter( 1 ) );
  outer->SetParameter( 1, dg->GetParameter( 1 ) );

  inner->SetParameter( 2, dg->GetParameter( 2 ) );
  outer->SetParameter( 2, dg->GetParameter( 4 ) );

}

void pos_hists( TH1F * data, TH1F * sign, TH1F * bckg, bool trex ){

  int value =  100 * ((int) trex);

  for (int bin_number = 1; bin_number <= data->GetNbinsX(); bin_number++){

    if ( data->GetBinContent( bin_number ) <= 0){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
    if ( sign->GetBinContent( bin_number ) <= 0){
      sign->SetBinContent( bin_number, value );
      sign->SetBinError( bin_number, sqrt(value) );
    }
    if (bckg->GetBinContent( bin_number ) <= 0){
      bckg->SetBinContent( bin_number, value );
      bckg->SetBinError( bin_number, sqrt(value) );
    }
    if ((bckg->GetBinContent( bin_number ) <= 0 ) && ( sign->GetBinContent( bin_number ) <= 0 ) ){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
  }
}




//class progress_bar {
//
//	public:
//		int bar_width;
//		int position;
//		float progress;
//		bool iteration_mode;
//		int start, end;
//
//
//		progress_bar( int bar_width ){
//
//			this->bar_width = bar_width;	
//			position = 0;
//			progress = 0;
//
//		}
//
//		void start( ){
//`			for ( int i = 0; i <= bar_width; i++ ) {
//        if (i < pos) std::cout << "=";
//        else if (i == pos) std::cout << ">";
//        else std::cout << " ";
//    	}
//		}
//		
//
//float progress = 0.0;
//while (progress < 1.0) {
//    int barWidth = 70;
//
//    std::cout << "[";
//    int pos = barWidth * progress;
//    for (int i = 0; i < barWidth; ++i) {
//        if (i < pos) std::cout << "=";
//        else if (i == pos) std::cout << ">";
//        else std::cout << " ";
//    }
//    std::cout << "] " << int(progress * 100.0) << " %\r";
//    std::cout.flush();
//
//    progress += 0.16; // for demonstration only
//}
//std::cout << std::endl;
//
//};


//TGraphAsymmErrors * make_diff_graph( TH1F * base, TH1F * sys, bool absolute ){
//
//  double sys_x[15];
//  double sys_y[15];
//  double err_x_upper[15];
//  double err_x_lower[15];
//  double err_y_upper[15];
//  double err_y_lower[15];
//
//  TH1F * err_hist = err_hist( base, sys, absolute )
//  int bins = base->GetNbinsX();
//  double min = base->GetXaxis()->GetBinLowEdge( 1 );
//  double width = base->GetXaxis()->GetBinUpEdge();
//
//  for ( int err_bin = 0; err_bin < bins; err_bin++){ 
//
//    sys_x[err_bin] = ( min + ( width/2.0 ) ) + width*( (double) err_bin );
//    sys_y[err_bin] = 1.0;
//    err_x_upper[err_bin] = 0.0;
//    err_x_lower[err_bin] = 0.0;
//
//    double sys_err = -1.0 + err_hist->GetBinContent( err_bin + 1 );
//
//    if ( sys_err > 0 ){
//      err_y_lower[err_bin] = 0.0;
//      err_y_upper[err_bin] = abs(sys_err);
//    } else {
//      err_y_lower[err_bin] = abs(sys_err);
//      err_y_upper[err_bin] = 0.0;
//    }
//  }
//  TGraphAsymmErrors * err_graph = new TGraphAsymmErrors( 15, sys_x, sys_y, err_x_lower, err_x_upper, 
//                                                        err_y_lower, err_y_upper);
//  err_graph->SetFillStyle(3004);
//  err_graph->SetFillColorAlpha( kBlue+2, 0.8 );
//  err_graph->SetLineWidth(0);
//
//  delete err_hist;
//
//  return err_graph;
//
//}

