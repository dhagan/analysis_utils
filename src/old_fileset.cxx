

//extern "C" class fileset {
//
//  public:
//    TFile * file;
//    std::string unique, filepath;
//    TDirectoryFile * sign_file;
//    TDirectoryFile * bckg_file;
//    TDirectoryFile * data_file;
//    TDirectoryFile * bbbg_file;
//    TDirectoryFile * efficiency_file;
//    TTree * data_tree;
//    TTree * sign_tree;
//    TTree * bckg_tree;
//    TTree * bbbg_tree;
//    TList * efficiency_keys;
//
//
//
//    fileset( std::string unique="", TFile * file=nullptr ){
//      this->unique = unique; 
//      this->file = file;
//      this->sign_file = nullptr;
//      this->bckg_file = nullptr;
//      this->data_file = nullptr;
//      this->bbbg_file = nullptr;
//      this->efficiency_file = nullptr;
//      data_tree = nullptr;
//      sign_tree = nullptr;
//      bckg_tree = nullptr;
//      bbbg_tree = nullptr;
//      //files_set = false;
//
//    }
//
//    fileset( std::string filepath="", std::string unique="" ){
//      this->unique = unique; 
//      if( !filepath.empty() ){ 
//        this->file = new TFile( filepath.c_str(), "READ" ); 
//        this->filepath = filepath;
//      } 
//      else { 
//        this->file = nullptr;
//        this->filepath = filepath;
//      }
//      this->sign_file = nullptr;
//      this->bckg_file = nullptr;
//      this->data_file = nullptr;
//      this->bbbg_file = nullptr;
//      this->efficiency_file = nullptr;
//      data_tree = nullptr;
//      sign_tree = nullptr;
//      bckg_tree = nullptr;
//      bbbg_tree = nullptr;
//    }
//
//    void set_unique( std::string & unique ){ this->unique = unique; }
//
//    void set_file( TFile * file ){ this->file = file; }
//    void set_file( std::string filepath ){ this->file = new TFile( filepath.c_str(), "READ" ); }
//    void set_filepath( std::string filepath ){ this->filepath = filepath; }
//
//    void set_sign( TFile * sign_file ){ 
//      this->sign_file = sign_file;
//    }
//    void set_sign( std::string & sign_filepath ){ 
//      this->sign_file = new TFile( sign_filepath.c_str(), "READ" );
//    }
//
//    void set_bckg( TFile * bckg_file ){ 
//      this->bckg_file = bckg_file;
//    }
//    void set_bckg( std::string & bckg_filepath ){ 
//      this->bckg_file = new TFile( bckg_filepath.c_str(), "READ" );
//    }
//
//    void set_data( TFile * data_file ){ 
//      this->data_file = data_file;
//      //
//    }
//    void set_data( std::string & data_filepath ){ 
//      this->data_file = new TFile( data_filepath.c_str(), "READ" );
//    }
//
//    void set_bbbg( TFile * bbbg_file ){ 
//      this->bbbg_file = bbbg_file;
//    }
//    void set_bbbg( std::string & bbbg_filepath ){ 
//      this->bbbg_file = new TFile( bbbg_filepath.c_str(), "READ" );
//    }
//
//    void set_efficiency( TFile * efficiency_file ){ 
//      this->efficiency_file = efficiency_file;
//    }
//    void set_efficiency( std::string & efficiency_filepath ){ 
//      this->efficiency_file = new TFile( efficiency_filepath.c_str(), "READ" ); 
//    }
//
//    void set_sign_tree( TTree * sign_tree ){ this->sign_tree = sign_tree; }
//    void set_bckg_tree( TTree * bckg_tree ){ this->bckg_tree = bckg_tree; }
//    void set_data_tree( TTree * data_tree ){ this->data_tree = data_tree; }
//    void set_bbbg_tree( TTree * bbbg_tree ){ this->bbbg_tree = bbbg_tree; }
//
//
//    TTree * get_sign_tree(){ 
//      if ( this->sign_tree == nullptr ){ std::cout << "WARNING: Sign tree not initialised" << std::endl; }
//      return this->sign_tree;
//    }
//    TTree * get_bckg_tree(){ 
//      if ( this->bckg_tree == nullptr ){ std::cout << "WARNING: Bckg tree not initialised" << std::endl; }
//      return this->bckg_tree;
//    }
//    TTree * get_data_tree(){ 
//      if ( this->data_tree == nullptr ){ std::cout << "WARNING: Data tree not initialised" << std::endl; }
//      return this->data_tree;
//    }
//    TTree * get_bbbg_tree(){
//      if ( this->bbbg_tree == nullptr ){ std::cout << "WARNING: Bbbg tree not initialised" << std::endl; }
//      return this->bbbg_tree;
//    }
//
//    TTree * get_tree( std::string & tree_type ){
//      if ( tree_type.find( "sign" ) != std::string::npos ){
//        return get_sign_tree();
//      }
//      if ( tree_type.find( "bckg" ) != std::string::npos ){
//        return get_bckg_tree();
//      }
//      if ( tree_type.find( "data" ) != std::string::npos ){
//        return get_data_tree();
//      }
//      if ( tree_type.find( "bbbg" ) != std::string::npos ){
//        return get_bbbg_tree();
//      }
//      std::cout << "WARNING: no tree string match, returning new" << std::endl;
//      return new TTree();
//    }
//
//    
//
//    void set_tree( TTree * tree, std::string & tree_type ){
//      if ( tree_type.find( "sign" ) != std::string::npos ){
//        set_sign_tree( tree );
//      }
//      if ( tree_type.find( "bckg" ) != std::string::npos ){
//        set_bckg_tree( tree );
//      }
//      if ( tree_type.find( "data" ) != std::string::npos ){
//        set_data_tree( tree );
//      }
//      if ( tree_type.find( "bbbg" ) != std::string::npos ){
//        set_bbbg_tree( tree );
//      }
//      std::cout << "WARNING: no tree string match, no store made" << std::endl;
//    }
//
//    void apply_sign_efficiency( TH1F * sign, variable_set & variables );
//
//    ~fileset(){};
//
//
//    void create_file( ){
//      this->file = new TFile( filepath.c_str(), "RECREATE" );
//      if ( sign_file == nullptr ){ this->sign_file = new TDirectoryFile( "sign", "", "", file ); }
//      if ( bckg_file == nullptr ){ this->bckg_file = new TDirectoryFile( "bckg", "", "", file ); }
//      if ( data_file == nullptr ){ this->data_file = new TDirectoryFile( "data", "", "", file ); }
//      if ( bbbg_file == nullptr ){ this->bbbg_file = new TDirectoryFile( "bbbg", "", "", file ); }
//      if ( efficiency_file == nullptr ){ this->efficiency_file = new TDirectoryFile( "efficiency", "", "", file ); }
//    }
//
//    void load_file( ){
//
//      if ( file == nullptr && this->filepath.empty() ){
//        std::cout << "WARNING: No input file or filepath, returning" << std::endl; 
//      } else if ( !this->filepath.empty() ){
//        if( gSystem->AccessPathName( this->filepath.c_str() ) ){
//          this->create_file();
//        } else {
//          file = new TFile( this->filepath.c_str(), "UPDATE" );  
//        }
//      }
//
//      this->sign_file = (TDirectoryFile *) file->Get( "sign" );
//      if ( sign_file == nullptr ){ 
//        std::cout << "WARNING: No sign to load" << std::endl; 
//      } else {
//        load_sign_tree();
//      }
//
//      this->bckg_file = (TDirectoryFile *) file->Get( "bckg" );
//      if ( bckg_file == nullptr ){ 
//        std::cout << "WARNING: No bckg to load" << std::endl;
//      } else {
//        load_bckg_tree();
//      }
//
//      this->data_file = (TDirectoryFile *) file->Get( "data" );
//      if ( data_file == nullptr ){ 
//        std::cout << "WARNING: No data to load" << std::endl;
//      } else {
//        load_data_tree();
//      }
//
//      this->bbbg_file = (TDirectoryFile *) file->Get( "bbbg" );
//      if ( bbbg_file == nullptr ){ 
//        std::cout << "WARNING: No bbbg to load" << std::endl;
//      } else {
//        load_bbbg_tree();
//      }
//
//      if ( efficiency_file == nullptr ){ 
//        std::cout << "WARNING: No efficiency to load" << std::endl;
//      } else {
//        check_efficiencies();
//      }
//
//    }
//
//    void load_sign_tree(){
//      sign_tree = (TTree *) sign_file->Get( "tree" );
//      if ( sign_tree == nullptr ){ std::cout << "WARNING: No sign tree present." << std::endl; }
//    }
//
//    void load_bckg_tree(){
//      bckg_tree = (TTree *) bckg_file->Get( "tree" );
//      if ( bckg_tree == nullptr ){ std::cout << "WARNING: No bckg tree present." << std::endl; }
//    }
//
//    void load_data_tree(){
//      this->data_tree = (TTree *) data_file->Get( "tree" );
//      if ( data_tree == nullptr ){ std::cout << "WARNING: No data tree present." << std::endl; }
//    }
//
//    void load_bbbg_tree(){
//      this->bbbg_tree = (TTree *) bbbg_file->Get( "tree" );
//      if ( bbbg_tree == nullptr ){ std::cout << "WARNING: No bbbg tree present." << std::endl; }
//    }
//
//    void check_efficiencies(){
//      efficiency_keys = (TList *) efficiency_file->GetListOfKeys();
//      if( efficiency_keys->GetSize() == 0 ){
//        std::cout << "WARNING: No efficiencies present." << std::endl;
//        return;
//      }
//    }
//
//    void load_trees(){
//      load_sign_tree();
//      load_bckg_tree();
//      load_data_tree();
//      load_bbbg_tree();
//      check_efficiencies();
//    }
//
//    void save_file(){
//
//      TDirectoryFile * output_sign = new TDirectoryFile( "sign", "", "", file );
//      TDirectoryFile * output_bckg = new TDirectoryFile( "bckg", "", "", file );
//      TDirectoryFile * output_data = new TDirectoryFile( "data", "", "", file );
//      TDirectoryFile * output_bbbg = new TDirectoryFile( "bbbg", "", "", file );
//      TDirectoryFile * output_efficiency = new TDirectoryFile( "efficiency", "", "", file );
//
//      sign_file->cd();
//      if ( sign_tree == nullptr ){ 
//        std::cout << "WARNING: No sign to write" << std::endl;
//      } else {
//        output_sign->cd();
//        sign_tree->CopyTree( "" ); 
//        std::cout << "sign written" << std::endl;
//      }
//
//      bckg_file->cd();
//      if ( bckg_tree == nullptr ){ 
//        std::cout << "WARNING: No bckg to write" << std::endl;
//      } else {
//        output_bckg->cd();
//        bckg_tree->CopyTree( "" ); 
//        std::cout << "bckg written" << std::endl;
//      }
//
//      data_file->cd();
//      if ( data_tree == nullptr ){ 
//        std::cout << "WARNING: No data to write" << std::endl;
//      } else {
//        output_data->cd();
//        data_tree->CopyTree( "" ); 
//        std::cout << "data written" << std::endl;
//      }
//      
//      bbbg_file->cd();
//      if ( bbbg_tree == nullptr ){ 
//        std::cout << "WARNING: No bbbg to write" << std::endl;
//      } else {
//        output_bbbg->cd();
//        bbbg_tree->CopyTree( "" ); 
//        std::cout << "bbbg written" << std::endl;
//      }
//
//      efficiency_keys = efficiency_file->GetList();
//      output_efficiency->cd();
//      if( efficiency_keys->GetSize() == 0 ){
//        std::cout << "WARNING: No efficiencies to write" << std::endl;
//      } else {
//        for ( TObject * key : *efficiency_keys ){
//          efficiency_file->Get( key->GetName() )->Write();
//        }
//      }
//
//      file->cd();
//      output_sign->Write();
//      output_bckg->Write();
//      output_data->Write();
//      output_bbbg->Write();
//      efficiency_file->Write();
//      file->Save();
//    }
//
//};
//
//
//extern "C" class final_fileset : public fileset {
//
//  public: 
//    TTree * weighted_tree;
//    TDirectoryFile * weighted_file; 
//    TDirectoryFile * differential_file; 
//    TDirectoryFile * statistics_file; 
//
//
//    final_fileset( std::string filepath="", std::string unique="" ) : fileset( filepath, unique ) {
//      this->weighted_tree = nullptr;
//      this->weighted_file = nullptr; 
//      this->differential_file = nullptr;
//      this->statistics_file = nullptr;
//    };
//    
//    void load_file(){
//      fileset::load_file( );
//      std::cout << 352 << std::endl;
//      this->weighted_file = (TDirectoryFile *) file->Get( "weight" );
//      if ( weighted_file == nullptr ){ 
//        this->weighted_file = new TDirectoryFile( "weight", "", "", file );
//        std::cout << "WARNING: No weight is present" << std::endl;
//      }
//      this->differential_file = (TDirectoryFile *) file->Get( "diff" );
//      if ( differential_file == nullptr ){ 
//        this->differential_file = new TDirectoryFile( "diff", "", "", file );
//        std::cout << "WARNING: No diffs are present" << std::endl;
//      }
//      this->statistics_file = (TDirectoryFile *) file->Get( "stat" );
//      if ( statistics_file == nullptr ){ 
//        std::cout << "WARNING: No stats are present" << std::endl;
//        this->statistics_file = new TDirectoryFile( "stat", "", "", file ); 
//      }
//      load_weighted();
//    }
//
//    void load_weighted(){
//      this->weighted_tree = (TTree *) weighted_file->Get( "tree" );
//      if ( weighted_tree == nullptr ){ std::cout << "WARNING: No weighted tree present." << std::endl; }
//    }
//
//    void load_trees(){
//      fileset::load_trees();
//      load_weighted();
//    }
//    
//    void set_weighted( TFile * weighted_file ){ this->weighted_file = weighted_file; }
//    void set_weighted( std::string & weighted_filepath ){ this->weighted_file = new TFile( weighted_filepath.c_str(), "READ" ); }
//
//    void set_differential( TFile * differential_file ){ this->differential_file = differential_file; }
//    void set_differential( std::string & differential_filepath ){ this->differential_file = new TFile( differential_filepath.c_str(), "READ" ); }
//
//    void set_statistics( TFile * statistics_file ){ this->statistics_file = statistics_file; }
//    void set_statistics( std::string & statistics_filepath ){ this->statistics_file = new TFile( statistics_filepath.c_str(), "READ" ); }
//
//    hist_group recreate_histograms_weights( variable_set & variables, bool efficiency_correct_signal=true, bool normalise=false );
//    hist_group recreate_histograms_differential( variable_set & variables, bool efficiency_correct_signal=true, bool normalise=false );
//    hist_group recreate_histograms_statistical( variable_set & variables, bool efficiency_correct_signal=true, bool normalise=false);
//
//    void save_file( std::string output_filepath ){
//  
//
//      TFile * output_file = new TFile( output_filepath.c_str(), "RECREATE" );
//      output_file->cd();
//      TDirectoryFile * output_weighted = new TDirectoryFile( "weight", "", "", output_file );
//      TDirectoryFile * output_statistics = new TDirectoryFile( "stat", "", "", output_file );
//      TDirectoryFile * output_differential = new TDirectoryFile( "diff", "", "", output_file );
//
//
//      TList * differential_keys = differential_file->GetListOfKeys();
//      output_differential->cd();
//      std::vector< TObject * > diff_objs;
//      if( differential_keys->GetSize() == 0 ){
//        std::cout << "WARNING: No stats to write." << std::endl;
//      } else {
//        for ( TObject * key : *differential_keys ){
//          diff_objs.push_back( differential_file->Get( key->GetName() ) );
//        }
//      }
//      output_differential->cd();
//      for ( TObject * key : diff_objs ){
//        if ( key->InheritsFrom( "TTree" ) ){
//          TTree * temp = (TTree *) key;
//          temp->CopyTree( "" );
//        } else {
//          key->Write();
//        }
//      }
//
//
//
//      TList * statistics_keys = statistics_file->GetListOfKeys();
//      output_statistics->cd();
//      std::vector< TObject * > stats_objs;
//      if( statistics_keys->GetSize() == 0 ){
//        std::cout << "WARNING: No stats to write." << std::endl;
//      } else {
//        for ( TObject * key : *statistics_keys ){
//          stats_objs.push_back( statistics_file->Get( key->GetName() ) );
//        }
//      }
//      output_statistics->cd();
//      for ( TObject * key : stats_objs ){
//        if ( key->InheritsFrom( "TTree" ) ){
//          TTree * temp = (TTree *) key;
//          temp->CopyTree( "" );
//        } else {
//          key->Write();
//        }
//      }
//
//      weighted_file->cd();
//      if ( weighted_tree == nullptr ){ 
//        std::cout << "WARNING: No weighted to write" << std::endl;
//      } else {
//        output_weighted->cd();
//        weighted_tree->CopyTree( "" ); 
//        std::cout << "weighted written" << std::endl;
//      }
//
//      //output
//      output_file->cd();
//      output_weighted->Write( "weight" );
//      output_differential->Write( "diff" );
//      output_statistics->Write( "stat" );
//
//
//    }
//
//
//};





//void fileset::apply_sign_efficiency( TH1F * sign, variable_set & variables ){
//  std::string efficiency_string = Form( "eff_%s_%s", variables.analysis_variable.c_str(), variables.mass.c_str() );
//  if ( this->efficiency_file->GetListOfKeys()->Contains( efficiency_string.c_str() ) ){
//    TH1F * sign_efficiency = (TH1F *) this->efficiency_file->Get( efficiency_string.c_str() );
//    sign->Divide( sign, sign_efficiency, 1.0, 1.0 );
//    std::cout << "Signal efficiency correction \"" << efficiency_string << "\" in \"" << this->unique << "\" applied." << std::endl;
//  } else {
//    std::cout << "WARNING: Efficiency \"" << efficiency_string << "\" not found. Continuing without correction." << std::endl;
//  }
//}
//
////void basic_fileset::apply_sign_efficiency( TH1F * sign, variable_set & variables ){
////  std::string efficiency_string = Form( "eff_%s_%s", variables.analysis_variable.c_str(), variables.mass.c_str() );
////  if ( this->efficiency_file->GetListOfKeys()->Contains( efficiency_string.c_str() ) ){
////    TH1F * sign_efficiency = (TH1F *) this->efficiency_file->Get( efficiency_string.c_str() );
////    sign->Divide( sign, sign_efficiency, 1.0, 1.0 );
////    std::cout << "Signal efficiency correction \"" << efficiency_string << "\" in \"" << this->unique << "\" applied." << std::endl;
////  } else {
////    std::cout << "WARNING: Efficiency \"" << efficiency_string << "\" not found. Continuing without correction." << std::endl;
////  }
////}
//
//
//hist_group final_fileset::recreate_histograms_weights( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "_effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_weightrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_weightrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_weightrecomb_%s", tag.c_str() ) );
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();
//
//  //TTree * weighted_tree = (TTree *) this->weighted_file->Get( "tree" );
//
//  weighted_tree->Draw( Form( "%s>>sign_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ), 
//                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ) );
//                                                                                                          
//  weighted_tree->Draw( Form( "%s>>bckg_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ), 
//                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ) );
//                                                                                                          
//  weighted_tree->Draw( Form( "%s>>data_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ),   
//                       Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
//  
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//
//}
//
//hist_group final_fileset::recreate_histograms_differential( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//  bool error_available = true;
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_diffrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_diffrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_diffrecomb_%s", tag.c_str() ) );
//  TList * key_list = this->differential_file->GetListOfKeys();
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//
//  for ( int analysis_bin = 1; analysis_bin <= variables.analysis_bound.get_bins(); analysis_bin++ ){
//
//    std::string bin_string = variables.mass + "_" + std::to_string( analysis_bin );
//    std::string err_hist_name = "errh_" + variables.mass + "_" + std::to_string( analysis_bin );
//
//    TH1D * data_bin_hist = (TH1D *) this->differential_file->Get( Form( "data_%s", bin_string.c_str()  ) );
//    TH1D * sign_bin_hist = (TH1D *) this->differential_file->Get( Form( "sign_%s", bin_string.c_str()  ) );
//    TH1D * bckg_bin_hist = (TH1D *) this->differential_file->Get( Form( "bckg_%s", bin_string.c_str()  ) );
//    data_hist->SetBinContent( analysis_bin, data_bin_hist->Integral());
//    sign_hist->SetBinContent( analysis_bin, sign_bin_hist->Integral());
//    bckg_hist->SetBinContent( analysis_bin, bckg_bin_hist->Integral());
//    if ( error_available && key_list->Contains( Form( "errh_%s", bin_string.c_str() ) ) ){
//        TH1D * error_hist = (TH1D *) this->differential_file->Get( Form( "errh_%s", bin_string.c_str()  ) );
//        data_hist->SetBinError( analysis_bin , error_hist->GetBinError( 1 ) );
//        sign_hist->SetBinError( analysis_bin , error_hist->GetBinError( 2 ) );
//        bckg_hist->SetBinError( analysis_bin , error_hist->GetBinError( 3 ) );
//    } else { 
//      error_available = false;
//      std::cout << "WARNING: Errors unavailable for \"" << variables.mass << "\" in \"" << this->unique << "\", using Sumw2()" << std::endl;
//    }
//  }
//  if ( !error_available ){ 
//    data_hist->Sumw2(); 
//    sign_hist->Sumw2();
//    bckg_hist->Sumw2();
//  }
//
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//
//}
//
//hist_group final_fileset::recreate_histograms_statistical( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "_effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_statrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_statrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_statrecomb_%s", tag.c_str() ) );
//  TList * key_list = this->statistics_file->GetListOfKeys();
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//
//  if ( !key_list->Contains( variables.mass.c_str() ) ){
//    std::cout << "WARNING: Statistics set \"" << variables.mass << "\" in \"" << this->unique << "\" not stored, returning empty hist group." << std::endl;
//    hist_group hists = hist_group();
//    return hists;
//  }
//  TTree * results_tree = (TTree *) this->statistics_file->Get( variables.mass.c_str() );
//
//  fit_results * results_entry = new fit_results();
//  results_tree->SetBranchAddress( "fit_stats", &results_entry );
//  for ( int analysis_bin = 0; analysis_bin < variables.analysis_bound.get_bins(); analysis_bin++ ){
//    results_tree->GetEntry( analysis_bin );
//    data_hist->SetBinContent( analysis_bin+1, (*results_entry).data ); 
//    data_hist->SetBinError(   analysis_bin+1, (*results_entry).data_error ); 
//    sign_hist->SetBinContent( analysis_bin+1, (*results_entry).sign_fit ); 
//    sign_hist->SetBinError(   analysis_bin+1, (*results_entry).sign_fit_error ); 
//    bckg_hist->SetBinContent( analysis_bin+1, (*results_entry).bckg_fit ); 
//    bckg_hist->SetBinError(   analysis_bin+1, (*results_entry).bckg_fit_error ); 
//  }
//
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//}





//hist_group final_fileset::recreate_histograms_weights( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "_effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_weightrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_weightrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_weightrecomb_%s", tag.c_str() ) );
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();
//
//  this->weighted_tree->Draw( Form( "%s>>sign_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ), 
//                       Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ) );
//                                                                                                           
//  this->weighted_tree->Draw( Form( "%s>>bckg_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ), 
//                       Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ) );
//                                                                                                           
//  this->weighted_tree->Draw( Form( "%s>>data_weightrecomb_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ),   
//                       Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
//  
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//
//}

//hist_group final_fileset::recreate_histograms_differential( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//  bool error_available = true;
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_diffrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_diffrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_diffrecomb_%s", tag.c_str() ) );
//  TList * key_list = this->differential_dir->GetListOfKeys();
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//
//  for ( int analysis_bin = 1; analysis_bin <= variables.analysis_bound.get_bins(); analysis_bin++ ){
//
//    std::string bin_string = variables.mass + "_" + std::to_string( analysis_bin );
//    std::string err_hist_name = "errh_" + variables.mass + "_" + std::to_string( analysis_bin );
//
//    TH1D * data_bin_hist = (TH1D *) this->differential_dir->Get( Form( "data_%s", bin_string.c_str()  ) );
//    TH1D * sign_bin_hist = (TH1D *) this->differential_dir->Get( Form( "sign_%s", bin_string.c_str()  ) );
//    TH1D * bckg_bin_hist = (TH1D *) this->differential_dir->Get( Form( "bckg_%s", bin_string.c_str()  ) );
//    data_hist->SetBinContent( analysis_bin, data_bin_hist->Integral());
//    sign_hist->SetBinContent( analysis_bin, sign_bin_hist->Integral());
//    bckg_hist->SetBinContent( analysis_bin, bckg_bin_hist->Integral());
//    if ( error_available && key_list->Contains( Form( "errh_%s", bin_string.c_str() ) ) ){
//        TH1D * error_hist = (TH1D *) this->differential_dir->Get( Form( "errh_%s", bin_string.c_str()  ) );
//        data_hist->SetBinError( analysis_bin , error_hist->GetBinError( 1 ) );
//        sign_hist->SetBinError( analysis_bin , error_hist->GetBinError( 2 ) );
//        bckg_hist->SetBinError( analysis_bin , error_hist->GetBinError( 3 ) );
//    } else { 
//      error_available = false;
//      std::cout << "WARNING: Errors unavailable for \"" << variables.mass << "\" in \"" << this->unique << "\", using Sumw2()" << std::endl;
//    }
//  }
//  if ( !error_available ){ 
//    data_hist->Sumw2(); 
//    sign_hist->Sumw2();
//    bckg_hist->Sumw2();
//  }
//
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//
//}

//hist_group final_fileset::recreate_histograms_statistical( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
//
//  std::string tag = variables.mass + "_" + this->unique;
//  if ( normalise ){ tag += "_norm"; }
//  if ( efficiency_correct_signal ){ tag += "_effc"; }
//  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_statrecomb_%s", tag.c_str() ) );
//  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_statrecomb_%s", tag.c_str() ) );
//  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_statrecomb_%s", tag.c_str() ) );
//  TList * key_list = this->statistics_dir->GetListOfKeys();
//
//  if ( variables.mass.empty() ){ 
//    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
//    variables.mass = "Q12";
//  }
//
//  if ( !key_list->Contains( variables.mass.c_str() ) ){
//    std::cout << "WARNING: Statistics set \"" << variables.mass << "\" in \"" << this->unique << "\" not stored, returning empty hist group." << std::endl;
//    hist_group hists = hist_group();
//    return hists;
//  }
//  TTree * results_tree = (TTree *) this->statistics_dir->Get( variables.mass.c_str() );
//
//  fit_results * results_entry = new fit_results();
//  results_tree->SetBranchAddress( "fit_stats", &results_entry );
//  for ( int analysis_bin = 0; analysis_bin < variables.analysis_bound.get_bins(); analysis_bin++ ){
//    results_tree->GetEntry( analysis_bin );
//    data_hist->SetBinContent( analysis_bin+1, (*results_entry).data ); 
//    data_hist->SetBinError(   analysis_bin+1, (*results_entry).data_error ); 
//    sign_hist->SetBinContent( analysis_bin+1, (*results_entry).sign_fit ); 
//    sign_hist->SetBinError(   analysis_bin+1, (*results_entry).sign_fit_error ); 
//    bckg_hist->SetBinContent( analysis_bin+1, (*results_entry).bckg_fit ); 
//    bckg_hist->SetBinError(   analysis_bin+1, (*results_entry).bckg_fit_error ); 
//  }
//
//  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
//
//  if ( normalise ){
//    data_hist->Scale( 1.0/data_hist->Integral() );    
//    sign_hist->Scale( 1.0/sign_hist->Integral() );    
//    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
//  }
//
//  hist_group hists = hist_group();
//  hists.data_hist = data_hist;
//  hists.sign_hist = sign_hist;
//  hists.bckg_hist = bckg_hist;
//  return hists;
//}

