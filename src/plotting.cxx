#include <plotting.hxx>

// things you can do
// get error bars from a hist and turn them into their own hist, relative or absolute
// get an error hist, turn those into error bars;
// calculate the absolute or relative error of a systematic ( one-sided )
// combine systematic groups


TPaveText * pseudostats( TH1 * hist, bool small, bool shifted ){
  if ( shifted ){ small = true; };
  double shift = 0.13 * shifted;
  double width = 0.24 / ( 1 + small );
  gPad->Update();
  TPaveText * stats = new TPaveText();
  stats->SetX1NDC( 0.56 + shift ); 
  stats->SetX2NDC( 0.56 + shift + width);
  stats->SetY1NDC( 0.55 ); 
  stats->SetY2NDC( 0.78 );
  stats->SetFillStyle( 0 ); 
  stats->SetTextFont( 42 );
  stats->SetTextSize( 0.02 * ( 2.0 / ( 2.0+small ) ) );
  stats->SetBorderSize( 1 );
  stats->AddText( Form( "Entries - %i", (int) hist->GetEntries() ) );
  stats->AddText( Form( "Mean - %.3f", hist->GetMean() ) );
  stats->AddText( Form( "Integral - %i", (int) hist->GetEntries() ) );
  stats->AddText( Form( "Kurtosis -%.3f", hist->GetKurtosis() ) );
  stats->AddText( Form( "StdDev - %.3f", hist->GetStdDev() ) );
  gPad->Modified();
  gPad->Update();
  return stats;
}




TGraphAsymmErrors * diff_graph( TH1F * base, TH1F * sys, bool absolute ){

  TH1F * base_clone = (TH1F *) base->Clone();
  TH1F * sys_clone = (TH1F *) sys->Clone();

  int bins = base_clone->GetNbinsX();
  double min = base_clone->GetXaxis()->GetXmin();
  double max = base_clone->GetXaxis()->GetXmax();
  double width = (max-min)/bins;

  TH1F * diff = sys_to_error_hist( base_clone, sys_clone, absolute );

  // diff to grapherr start
  double * sys_x = new double[bins];
  double * sys_y = new double[bins];
  double * err_x_lower = new double[bins];
  double * err_x_upper = new double[bins];
  double * err_y_lower = new double[bins];
  double * err_y_upper = new double[bins];

  for ( int bin = 0; bin < bins; bin++ ){

    sys_x[bin] = ( min + ( width/2.0 ) ) + ( width * ( (double) bin ) );
    sys_y[bin] = 1.0;
    err_x_lower[bin] = 0;
    err_x_upper[bin] = 0;
    double err = diff->GetBinContent( bin + 1 );
    if ( err > 0 ){
      err_y_lower[bin] = 0.0;
      err_y_upper[bin] = abs( err );
    } else {
      err_y_lower[bin] = abs( err );
      err_y_upper[bin] = 0.0;
    }
  }

  // create symm error graphs
  TGraphAsymmErrors * err_graph = new TGraphAsymmErrors( bins, sys_x, sys_y,
                                       err_x_lower, err_x_upper, err_y_lower, err_y_upper);
	return err_graph;

}

// tested only for div 2,1
TH1F * ratio_pad( TH1 * numerator, TH1 * denominator, TPad * pad ){


  pad->Divide( 1, 2, 0.0, 0.0 );
  // prep ratio
  TH1F * ratio = (TH1F *) numerator->Clone();
  ratio->Reset();
  ratio->Divide( numerator, denominator, 1.0, 1.0 );
  ratio->SetLineColorAlpha( 1.0, 1.0 );
  ratio->SetLineStyle( 1.0 );
  ratio->SetLineWidth( 1.0 );

  // prep pads
  TPad * upper_subpad = (TPad *) pad->cd( 1 );
  upper_subpad->SetPad( 0.0, 0.35, 0.84, 0.81 );
  TPad * lower_subpad = (TPad *) pad->cd( 2 );
  lower_subpad->SetPad( 0.0, 0.034, 0.84, 0.35 );
  lower_subpad->SetBottomMargin( 0.4 );

  // upper pad
  upper_subpad = (TPad *) pad->cd( 1 );
  numerator->Draw( "HIST E1" );
  denominator->Draw( "HIST E1 SAMES" );

  // upper y axis
  numerator->GetYaxis()->SetLabelSize( 0.08 );
  numerator->GetYaxis()->SetTitleSize( 0.08 );
  numerator->GetYaxis()->ChangeLabel( 1, -1, 0 );
  numerator->GetYaxis()->SetTitleOffset( 0.8 );
  numerator->GetYaxis()->SetRangeUser( 0, std::max( { numerator->GetMaximum(), denominator->GetMaximum() } ) * 1.5 );
  // upper x axis
  numerator->GetXaxis()->SetLabelSize( 0 );
  numerator->GetXaxis()->SetTitleSize( 0 );

  TPaveStats * numerator_stats = make_ratio_stats( numerator, true, true );
  numerator_stats->Draw( "SAME" );
  TPaveStats * denominator_stats = make_ratio_stats( denominator, true, false );
  denominator_stats->Draw( "SAME" );
  // lower pad
  lower_subpad = (TPad *) pad->cd( 2 );
  lower_subpad->SetGridy();
  ratio->Draw( "HIST E1" );
  ratio->SetStats( 0 );
  hist_prep_axes( ratio, true );

  // lower y axis
  ratio->GetYaxis()->SetLabelSize( 0.09 );
  ratio->GetYaxis()->SetTitleSize( 0.09 );
  ratio->GetYaxis()->ChangeLabel( 1, -1, 0 );
  ratio->GetYaxis()->ChangeLabel( -1, -1, 0 );
  ratio->GetYaxis()->SetTitleOffset( 0.6 );
  ratio->GetYaxis()->SetTitle( "Ratio" );
  ratio->GetYaxis()->SetRangeUser( -1, 3.0 );

  // lower x axis
  ratio->GetXaxis()->SetLabelSize( 0.105 );
  ratio->GetXaxis()->SetTitleSize( 0.105 );
  ratio->GetXaxis()->SetTitle( numerator->GetXaxis()->GetTitle() );

  return ratio;

}

void hist_limits( TH1 * hist, double && min, double && max ){
  hist->GetYaxis()->SetRangeUser( min, max );
}



void hist_prep_axes( TH1 * hist, bool zero, bool centre, double centre_val ){
  //hist->GetXaxis()->SetRange( hist->GetXaxis()->GetXmin(), hist->GetXaxis()->GetXmax() );
  
  double min = ( zero ) ? 0.0 : hist->GetMinimum();
  if ( min < 0.0 ){ min *= 1.75; }
  else { min *= 0.55; }
  double max = hist->GetMaximum()*1.75;
  if ( centre ){
    double min_diff = abs( hist->GetMaximum() - centre_val );
    double max_diff = abs( hist->GetMinimum() - centre_val );;
    double range = std::max( { min_diff, max_diff } );
    min = centre_val-range;
    max = centre_val-range;
  }
  hist->GetYaxis()->SetRangeUser( min, max );
  hist->GetYaxis()->SetLabelSize( 0.035 );
  hist->GetYaxis()->SetTitleSize( 0.035 );
  hist->GetXaxis()->SetLabelSize( 0.035 );
  hist->GetXaxis()->SetTitleSize( 0.035 );
  hist->GetYaxis()->SetMaxDigits( 3 );
  //hist->GetXaxis()->SetRange( 1, hist->GetNbinsX() );
}

void hist_prep_text( TH1 * hist ){
  hist->GetYaxis()->SetLabelSize( 0.035 );
  hist->GetYaxis()->SetTitleSize( 0.035 );
  hist->GetXaxis()->SetLabelSize( 0.035 );
  hist->GetXaxis()->SetTitleSize( 0.035 );
  hist->GetYaxis()->SetMaxDigits( 3 );
}



void error_prep_axes( TGraphAsymmErrors * err ){

  double max = std::max( { err->GetMaximum(), abs( err->GetMinimum() ) } );
  max = std::ceil( max );
  err->GetYaxis()->SetRangeUser( max, max );
  err->GetYaxis()->SetLabelSize( 0.035 );
  err->GetYaxis()->SetTitleSize( 0.035 );
  err->GetXaxis()->SetLabelSize( 0.035 );
  err->GetXaxis()->SetTitleSize( 0.035 );
  err->GetYaxis()->SetMaxDigits( 3 );
  //hist->GetXaxis()->SetRange( 1, hist->GetNbinsX() );
}


// turns errorbar on a histogram into histogram containing the error
TH1F * errorbar_to_hist( TH1F * hist, bool absolute ){
  TH1F * err_hist = (TH1F *) hist->Clone();
  err_hist->Reset();
  int bins = err_hist->GetNbinsX();
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    double error = hist->GetBinError( bin_idx );
    if ( !absolute ){ error /= hist->GetBinContent( bin_idx ); }
    err_hist->SetBinContent( bin_idx, error ); 
  }
	return err_hist;
}

// turns a base histogram and a histogram representing the error into
// a single histogram with an errorbar
TH1F * hist_to_errorbar( TH1F * base, TH1F * err_hist, bool absolute ){
  TH1F * errorbar_hist = (TH1F *) base->Clone();
  errorbar_hist->Reset();
  int bins = errorbar_hist->GetNbinsX();
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    double error = err_hist->GetBinContent( bin_idx );
    if( !absolute ){ error *= base->GetBinContent( bin_idx ); }
    errorbar_hist->SetBinContent( bin_idx, base->GetBinContent( bin_idx ) );
    errorbar_hist->SetBinError( bin_idx, error );
  }
	return errorbar_hist;
}

// take a nominal histogram and a systematic, 
// convert this into a histogram with the systematic as an error bar;
// absolute error obviously
TH1F * single_sys_to_error( TH1F * base, TH1F * sys ){
  TH1F * err_hist = (TH1F *) base->Clone();
  err_hist->Reset();
  int bins = err_hist->GetNbinsX();
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    err_hist->SetBinContent( bin_idx, base->GetBinContent( bin_idx ) );
    err_hist->SetBinError( bin_idx, std::abs( sys->GetBinContent( bin_idx ) - base->GetBinContent( bin_idx ) ) );
  }
  return err_hist;
}

// take a nominal histogram and a systematic
// turn this into the absolute or relative error histogram
TH1F * sys_to_error_hist( TH1F * base, TH1F * sys, bool absolute ){
  TH1F * err_hist = (TH1F *) base->Clone();
  err_hist->Reset();
  int bins = err_hist->GetNbinsX();
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){
    double error = sys->GetBinContent( bin_idx ) - base->GetBinContent( bin_idx );
    if ( !absolute ){ error /= base->GetBinContent( bin_idx );}
    err_hist->SetBinContent( bin_idx, error );
  }
  return err_hist;
}


void add_pad_title( TPad * active_pad, std::string title, bool sci_axis ){
  active_pad->cd();
	TLatex pad_title;
	pad_title.SetTextSize( 0.035 );
	pad_title.DrawLatexNDC( (sci_axis) ? 0.23 : 0.17, 0.8175, title.c_str() );  
}

void set_axis_labels( TH1 * hist, std::string xaxis, std::string yaxis ){
  hist->GetXaxis()->SetTitle( xaxis.c_str() );
  hist->GetYaxis()->SetTitle( yaxis.c_str() );
}

void set_err_axis_labels( TGraphAsymmErrors * err, std::string xaxis, std::string yaxis ){
  err->GetXaxis()->SetTitle( xaxis.c_str() );
  err->GetYaxis()->SetTitle( yaxis.c_str() );
}

void add_atlas_decorations( TPad * active_pad, bool wip, bool sim ){
  active_pad->cd();
  TLatex * atlas_logo_ltx = new TLatex(); 
  atlas_logo_ltx->SetNDC(); 
  atlas_logo_ltx->SetTextSize(0.038);
  atlas_logo_ltx->SetTextFont(72);
  atlas_logo_ltx->SetTextColor(1);
  TLatex * wip_ltx = new TLatex(); 
  wip_ltx->SetNDC();
  wip_ltx->SetTextFont(42);
  wip_ltx->SetTextSize(0.038);
  wip_ltx->SetTextColor(1);
  TLatex * sim_ltx = new TLatex();
  sim_ltx->SetNDC();
  sim_ltx->SetTextFont(42);
  sim_ltx->SetTextSize(0.038);
  sim_ltx->SetTextColor(1);
  atlas_logo_ltx->DrawLatexNDC( 0.187, 0.75, "ATLAS" );
  if ( sim ){ sim_ltx->DrawLatexNDC( 0.350, 0.75, "Simulation" ); }
	if ( wip ){ wip_ltx->DrawLatexNDC( 0.187, 0.705, "Work In Progress" ); }
}

void add_internal( TPad * active_pad ){
  active_pad->cd();
  TLatex * int_ltx = new TLatex();
  int_ltx->SetNDC();
  int_ltx->SetTextFont(42);
  int_ltx->SetTextSize(0.038);
  int_ltx->SetTextColor(1);
  int_ltx->DrawLatexNDC( 0.187, 0.705, "Internal" );
}

void add_simulation( TPad * active_pad ){
  active_pad->cd();
  TLatex * int_ltx = new TLatex();
  int_ltx->SetNDC();
  int_ltx->SetTextFont(42);
  int_ltx->SetTextSize(0.038);
  int_ltx->SetTextColor(1);
  int_ltx->DrawLatexNDC( 0.315, 0.75, "Simulation" );
}


TLegend * create_atlas_legend(){
  TLegend * pad_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
  pad_legend->SetBorderSize( 0 );
  pad_legend->SetFillColor( 0 );
  pad_legend->SetFillStyle( 0 );
  pad_legend->SetTextFont( 42 );
  pad_legend->SetTextSize( 0.025 );
  return pad_legend;
}

TLegend * below_logo_legend(){
  TLegend * pad_legend = new TLegend( 0.187, 0.6, 0.4, 0.68 );
  pad_legend->SetBorderSize( 0 );
  pad_legend->SetFillColor( 0 );
  pad_legend->SetFillStyle( 0 );
  pad_legend->SetTextFont( 42 );
  pad_legend->SetTextSize( 0.025 );
  return pad_legend;
}

void style_hist( TH1 * hist, std::vector< float > & style_vec ){ 
  hist->SetMarkerStyle( style_vec.at( 0 ) );
  hist->SetMarkerColorAlpha( style_vec.at( 1 ), style_vec.at( 2 ) );
  hist->SetMarkerSize( style_vec.at( 3 ) );
  hist->SetLineStyle( style_vec.at( 4 ) );
  hist->SetLineColorAlpha( style_vec.at( 5 ), style_vec.at( 6 ) );
  hist->SetLineWidth( style_vec.at( 7 ) );
  hist->SetFillColorAlpha( style_vec.at( 8 ), style_vec.at( 9 ) );
  hist->SetFillStyle( style_vec.at( 10 ) );
}


void style_func( TF1 * func, std::vector<float> & style_vec ){
  func->SetLineStyle( style_vec.at( 0 ) );
  func->SetLineColorAlpha( style_vec.at( 1 ), style_vec.at( 2 ) );
  func->SetLineWidth( style_vec.at( 3 ) );
}

TH1F * quadrature_error_combination( TH1F * statistical, std::vector<TH1F *> systematic, bool sys_only ){
  TH1F * combined_hist = (TH1F*) statistical->Clone();
  combined_hist->Reset();
  int bins = combined_hist->GetNbinsX();
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ 
    double bin_error = 0;
    if ( !sys_only ){ bin_error += std::pow( statistical->GetBinError( bin_idx ), 2.0 ); }
    for ( TH1F * sys : systematic ){
      bin_error += std::pow( sys->GetBinError( bin_idx ), 2.0 );
    }
    combined_hist->SetBinContent( bin_idx, statistical->GetBinContent( bin_idx ) );
    combined_hist->SetBinError( bin_idx, std::sqrt( bin_error ) );
  }
  return combined_hist;
}

TPaveStats * make_stats( TH1 * hist, bool small, bool shifted ){
  if ( shifted ){ small = true; };
  double shift = 0.13 * shifted;
  double width = 0.24 / ( 1 + small );
  gPad->Update();
  TPaveStats * stats = (TPaveStats*) hist->FindObject( "stats" );
  stats->SetX1NDC( 0.56 + shift ); 
  stats->SetX2NDC( 0.56 + shift + width);
  stats->SetY1NDC( 0.55 ); 
  stats->SetY2NDC( 0.78 );
  stats->SetFillStyle( 0 ); 
  stats->SetTextFont( 42 );
  stats->SetTextSize( 0.02 * ( 2.0 / ( 2.0+small ) ) );
  stats->SetBorderSize( 1 );
  gPad->Modified();
  gPad->Update();
  return stats;
}

TPaveStats * make_ratio_stats( TH1 * hist, bool small, bool shifted ){
  if ( shifted ){ small = true; };
  double shift = 0.16 * shifted;
  double width = 0.30 / ( 1 + small );
  gPad->Update();
  TPaveStats * stats = (TPaveStats*) hist->FindObject("stats");
  stats->SetX1NDC( 0.65 + shift ); 
  stats->SetX2NDC( 0.65 + shift + width);
  stats->SetY1NDC( 0.5 ); 
  stats->SetY2NDC( 0.95 );
  stats->SetFillStyle( 0 ); 
  stats->SetTextFont( 42 );
  stats->SetTextSize( 0.045 * ( 2.0 / ( 2.0+small ) ) );
  stats->SetBorderSize( 1 );
  gPad->Modified();
  gPad->Update();
  return stats;
}

TLegend * create_stat_legend(){
  TLegend * pad_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
  pad_legend->SetBorderSize( 0 );
  pad_legend->SetFillColor( 0 );
  pad_legend->SetFillStyle( 0 );
  pad_legend->SetTextFont( 42 );
  pad_legend->SetTextSize( 0.025 );
  return pad_legend;
}


