#include "fileset.hxx"
#include "scalefactor.hxx"
#include <yaml-cpp/yaml.h>

bool basic_fileset::typecheck( fileset_type check_type ){
  if ( type == fileset_type::multi ){
    return ( std::find( multi_types.begin(), multi_types.end(), check_type ) != multi_types.end() );
  } else { return type == check_type; }

}

void basic_fileset::modify_type( fileset_type new_type ){

  if ( type == fileset_type::none ){ 
    type = new_type;
  } else { 
    multi_types.push_back( new_type );
    multi_types.push_back( type );
    type = fileset_type::multi;
  }

}

void basic_fileset::set_unique( const std::string & unique ){

  if ( !(this->unique).empty() ){ 
    if ( this->unique.compare( unique ) == 0 ){ 
      return; 
    } else {
      std::cout << "Unique of fileset is already assigned to \"" 
      << this->unique << "\", cannot reassign to \"" 
      << unique << "\"." << std::endl; 
    }
  } else { 
    this->unique = unique;
    this->unique_c = this->unique.c_str();
  }

}

const char * basic_fileset::get_unique(){
  return this->unique.c_str(); 
}


TTree * basic_fileset::get_tree( sample_type type ){
  switch( type ){
    case sign:
      return sign_tree;
    case bckg:
      return bckg_tree;
    case bbbg:
      return bbbg_tree;
   case data:
      return data_tree;
   default:
      return nullptr;
  }
}

TFile * basic_fileset::get_file( sample_type type ){
  switch( type ){
    case sign:
      return sign_file;
    case bckg:
      return bckg_file;
    case bbbg:
      return bbbg_file;
   case data:
      return data_file;
   default:
      return nullptr;
  }
}

void basic_fileset::load_efficiency_fileset( const std::string & efficiency_path, const std::string & unique, bool fiducial ){
  this->modify_type( ( fiducial ) ? fileset_type::fiducial : fileset_type::efficiency );
  this->set_unique( unique );
  this->efficiency_file = new TFile( efficiency_path.c_str(), "READ");
  this->eff_reco_tree = (TTree *) this->efficiency_file->Get( "reco_tree" );
  if (eff_reco_tree==nullptr){ std::cout << "eff_reco_tree null" << std::endl; }
  this->eff_truth_tree = (TTree *) this->efficiency_file->Get( "truth_tree" );
  if (eff_truth_tree==nullptr){ std::cout << "eff_truth_tree null" << std::endl; }
}

void basic_fileset::load_final_fileset( std::string & fileset_path, std::string & efficiency_path, std::string & unique, bool efficiency_absolute ){

  
  this->modify_type( fileset_type::final );
  this->modify_type( fileset_type::efficiency );
  this->set_unique( unique );

  this->weighted_file = new TFile( Form( "%s/weight_%s.root", fileset_path.c_str(), unique.c_str() ), "READ" );
  this->differential_file = new TFile( Form( "%s/diff_%s.root", fileset_path.c_str(), unique.c_str() ), "READ" );
  this->statistics_file = new TFile( Form( "%s/stat_%s.root", fileset_path.c_str(), unique.c_str() ), "READ" );
  if ( efficiency_absolute ){
    this->efficiency_file = new TFile( efficiency_path.c_str(), "READ");
  } else {
    this->efficiency_file = new TFile( Form( "%s/%s/efficiency/sign_efficiencies_%s.root", efficiency_path.c_str(), unique.c_str(), unique.c_str() ), "READ");
  }
  
  this->eff_reco_tree = (TTree *) this->efficiency_file->Get( "reco_tree" );
  if (eff_reco_tree==nullptr){ std::cout << "eff_reco_tree null" << std::endl; }
  this->eff_truth_tree = (TTree *) this->efficiency_file->Get( "truth_tree" );
  if (eff_truth_tree==nullptr){ std::cout << "eff_truth_tree null" << std::endl; }
  this->weighted_tree = (TTree *) this->weighted_file->Get( "tree" );
}


void basic_fileset::load_trex_results( const std::string & filepath, const std::string & unique, variable_set & variables ){

  this->modify_type( fileset_type::trex_res );

  trex_results_path = filepath.c_str();
  if ( this->unique.empty() ){ this->unique = unique; }
  trex_results_cuts = variables.analysis_bound.get_cut_series();
  trex_results_dirs = variables.analysis_bound.get_series_names();

}

void basic_fileset::load_trex_fileset( const std::string & filepath, const std::string & unique ){
  
  this->modify_type( fileset_type::trex );
  this->set_unique( unique );
  trex_file = new TFile( filepath.c_str(), "READ" );
  trex_tree = static_cast<TTree *>( trex_file->Get( "trex_results" ) );

}

void basic_fileset::process_trex_results( bool stat_only ){

  if ( stat_only ){ std::cout << "IMPLEMENT STATONLY BEHAVIOUR" << std::endl; }

  char current_path[150], output_filename[40];
  
  std::string workdir = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + unique;

  sprintf( output_filename, "./trex_processed_%s.root", unique.c_str() );
  TFile * processed_trex_results = new TFile( output_filename, "RECREATE" );
  processed_trex_results->cd();
  TTree * output_tree = new TTree( "trex_results", "trex_results" );

  fit_results trex_processed_result;
  output_tree->Branch( "trex_results", &trex_processed_result, 32000, 2 );

  RooFitResult * trex_result = nullptr;
  TFile * trex_data = nullptr;

  std::string weight_and_cut = "(muon_sf)*(photon_sf)*(pu_weight)*(1.0)";

  for ( const std::string & trex_bin : trex_results_dirs ){

    sprintf( current_path, "%s/%s/%s/Fits/%s.root", trex_results_path, trex_bin.c_str(), unique.c_str(), unique.c_str() );
    trex_data = new TFile( current_path, "READ" );
    trex_result = (RooFitResult *) trex_data->Get( "nll_simPdf_newasimovData_with_constr" );

    std::string result_path = workdir + "/" + trex_bin + "/" + unique + "/Histograms/";
    std::cout << result_path << std::endl;
    std::string output_filepath =  result_path + unique + "_histos.root";
    TFile * output_file = new TFile( output_filepath.c_str(), "READ" ); 
    TH1F * sign_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) );
    TH1F * bckg_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) );
    TH1F * data_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
    TH1F * sign_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) );
    TH1F * bckg_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) );
    TH1F * data_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );
    hist_group sr_hists( sign_sr, bckg_sr, data_sr );
    hist_group cr_hists( sign_cr, bckg_cr, data_cr );

    trex_processed_result = fit_results( trex_result, sr_hists, cr_hists );
    output_tree->Fill();
    trex_data->Close();

  }

  processed_trex_results->cd();
  output_tree->Write();
  processed_trex_results->Close();
  delete processed_trex_results;
  delete trex_data;
  delete trex_result;

}

//void basic_fileset::process_trex_yields( const std::string & unique, variable_set & variables ){
//
//  std::vector< std::string > ana_cut_series = variables.analysis_bound.get_cut_series();
//  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();
//
//  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
//  std::string input_hists = trex_path + "hists/";
//  TH1F * sign = variables.analysis_bound.get_hist();
//  TH1F * bckg = variables.analysis_bound.get_hist();
//  TH1F * sign_fit = variables.analysis_bound.get_hist();
//  TH1F * bckg_fit = variables.analysis_bound.get_hist();
//  TH1F * data = variables.analysis_bound.get_hist();
//  TH1F * fit = variables.analysis_bound.get_hist();
//
//  size_t min_bins = 0;
//  size_t max_bins = 15;
//
//  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){
//
//    const std::string & qta_name = ana_cut_names[ ana_idx ];
//    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
//    std::string table_path = trex_qta_path + "Tables/";
//    YAML::Node table_prefit_result = YAML::LoadFile( table_path + "Table_prefit.yaml" );
//    YAML::Node table_postfit_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
//
//    double sign_signal_yield = table_prefit_result[0]["Samples"][0]["Yield"].as<double>();
//    double sign_signal_error = table_prefit_result[0]["Samples"][0]["Error"].as<double>();
//    double sign_control_yield = table_prefit_result[1]["Samples"][0]["Yield"].as<double>();
//    double sign_control_error = table_prefit_result[1]["Samples"][0]["Error"].as<double>();
//    double bckg_signal_yield = table_prefit_result[0]["Samples"][1]["Yield"].as<double>();
//    double bckg_signal_error = table_prefit_result[0]["Samples"][1]["Error"].as<double>();
//    double bckg_control_yield = table_prefit_result[1]["Samples"][1]["Yield"].as<double>();
//    double bckg_control_error = table_prefit_result[1]["Samples"][1]["Error"].as<double>();
//
//    double data_signal_yield = table_prefit_result[0]["Samples"][1]["Yield"].as<double>();
//    double data_control_yield = table_prefit_result[1]["Samples"][1]["Yield"].as<double>();
//
//    double sign_fit_signal_yield = table_prefit_result[0]["Samples"][0]["Yield"].as<double>();
//    double sign_fit_signal_error = table_prefit_result[0]["Samples"][0]["Error"].as<double>();
//    double sign_fit_control_yield = table_prefit_result[1]["Samples"][0]["Yield"].as<double>();
//    double sign_fit_control_error = table_prefit_result[1]["Samples"][0]["Error"].as<double>();
//    double bckg_fit_signal_yield = table_prefit_result[0]["Samples"][1]["Yield"].as<double>();
//    double bckg_fit_signal_error = table_prefit_result[0]["Samples"][1]["Error"].as<double>();
//    double bckg_fit_control_yield = table_prefit_result[1]["Samples"][1]["Yield"].as<double>();
//    double bckg_fit_control_error = table_prefit_result[1]["Samples"][1]["Error"].as<double>();
//
//    double fit_signal_yield = table_postfit_result[0]["Samples"][2]["Yield"].as<double>();
//    double fit_signal_error = table_postfit_result[0]["Samples"][2]["Error"].as<double>();
//    double fit_control_yield = table_postfit_result[1]["Samples"][2]["Yield"].as<double>();
//    double fit_control_error = table_postfit_result[1]["Samples"][2]["Error"].as<double>();
//
//    sign->SetBinContent( ana_idx+1, sign_signal_yield + sign_control_yield );
//    sign->SetBinError( ana_idx+1, std::sqrt( sign_signal_error * sign_signal_error
//                        + sign_control_error * sign_control_error ) );
//
//    bckg->SetBinContent( ana_idx+1, bckg_signal_yield + bckg_control_yield );
//    bckg->SetBinError( ana_idx+1, std::sqrt( bckg_signal_error * bckg_signal_error
//                        + bckg_control_error * bckg_control_error ) );
//
//    sign_fit->SetBinContent( ana_idx+1, sign_fit_signal_yield + sign_fit_control_yield );
//    sign_fit->SetBinError( ana_idx+1, std::sqrt( sign_fit_signal_error * sign_fit_signal_error
//                        + sign_fit_control_error * sign_fit_control_error ) );
//
//    bckg_fit->SetBinContent( ana_idx+1, bckg_fit_signal_yield + bckg_fit_control_yield );
//    bckg_fit->SetBinError( ana_idx+1, std::sqrt( bckg_fit_signal_error * bckg_fit_signal_error
//                        + bckg_fit_control_error * bckg_fit_control_error ) );
//
//    data->SetBinContent( ana_idx+1, data_signal_yield + data_control_yield );
//    data->SetBinError( ana_idx+1, std::sqrt( data_signal_yield + data_control_yield ) );
//
//    fit->SetBinContent( ana_idx+1, fit_signal_yield + fit_control_yield );
//    fit->SetBinError( ana_idx+1, std::sqrt( fit_signal_error * fit_signal_error
//                        + fit_control_error * fit_control_error ) );
//
//  }
//
//
//}


hist_group basic_fileset::recreate_prefit_histograms( variable_set & variables, int bin, const std::string & cut, const std::string & weight, region signal_region ){
  
  if ( !typecheck( fileset_type::subtraction ) || bin < 1 || !typecheck( fileset_type::trex_res ) ){ return hist_group(); }

  char group_name[100], draw_expression[150], cut_and_weight[300], data_cut[300];
  sprintf( cut_and_weight, "((subtraction_weight)*(%s))*((%s)&&(%s)&&(Lambda>=25.00000)&&(Lambda<=200.00000))", weight.c_str(), cut.c_str(), region_cuts[signal_region] );
  sprintf( data_cut,       "((subtraction_weight))*((%s)&&(%s)&&(Lambda>=25.00000)&&(Lambda<=200.00000))", cut.c_str(), region_cuts[signal_region] );

  //std::cout << region_cuts[ signal_region] << std::endl;
  std::cout << "Prefit sibg cut: " << cut_and_weight << std::endl;
  std::cout << "Prefit data cut: " << data_cut << std::endl;

  hist_group prefit_group;  
  sprintf( group_name, "sign_pf_%s_%i", unique.c_str(), bin );
  sprintf( draw_expression, "%s>>%s", variables.spectator_variable.c_str(), group_name );
  prefit_group.sign_hist = variables.spectator_bound.get_hist( group_name );
  prefit_group.sign_hist->Sumw2();
  sign_tree->Draw( draw_expression, cut_and_weight, "e goff" );

  sprintf( group_name, "bckg_pf_%s_%i", this->unique.c_str(), bin );
  sprintf( draw_expression, "%s>>%s", variables.spectator_variable.c_str(), group_name );
  prefit_group.bckg_hist = variables.spectator_bound.get_hist( group_name );
  prefit_group.bckg_hist->Sumw2();
  bckg_tree->Draw( draw_expression, cut_and_weight, "e goff" );

  sprintf( group_name, "data_pf_%s_%i", this->unique.c_str(), bin );
  sprintf( draw_expression, "%s>>%s", variables.spectator_variable.c_str(), group_name );
  prefit_group.data_hist = variables.spectator_bound.get_hist( group_name );
  prefit_group.data_hist->Sumw2();
  data_tree->Draw( draw_expression, data_cut, "e goff" );
  
  return prefit_group;

}


void basic_fileset::load_subtraction_fileset( std::string & fileset_path, std::string & unique, bool split ){
  

  this->set_unique( unique );
  this->modify_type( fileset_type::subtraction );


  this->sign_file = new TFile( Form( "%s/%ssign_%s.root", fileset_path.c_str(), split ? "sign/" : "", unique.c_str() ), "READ" ); 
  this->bckg_file = new TFile( Form( "%s/%sbckg_%s.root", fileset_path.c_str(), split ? "bckg/" : "", unique.c_str() ), "READ" ); 
  this->bbbg_file = new TFile( Form( "%s/%sbbbg_%s.root", fileset_path.c_str(), split ? "bbbg/" : "", unique.c_str() ), "READ" ); 
  this->data_file = new TFile( Form( "%s/%sdata_%s.root", fileset_path.c_str(), split ? "data/" : "", unique.c_str() ), "READ" ); 
  this->sign_tree = (TTree *) sign_file->Get( "tree" ); 
  this->bckg_tree = (TTree *) bckg_file->Get( "tree" );
  this->bbbg_tree = (TTree *) bbbg_file->Get( "tree" );
  this->data_tree = (TTree *) data_file->Get( "tree" );
}

void basic_fileset::load_stats_fileset( const std::string & filepath, const std::string & unique ){

  this->modify_type( fileset_type::stats );

  this->unique = unique;
  this->statistics_file = new TFile( filepath.c_str(), "READ" );

}

void basic_fileset::close_subtraction_fileset(){
  if ( sign_tree ){ delete sign_tree; }
  if ( bckg_tree ){ delete bckg_tree; }
  if ( bbbg_tree ){ delete bbbg_tree; }
  if ( data_tree ){ delete data_tree; }
  if ( sign_file ){  sign_file->Close(); delete sign_file; }
  if ( bckg_file ){  bckg_file->Close(); delete bckg_file; }
  if ( bbbg_file ){  bbbg_file->Close(); delete bbbg_file; }
  if ( data_file ){  data_file->Close(); delete data_file; }
}

void basic_fileset::load_predefined_fileset( const std::string & predefined_filepath, const std::string & unique, fileset_type type ){

  predefined = true;
  this->type = type;

  this->unique = unique;
  this->predefined_file = new TFile( predefined_filepath.c_str(), "READ" ); 
  this->predefined_tree = (TTree *) predefined_file->Get( "tree" );

}

TH1F * basic_fileset::get_efficiency( variable_set & variables, const std::string & extra_cut ){

  const char * analysis_variable = variables.analysis_variable.c_str();
  const char * mass = ( variables.mass.empty() ) ? "Q12" : variables.mass.c_str();
  std::string mass_cut = variables.bound_manager->get_bound( mass ).get_cut();
  std::string efficiency_string = Form( "eff_%s_%s", analysis_variable, mass );
  if ( !extra_cut.empty() ){ mass_cut += "&&(" + variables.bound_manager->get_bound( extra_cut ).get_cut() + ")"; }
  TH1F * reco_hist = variables.analysis_bound.get_hist( "reco" ); 
  TH1F * truth_hist = variables.analysis_bound.get_hist( "truth" ); 
  TH1F * eff_hist = variables.analysis_bound.get_hist( Form( "eff_%s_%s_%s", mass, analysis_variable, unique.c_str() ) ); 

  eff_reco_tree->Draw( Form( "%s>>reco", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_truth_tree->Draw( Form( "%s>>truth", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" );

  return eff_hist;

}

std::vector< TH1F * > basic_fileset::get_efficiency_hists( variable_set & variables, const std::string & extra_cut ){

  const char * analysis_variable = variables.analysis_variable.c_str();
  const char * mass = ( variables.mass.empty() ) ? "Q12" : variables.mass.c_str();
  std::string mass_cut = variables.bound_manager->get_bound( mass ).get_cut();
  std::string efficiency_string = Form( "eff_%s_%s", analysis_variable, mass );
  if ( !extra_cut.empty() ){ mass_cut += "&&(" + variables.bound_manager->get_bound( extra_cut ).get_cut() + ")"; }
  TH1F * reco_hist = variables.analysis_bound.get_hist( "reco" ); 
  TH1F * truth_hist = variables.analysis_bound.get_hist( "truth" ); 
  TH1F * eff_hist = variables.analysis_bound.get_hist( Form( "eff_%s_%s_%s", mass, analysis_variable, unique.c_str() ) ); 

  eff_reco_tree->Draw( Form( "%s>>reco", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_truth_tree->Draw( Form( "%s>>truth", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" );

  std::string efftype = typecheck( fileset_type::fiducial) ? "fiducial" : "selection";
  std::cout << "Generated " <<  efftype << " efficiency" << std::endl;

  return { truth_hist, reco_hist, eff_hist };
}




void basic_fileset::apply_sign_efficiency( TH1F * sign, variable_set & variables ){

  const char * analysis_variable = variables.analysis_variable.c_str();
  const char * mass = ( variables.mass.empty() ) ? "Q12" : variables.mass.c_str();
  std::string mass_cut = variables.bound_manager->get_bound( mass ).get_cut();
  std::string efficiency_string = Form( "eff_%s_%s", analysis_variable, mass );

  if ( this->efficiency_file != nullptr ){
    std::cout << "Signal efficiency correction in \"" << this->unique << "\" applied." << std::endl;
  } else {
    std::cout << "WARNING: Efficiency in \"" << unique << "\" not found. Continuing without correction." << std::endl;
    return;
  }

  TH1F * reco_hist = variables.analysis_bound.get_hist( "reco" ); 
  TH1F * truth_hist = variables.analysis_bound.get_hist( "truth" ); 
  TH1F * eff_hist = variables.analysis_bound.get_hist( Form( "eff_%s_%s_%s", mass, analysis_variable, unique.c_str() ) ); 

  eff_reco_tree->Draw( Form( "%s>>reco", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_truth_tree->Draw( Form( "%s>>truth", analysis_variable ), mass_cut.c_str(), "goff" );
  eff_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" );
  sign->Divide( eff_hist );

  delete reco_hist;
  delete truth_hist;

}

    
// NO EFFECT ON ERROR
void basic_fileset::apply_scalefactor_correction( TH1F * sign, variable_set & variables, fileset_type sf_correction_type, const std::string & variable ){

  std::string sf_string;
  if ( sf_correction_type == sf_muon ){
    sf_string = "SF";
    std::cout << "Applying muon SF correction to \"" << sign->GetName() <<  "\" histogram" << std::endl;
  } else if ( sf_correction_type == sf_photon ){
    sf_string = "photon_id_sf"; 
    std::cout << "Applying photon_id SF correction to \"" << sign->GetName() <<  "\" histogram" << std::endl;
  } else { return; }

  const char * mass_c = ( variables.mass.empty() ) ? "Q12" : variables.mass.c_str();
  const char * variable_c = ( variable.empty() ) ? variables.analysis_variable.c_str() : variable.c_str();
  bound & variable_bound = variables.bound_manager->get_bound( variable_c );
  std::string mass_cut = variables.bound_manager->get_bound( mass_c ).get_cut();
  const char * mass_cut_c = mass_cut.c_str();
  std::vector< std::string > cut_series = variable_bound.get_cut_series();

  TH1F * temp_hist = variable_bound.get_hist( "temp" );
  for ( int bin = 1; bin < variable_bound.get_bins(); bin++ ){
    predefined_tree->Draw( Form( "%s>>temp", sf_string.c_str() ), Form( "%s&&%s", mass_cut_c, cut_series[bin].c_str() ), "goff" );
    sign->SetBinContent( bin, sign->GetBinContent( bin )/temp_hist->GetMean() );
    temp_hist->Reset();
  }

  delete temp_hist;

}
  

hist_group basic_fileset::recreate_histograms_subtracted( variable_set & variables, bool efficiency_correct_signal, bool normalise, std::string variable, bool scalefactor){
      
  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }
  if ( scalefactor ){ tag += "_sf"; }

  bound active_bound = ( variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( variable );

  TH1F * data_hist = active_bound.get_hist( "data_subt_recomb_" + tag );
  TH1F * sign_hist = active_bound.get_hist( "sign_subt_recomb_" + tag );
  TH1F * bckg_hist = active_bound.get_hist( "bckg_subt_recomb_" + tag );

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }
  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();

  std::string draw_weight = "(subtraction_weight)";
  draw_weight += "*("+ mass_cut;
  if ( scalefactor ){ draw_weight += ( "&&" + std::string( this->get_phot_sf_weight() ) ); }
  draw_weight += ")";

  char sign_draw[150], bckg_draw[150], data_draw[150];
  sprintf( sign_draw, "%s>>sign_subt_recomb_%s", active_bound.get_var().c_str(), tag.c_str() );
  sprintf( bckg_draw, "%s>>bckg_subt_recomb_%s", active_bound.get_var().c_str(), tag.c_str() );
  sprintf( data_draw, "%s>>data_subt_recomb_%s", active_bound.get_var().c_str(), tag.c_str() );


  sign_tree->Draw( sign_draw, Form( "(subtraction_weight)*(%s)", draw_weight.c_str() ), "goff" );
  bckg_tree->Draw( bckg_draw, Form( "(subtraction_weight)*(%s)", draw_weight.c_str() ), "goff" );
  data_tree->Draw( data_draw, Form( "(subtraction_weight)*(%s)", draw_weight.c_str() ), "goff" );
  
  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }

  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }

  hist_group hists = hist_group();
  hists.data_hist = data_hist;
  hists.sign_hist = sign_hist;
  hists.bckg_hist = bckg_hist;
  return hists;

}

TH1F * basic_fileset::get_error( variable_set & variables, const std::string & variable ){

  if ( !predefined ){
    std::cout << "WARNING: Not in predefined error mode, get error has no meaning, returning empty hist." << std::endl;
    return variables.analysis_bound.get_hist();

  }
  if ( type == fileset_type::sf_muon ||  std::find( multi_types.begin(), multi_types.end(), fileset_type::sf_muon ) != multi_types.end() ){
    scalefactor photon_muon_sf;
    photon_muon_sf.load_photon_SF();
    photon_muon_sf.load_muon_SF();
    TH1F * sf_error = photon_muon_sf.error_muon_SF( predefined_tree, variables, variable, variables.mass );
    return sf_error;
  } else if ( type == fileset_type::sf_photon || std::find( multi_types.begin(), multi_types.end(), fileset_type::sf_photon ) != multi_types.end() ){
    scalefactor photon_muon_sf;
    photon_muon_sf.load_photon_SF();
    photon_muon_sf.load_muon_SF();
    TH1F * sf_error = photon_muon_sf.error_photon_SF( predefined_tree, variables, variable, variables.mass );
    return sf_error;
  } else {
    std::cout << "WARNING: get_error only implemented for predefined scalefactor mode, returning empty hist" << std::endl;
    return variables.analysis_bound.get_hist();
  }

}



TH1F * basic_fileset::get_weighted_histogram( sample_type type, variable_set & variables, const std::string & variable, bool efficiency_correct_signal, bool normalise, const std::string & name ){

  if( type != sign && type != bckg ){
    std::cout << "WARNING: Sample type is not set to sign or bckg, returning sign." << std::endl;
    type = sign;
  }

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }

  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  tag += "_" + variable;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }
  const char * hist_name = ( name.empty() ) ? Form( "weight_recomb_%s", tag.c_str() ) : name.c_str();

  bound active_bound = ( variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( variable );
  TH1F * hist = active_bound.get_hist( hist_name );

  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();

  char * draw_char = Form( "%s>>%s", active_bound.get_var().c_str(), hist_name );
  char * weight = Form( "(subtraction_weight*hf_%s_weight)*(%s)", type_c[type], mass_cut.c_str() );
  weighted_tree->Draw( draw_char, weight, "goff" );

  if ( type == sign && efficiency_correct_signal ){ apply_sign_efficiency( hist, variables ); }
  if ( normalise ){ hist->Scale( 1.0/hist->Integral() ); }

  return hist;

}

TH1F * basic_fileset::get_subtracted_histogram( sample_type type, variable_set & variables, const std::string & variable, bool efficiency_correct_signal, bool normalise, const std::string & name ){

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }

  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  tag += "_" + variable;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }
  const char * hist_name = ( name.empty() ) ? Form( "subtr_%s", tag.c_str() ) : name.c_str();


  bound active_bound = ( variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( variable );
  TH1F * hist = active_bound.get_hist( hist_name );

  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();

  char * draw_char = Form( "%s>>%s", active_bound.get_var().c_str(), hist_name );
  char * weight = Form( "(subtraction_weight)*(%s)", mass_cut.c_str() );
  weighted_tree->Draw( draw_char, weight, "goff" );

  if ( type == sign && efficiency_correct_signal ){ apply_sign_efficiency( hist, variables ); }
  if ( normalise ){ hist->Scale( 1.0/hist->Integral() ); }

  return hist;

}


hist_group basic_fileset::recreate_histograms_weights( variable_set & variables, bool efficiency_correct_signal, bool normalise, std::string variable ){
    
  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }

  bound active_bound = ( variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( variable );

  TH1F * data_hist = active_bound.get_hist( Form( "data_weightrecomb_%s", tag.c_str() ) );
  TH1F * sign_hist = active_bound.get_hist( Form( "sign_weightrecomb_%s", tag.c_str() ) );
  TH1F * bckg_hist = active_bound.get_hist( Form( "bckg_weightrecomb_%s", tag.c_str() ) );
  
  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }
  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();
  
  weighted_tree->Draw( Form( "%s>>sign_weightrecomb_%s", active_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  weighted_tree->Draw( Form( "%s>>bckg_weightrecomb_%s", active_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  weighted_tree->Draw( Form( "%s>>data_weightrecomb_%s", active_bound.get_var().c_str(), tag.c_str() ),   
                       Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );

  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }
  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }
  
  hist_group hists = hist_group();
  hists.data_hist = data_hist;
  hists.sign_hist = sign_hist;
  hists.bckg_hist = bckg_hist;
  return hists;
    
}

hist_group_2d basic_fileset::recreate_map_histograms_weights( variable_set & variables, bool normalise, std::string x_variable, std::string y_variable ){


  bound x_bound = ( x_variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( x_variable );
  bound y_bound = ( y_variable.empty() ) ? variables.spectator_bound : variables.bound_manager->get_bound( y_variable );

  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  if ( normalise ){ tag += "_norm"; }

  TH2F * data_hist = x_bound.get_2d_hist( Form( "data_weight_map_%s", tag.c_str() ), y_bound );
  TH2F * sign_hist = x_bound.get_2d_hist( Form( "sign_weight_map_%s", tag.c_str() ), y_bound );
  TH2F * bckg_hist = x_bound.get_2d_hist( Form( "bckg_weight_map_%s", tag.c_str() ), y_bound );

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }
  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();

  weighted_tree->Draw( Form( "%s:%s>>sign_weight_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  weighted_tree->Draw( Form( "%s:%s>>bckg_weight_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  weighted_tree->Draw( Form( "%s:%s>>data_weight_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ),   
                       Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );

  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }

  hist_group_2d maps;
  maps.data_hist = data_hist;
  maps.sign_hist = sign_hist;
  maps.bckg_hist = bckg_hist;
  return maps;

}

hist_group_2d basic_fileset::recreate_map_histograms_subtraction( variable_set & variables, bool normalise, std::string x_variable, std::string y_variable ){

  bound x_bound = ( x_variable.empty() ) ? variables.analysis_bound : variables.bound_manager->get_bound( x_variable );
  bound y_bound = ( y_variable.empty() ) ? variables.spectator_bound : variables.bound_manager->get_bound( y_variable );

  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  if ( normalise ){ tag += "_norm"; }

  TH2F * data_hist = x_bound.get_2d_hist( Form( "data_subt_map_%s", tag.c_str() ), y_bound );
  TH2F * sign_hist = x_bound.get_2d_hist( Form( "sign_subt_map_%s", tag.c_str() ), y_bound );
  TH2F * bckg_hist = x_bound.get_2d_hist( Form( "bckg_subt_map_%s", tag.c_str() ), y_bound );

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }
  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();

  sign_tree->Draw( Form( "%s:%s>>sign_subt_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  bckg_tree->Draw( Form( "%s:%s>>bckg_subt_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );
                                                                                                          
  data_tree->Draw( Form( "%s:%s>>data_subt_map_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), tag.c_str() ),   
                       Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );

  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }

  hist_group_2d maps;
  maps.data_hist = data_hist;
  maps.sign_hist = sign_hist;
  maps.bckg_hist = bckg_hist;
  return maps;

}

hist_group basic_fileset::recreate_histograms_differential( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
    
  bool error_available = true;
  std::string tag = variables.mass + "_" + this->unique;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "effc"; }
  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_diffrecomb_%s", tag.c_str() ) );
  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_diffrecomb_%s", tag.c_str() ) );
  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_diffrecomb_%s", tag.c_str() ) );
  TList * key_list = this->differential_file->GetListOfKeys();

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }

  for ( int analysis_bin = 1; analysis_bin <= variables.analysis_bound.get_bins(); analysis_bin++ ){

    std::string bin_string = variables.mass + "_" + std::to_string( analysis_bin );
    std::string err_hist_name = "errh_" + variables.mass + "_" + std::to_string( analysis_bin );

    TH1D * data_bin_hist = (TH1D *) this->differential_file->Get( Form( "data_%s", bin_string.c_str()  ) );
    TH1D * sign_bin_hist = (TH1D *) this->differential_file->Get( Form( "sign_%s", bin_string.c_str()  ) );
    TH1D * bckg_bin_hist = (TH1D *) this->differential_file->Get( Form( "bckg_%s", bin_string.c_str()  ) );
    data_hist->SetBinContent( analysis_bin, data_bin_hist->Integral());
    sign_hist->SetBinContent( analysis_bin, sign_bin_hist->Integral());
    bckg_hist->SetBinContent( analysis_bin, bckg_bin_hist->Integral());
    if ( error_available && key_list->Contains( Form( "errh_%s", bin_string.c_str() ) ) ){
      TH1D * error_hist = (TH1D *) this->differential_file->Get( Form( "errh_%s", bin_string.c_str()  ) );
      data_hist->SetBinError( analysis_bin , error_hist->GetBinError( 1 ) );
      sign_hist->SetBinError( analysis_bin , error_hist->GetBinError( 2 ) );
      bckg_hist->SetBinError( analysis_bin , error_hist->GetBinError( 3 ) );
    } else { 
      error_available = false;
      std::cout << "WARNING: Errors unavailable for \"" << variables.mass << "\" in \"" << this->unique << "\", using Sumw2()" << std::endl;
    }
  }
  if ( !error_available ){ 
    data_hist->Sumw2(); 
    sign_hist->Sumw2();
    bckg_hist->Sumw2();
  }

  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }

  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }

  hist_group hists = hist_group();
  hists.data_hist = data_hist;
  hists.sign_hist = sign_hist;
  hists.bckg_hist = bckg_hist;
  return hists;

}

hist_group basic_fileset::recreate_histograms_trex( variable_set & variables, bool efficiency_correct_signal, bool normalise ){

  std::string tag = variables.mass + "_" + this->unique;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }
  
  hist_group hists = hist_group();
  hists.data_hist = variables.analysis_bound.get_hist( Form( "data_statrecomb_%s", tag.c_str() ) );
  hists.sign_hist = variables.analysis_bound.get_hist( Form( "sign_statrecomb_%s", tag.c_str() ) );
  hists.bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_statrecomb_%s", tag.c_str() ) );

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }

  fit_results * results_entry = new fit_results();
  trex_tree->SetBranchAddress( "trex_results", &results_entry );

  for ( int analysis_bin = 0; analysis_bin < variables.analysis_bound.get_bins(); analysis_bin++ ){
    trex_tree->GetEntry( analysis_bin );
    hists.data_hist->SetBinContent( analysis_bin+1, (*results_entry).data ); 
    hists.data_hist->SetBinError(   analysis_bin+1, (*results_entry).data_error ); 
    hists.sign_hist->SetBinContent( analysis_bin+1, (*results_entry).sign_fit ); 
    hists.sign_hist->SetBinError(   analysis_bin+1, (*results_entry).sign_fit_error ); 
    hists.bckg_hist->SetBinContent( analysis_bin+1, (*results_entry).bckg_fit ); 
    hists.bckg_hist->SetBinError(   analysis_bin+1, (*results_entry).bckg_fit_error ); 
  }

  if ( efficiency_correct_signal ){ apply_sign_efficiency( hists.sign_hist, variables ); }
  if ( normalise ){
    hists.data_hist->Scale( 1.0/hists.data_hist->Integral() );    
    hists.sign_hist->Scale( 1.0/hists.sign_hist->Integral() );    
    hists.bckg_hist->Scale( 1.0/hists.bckg_hist->Integral() );    
  }

  return hists;

}

hist_group basic_fileset::recreate_histograms_statistical( variable_set & variables, bool efficiency_correct_signal, bool normalise ){
    
  std::string tag = variables.mass + "_" + this->unique;
  if ( normalise ){ tag += "_norm"; }
  if ( efficiency_correct_signal ){ tag += "_effc"; }
  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "data_statrecomb_%s", tag.c_str() ) );
  TH1F * sign_hist = variables.analysis_bound.get_hist( Form( "sign_statrecomb_%s", tag.c_str() ) );
  TH1F * bckg_hist = variables.analysis_bound.get_hist( Form( "bckg_statrecomb_%s", tag.c_str() ) );
  TList * key_list = this->statistics_file->GetListOfKeys();

  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }

  if ( !key_list->Contains( variables.mass.c_str() ) ){
    std::cout << "WARNING: Statistics set \"" << variables.mass << "\" in \"" << this->unique << "\" not stored, returning empty hist group." << std::endl;
    hist_group hists = hist_group();
    return hists;
  }
  TTree * results_tree = (TTree *) this->statistics_file->Get( variables.mass.c_str() );

  fit_results * results_entry = new fit_results();
  results_tree->SetBranchAddress( "fit_stats", &results_entry );
  for ( int analysis_bin = 0; analysis_bin < variables.analysis_bound.get_bins(); analysis_bin++ ){
    results_tree->GetEntry( analysis_bin );
    data_hist->SetBinContent( analysis_bin+1, (*results_entry).data ); 
    data_hist->SetBinError(   analysis_bin+1, (*results_entry).data_error ); 
    sign_hist->SetBinContent( analysis_bin+1, (*results_entry).sign_fit ); 
    sign_hist->SetBinError(   analysis_bin+1, (*results_entry).sign_fit_error ); 
    bckg_hist->SetBinContent( analysis_bin+1, (*results_entry).bckg_fit ); 
    bckg_hist->SetBinError(   analysis_bin+1, (*results_entry).bckg_fit_error ); 
  }

  if ( efficiency_correct_signal ){ apply_sign_efficiency( sign_hist, variables ); }

  if ( normalise ){
    data_hist->Scale( 1.0/data_hist->Integral() );    
    sign_hist->Scale( 1.0/sign_hist->Integral() );    
    bckg_hist->Scale( 1.0/bckg_hist->Integral() );    
  }

  hist_group hists = hist_group();
  hists.data_hist = data_hist;
  hists.sign_hist = sign_hist;
  hists.bckg_hist = bckg_hist;
  return hists;
}

TH1F * basic_fileset::get_subtracted_data( variable_set & variables, bool normalise ){
  std::string tag = variables.mass + "_" + this->unique + "_" + std::to_string( rand()%1000 + 1 ) ;
  if ( normalise ){ tag += "_norm"; }
  TH1F * data_hist = variables.analysis_bound.get_hist( Form( "sub_data_%s", tag.c_str() ) );
  if ( variables.mass.empty() ){ 
    std::cout << "WARNING: mass bound not set for variable_set object, selecting and setting Q12 bound" << std::endl;
    variables.mass = "Q12";
  }
  std::string mass_cut = variables.bound_manager->get_bound( variables.mass ).get_cut();
  weighted_tree->Draw( Form( "%s>>sub_data_%s", variables.analysis_bound.get_var().c_str(), tag.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ), "goff" );
  if ( normalise ){ data_hist->Scale( 1.0/data_hist->Integral() ); }
  return data_hist;
}

