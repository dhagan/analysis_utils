#include <common.hxx>

region region_convert( std::string input_region ){
  uint8_t dist = std::distance( region_str.begin(), std::find( region_str.begin(), region_str.end(), input_region ) ); 
  return static_cast<region>( dist );
}

sample_type sample_type_convert( std::string input_type ){
  uint8_t dist = std::distance( type_str.begin(), std::find( type_str.begin(), type_str.end(), input_type) ); 
  return static_cast<sample_type>( dist );
}
