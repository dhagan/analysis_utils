#include <variable_set.hxx>

variable_set::variable_set( const std::string & manager_filepath, const std::string & ranges, const std::string & analysis_variable, const std::string & spectator_variable, const std::string & mass ){
  this->mass = ( mass.empty() ) ? "Q12" : mass;
  this->bound_manager = new bound_mgr();
  this->bound_manager->load_bound_mgr( manager_filepath );
  if ( !ranges.empty() ){ this->bound_manager->process_bounds_string( ranges ); }
  if ( !analysis_variable.empty() ){ 
    this->analysis_variable = analysis_variable;
    this->analysis_bound = this->bound_manager->get_bound( this->analysis_variable );
  }
  if ( !spectator_variable.empty() ){ 
    this->spectator_variable = spectator_variable;
    this->spectator_bound = this->bound_manager->get_bound( this->spectator_variable );
  }
}

void variable_set::set_extra_bounds( std::string & extra_variables, bool replace_current ){

  if( replace_current ){
    this->extra_variables = extra_variables;
  } else {
    this->extra_variables += ":" + extra_variables;
  }
  this->extra_bounds.clear();
  std::vector< std::string > extra_bounds_names;
  split_strings( extra_bounds_names, this->extra_variables, ":" ); 
  auto converter = [ this ] ( std::string & bound_str ){ return this->bound_manager->get_bound( bound_str ); };
  std::transform( extra_bounds_names.begin(), extra_bounds_names.end(), std::back_inserter( this->extra_bounds ), converter  );
  
}
