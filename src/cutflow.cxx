#include <cutflow.hxx>

std::vector< double > cutflow::get_counts(){
  std::vector< double > counts = {};
  counts.reserve( cut_distributions.size() );
  for ( int bin = 1; bin <= cutflow_hist->GetNbinsX(); bin++ ){ counts.push_back( cutflow_hist->GetBinContent( bin ) ); }
  return counts;
}

// write out cutflow histograms
// makes use of utils and style functions in library
void cutflow::write( std::string unique, bound var_bound ){

  prep_style();
  gStyle->SetOptStat( "inrm" );
  std::vector<float> var_style({ 0, 0, 0, 0, 1, kBlue+1, 1, 1, 0, 0, 0} );

  hist_store * cutflow_store = new hist_store();


  //for ( std::string & cut_name : cut_names ){ std::cout << cut_name << std::endl; }
 
  // individial cut distributions
  for ( int cut_idx = 0; cut_idx < cuts; cut_idx++ ){

    TH1 * var_hist = cut_distributions.at( cut_idx );

    TCanvas * var_canvas = new TCanvas(Form( "var_canv_%i",cut_idx ),"",600,600,1500,1500);
    var_canvas->Divide(1);
    TPad * var_pad = (TPad*) var_canvas->cd(1);

    var_hist->Draw( "HIST" );
    hist_prep_axes( var_hist );
    set_axis_labels( var_hist, var_bound.get_x_str(), Form("Cut %i Entries%s", 
                    cut_idx, var_bound.get_y_str().c_str() ) );
    style_hist( var_hist, var_style );
    TPaveStats * var_stats = make_stats( var_hist );
    var_stats->Draw();
    add_atlas_decorations( var_pad, true, false );
    std::string var_title(Form("Cut %i, %s, %s",cut_idx, cut_names[cut_idx].c_str(), var_bound.get_ltx().c_str() ) );
    add_pad_title( var_pad, var_title, true );

    //std::cout << cut_names[cut_idx] << std::endl;
    var_canvas->SaveAs( Form("cutflow/%s_cut-%i.png", unique.c_str(), cut_idx ) );
    cutflow_store->add_hist( var_hist, Form("%s_cut-%i", unique.c_str(), cut_idx ) );
    var_hist->GetYaxis()->SetRangeUser( 1, var_hist->GetMaximum()*100 );
    gPad->SetLogy();
    var_canvas->SaveAs( Form("cutflow/%s_cut-%i_log.png", unique.c_str(), cut_idx ) );
    delete var_canvas;

  }

  // Overlain cuts
  gStyle->SetOptStat( 0000000 );
  TCanvas * ovly_canvas = new TCanvas( Form( "var_canv_ovly" ), "", 600, 600, 1500, 1500 );
  ovly_canvas->Divide(1);
  TPad * var_pad = (TPad*) ovly_canvas->cd(1);
  TH1 * var_hist = cut_distributions.at( 2 );
  var_hist->Draw( "HIST" );
  hist_prep_axes( var_hist, true );
  set_axis_labels( var_hist, var_bound.get_x_str(), Form("Ovly Entries%s", var_bound.get_y_str().c_str() ) );
  style_hist( var_hist, var_style );
  add_atlas_decorations( var_pad, true, false );
  add_pad_title( var_pad, Form( "All cuts %s", var_bound.get_ltx().c_str() ), true );


  for ( int cut_idx = 0; cut_idx < cuts; cut_idx++ ){
    TH1 * ovly_hist = cut_distributions.at( cut_idx );
    ovly_hist->Draw( "HIST SAME" );
    style_hist( ovly_hist, var_style );
    ovly_hist->SetLineColorAlpha( kBlue + 1, 1.00 - cut_idx*0.05 );
  }
  ovly_canvas->SaveAs( Form("cutflow/%s_ovly_cut.png", unique.c_str() ) );

  var_hist->GetYaxis()->SetRangeUser( 1, var_hist->GetMaximum()*100 );
  gPad->SetLogy();
  ovly_canvas->SaveAs( Form("cutflow/%s_ovly_cut_log.png", unique.c_str() ) );
  delete ovly_canvas;


  


  // cutflow
  //std::cout << "start full cutflow " << std::endl; 
  gStyle->SetOptStat( "inrm" );
  TCanvas * cutflow_canvas = new TCanvas("cutflow_canvas","",600,600,3000,1500);
  cutflow_canvas->Divide(1);

  TPad * cutflow_pad = (TPad*) cutflow_canvas->cd(1);
  cutflow_hist->Draw("HIST");

  hist_prep_axes( cutflow_hist );
  set_axis_labels( cutflow_hist, "Cut", "Events passed" );
  add_pad_title( cutflow_pad, "Cutflow, event counts", true );

  TAxis * cutflow_axis = (TAxis*) cutflow_hist->GetXaxis();
  for (int bin_no = 1; bin_no <= ( (int) cut_names.size() ); ++bin_no){
    cutflow_axis->SetBinLabel( bin_no, ( cut_names[bin_no-1] ).c_str());
    cutflow_axis->SetLabelSize( 0.025 );
    cutflow_axis->ChangeLabel( bin_no, -45.0 ); // -90
  }
  TPaveStats * cf_stats = make_stats( cutflow_hist );
  cf_stats->Draw(); 
  add_atlas_decorations( cutflow_pad, true );
  gPad->SetGrid( 1, 0 );

  cutflow_canvas->SaveAs( Form( "cutflow/%s_cutflow.png", unique.c_str() ) );
  //std::cout << "finish full cutflow " << std::endl; 



  cutflow_store->add_hist( cutflow_hist, Form("%s_cutflow", unique.c_str() ) );
  cutflow_hist->GetYaxis()->SetRangeUser( 1, cutflow_hist->GetMaximum()*10000 );
  gPad->SetLogy();
  cutflow_canvas->SaveAs( Form( "cutflow/%s_cutflow_log.png", unique.c_str() ) );



	auto cut_delimit = []( std::string acc, std::string cut ){
    return  std::string( std::move( acc ) + ":" + cut );
  };
  std::string cutflow_string = std::accumulate( cut_names.begin()+1, cut_names.end(), cut_names.at(0), cut_delimit );
  std::string cutflow_config = "cutflow_config";
  cutflow_string += "#" + this->var_str;
  cutflow_store->add_string( cutflow_config, cutflow_string );
  cutflow_store->write( Form( "cutflow/%s_cutflow.root", unique.c_str() ) );

  cutflow_canvas->Close();
  delete cutflow_canvas;

}

void cutflow::load_cutflow( std::string & filepath, std::string & unique ){

  TFile * cutflow_file = new TFile( filepath.c_str(), "READ" );
  
  TNamed * cutflow_string = (TNamed *) cutflow_file->Get( "cutflow_config" );

	std::vector< std::string > config_vec;
  std::string cutflow_config = std::string( cutflow_string->GetTitle() );
	split_strings( config_vec, cutflow_config, "#" );
	split_strings( this->cut_names, config_vec.at( 0 ), ":" );
  this->cuts = this->cut_names.size();
  this->cutflow_hist = (TH1F *) cutflow_file->Get( Form( "%s_cutflow", unique.c_str() ) );
  this->var_str = config_vec.at( 1 );
  this->current_cut = 0;
  
  for ( int idx = 0; idx <= (int) cut_names.size(); idx++ ){
    std::string dist_name = unique + "_cut-" + std::to_string( idx );
    TH1 * stored_histogram = (TH1*) cutflow_file->Get( dist_name.c_str() );
    cut_distributions.push_back( stored_histogram ); 
  }

}

void cutflow::expand_cutflow( std::vector< std::string > & extra_cuts ){

  this->cut_names.reserve( this->cut_names.size()+extra_cuts.size() );
  this->cut_names.insert( this->cut_names.end(), extra_cuts.begin(), extra_cuts.end() );
  this->cuts += (int) extra_cuts.size();
  TH1 * temp = (TH1 *) this->cutflow_hist->Clone( "temp" );
  delete this->cutflow_hist;
  this->cutflow_hist = new TH1F( "Cutflow", "", this->cuts, 0, this->cuts );
  for ( int idx = 1; idx <= this->cuts; idx++ ){
    this->cutflow_hist->SetBinContent( idx, temp->GetBinContent( idx ) );
  }
  delete temp;
  for ( int idx = cuts - extra_cuts.size(); idx < this->cuts; idx ++ ){
    cut_distributions.push_back( new TH1F( Form( "%s_cut_%i", cut_names[idx].c_str(), idx ), "", 50, -100, 100 ) );
  }

}

