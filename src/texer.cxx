#include "texer.hxx"


void texer::set_filename( std::string & filename ){
  this->filename = filename;
  if ( filename.find( ".tex" ) != std::string::npos){ this->filename += ".tex"; }
}
    
void texer::set_orientation( bool landscape ){
  this->landscape = landscape;
}

void texer::set_packages( std::vector< std::string > & packages ){
  for ( std::string & package : packages ){ this->packages.push_back( Form( "\\usepackage{ %s }", package.c_str() ) ); } 
}
void texer::set_packages( std::vector< std::string > && packages ){
  for ( std::string & package : packages ){ this->packages.push_back( Form( "\\usepackage{ %s }", package.c_str() ) ); } 
}

void texer::initialise(){
  if ( filename.empty() ){ 
    std::cout << "No filename, creating 'file.tex'." << std::endl;
    this->filename = "file";
  } 
  output_stream.open( filename );
  if ( packages.size() == 0 ){ std::cout << "No packages specified." << std::endl; }
  lines.push_back( "\\documentclass[]{ report }" );
  for ( std::string & line : this->packages ){ lines.push_back( line ); } 
  lines.push_back( "\\begin{ document }" );

  initialised = true; 
}

void texer::add_line( std::string & line ){
  if ( !initialised ){ std::cout << "not initialised, no line added" << std::endl; return; }
  if ( table_mode ){ std::cout << "in table mode, use 'add_row()', no line added" << std::endl; return; }
  this->lines.push_back( line );
}

void texer::add_line( std::string && line ){
  if ( !initialised ){ std::cout << "not initialised, no line added" << std::endl; return; }
  if ( table_mode ){ std::cout << "in table mode, use 'add_row()', no line added" << std::endl; return; }
  this->lines.push_back( line );
}

void texer::start_table( std::string & table_format, std::vector< std::string > & columns ){
  if ( !initialised ){ std::cout << "not initialised, no table started" << std::endl; return; }
  table_mode = true;
  this->lines.push_back( "\\begin{ figure }[h!]" );
  this->lines.push_back( "\\centering" );
  this->lines.push_back( Form( "\\begin{ tabular }{ %s } \n\\hline\n", table_format.c_str() ) );
  std::string column_string;
  for ( std::string & column : columns ){
    column_string += column;  
    column_string += " & ";
  }
  column_string = column_string.substr( 0, column_string.size() - 2 );
  column_string += "\\\\\n\\hline\n\\hline\n";
  this->lines.push_back( column_string );   
}

void texer::add_row( std::string & row_string ){
  if ( !initialised ){ std::cout << "not initialised, no table row added" << std::endl; return; }
  if ( table_mode ){ std::cout << "in table mode, use 'add_row()', no row added" << std::endl; return; }
  this->lines.push_back( row_string + " \\\\\n\\hline\n" );
}

void texer::end_table(){
  if ( !initialised ){ std::cout << "not initialised, no table ended" << std::endl; return; }
  this->lines.push_back( "\\end{ tabular }" );
  this->lines.push_back( "\\end{ figure }" );
  table_mode = false;
}


void texer::add_table( std::string & table_format, std::string & entry_format, std::vector< TH1 * > & columns ){

  if ( !initialised ){ std::cout << "not initialised, no table added" << std::endl; return; }
  std::vector< std::string > entry_format_vec;
  split_strings( entry_format_vec, entry_format, ":" );
  this->lines.push_back( "\\begin{ figure }[h!]" );
  this->lines.push_back( "\\centering" );
  this->lines.push_back( Form( "\\begin{ tabular }{ %s } \n\\hline\n", table_format.c_str() ) );
  auto fold_columns = []( std::string & first, TH1 * second ){  return std::string( first + " & " + second->GetName() ); };
  std::string starter = std::string( columns.at( 0 )->GetName() );
  this->lines.push_back( std::string( std::accumulate( columns.begin(), columns.end(), starter, fold_columns ) ) + " \\\\\n\\hline\n" );
  auto fold_line = [ &entry_format_vec ]( int & row, TH1 * hist ){ return Form( std::string( " & " + entry_format_vec.at( row-1 ) ).c_str(), hist->GetBinContent( row ) ); };
  for ( int row = 1; row < columns.at( 0 )->GetNbinsX(); row++ ){
    std::string current_line = std::to_string( row );
    for ( TH1 * & hist : columns ){ 
      current_line += fold_line( row, hist );
    }
    current_line += " \\\\\n\\hline\n";
    this->lines.push_back( current_line );
  }
}

void texer::add_table( std::string & table_format, std::string & entry_format, std::vector< std::vector< double > > & values, std::vector< std::string> & columns ){

  if ( !initialised ){ std::cout << "not initialised, no table added" << std::endl; return; }
  std::vector< std::string > entry_format_vec;
  split_strings( entry_format_vec, entry_format, ":" );

  this->lines.push_back( "\\begin{ figure }[h!]" );
  this->lines.push_back( "\\centering" );
  this->lines.push_back( Form( "\\begin{ tabular }{ %s } \n\\hline\n", table_format.c_str() ) );
  auto fold_columns = []( std::string & first, std::string & second ){  return ( first + " & " + second ); };
  this->lines.push_back( std::string( std::accumulate( columns.begin(), columns.end(), columns.at(0), fold_columns) ) + " \\\\\n\\hline\n" );

  auto fold_line = [ &entry_format_vec ]( size_t & row, std::vector< double > & column ){ return Form( std::string( std::string(" & ") + entry_format_vec.at( row-1 ) ).c_str(), column.at( row ) ); };
  for ( size_t row = 1; row < values.at( 0 ).size(); row++ ){
    std::string current_line = std::to_string( row );
    for ( std::vector< double > & column : values ){ 
      std::string thing = fold_line( row, column );
    }
    current_line += " \\\\\n\\hline\n";
    this->lines.push_back( current_line );

  }
}



void texer::write( bool reset ){

  for ( std::string & line : lines ){
    output_stream << line; 
  }
  output_stream << "\\end{ document }";

  if ( reset ){ clear(); }
}

void texer::clear(){
  if ( output_stream.is_open() ){ output_stream.close(); }
  landscape = false;
  initialised = false;
  table_mode = false;
  filename = "";
  lines.clear();
  packages.clear();
}
