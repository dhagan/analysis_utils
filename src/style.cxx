#include "style.hxx"

// prepare root style
void prep_style() {

  gROOT->SetBatch(true);

  gStyle->SetPaperSize( 20, 26 );

  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetOptTitle(0);
  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);
  gStyle->SetEndErrorSize( 5 );
  gStyle->SetErrorX( 0 );

  Int_t icol = 0;
  gStyle->SetFrameBorderMode(icol);
  gStyle->SetFrameFillColor(icol);
  gStyle->SetCanvasBorderMode(icol);
  gStyle->SetCanvasColor(icol);
  gStyle->SetPadBorderMode(icol);
  gStyle->SetPadColor(icol);
  gStyle->SetStatColor(icol);

  // set title offsets (for axis label)
  gStyle->SetTitleXOffset(1.3);
  gStyle->SetTitleYOffset(1.3);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t text_size = 0.035;
  gStyle->SetTextFont( font );
  gStyle->SetTextSize( text_size );
  gStyle->SetLabelFont( font,"x" );
  gStyle->SetTitleFont( font,"x" );
  gStyle->SetLabelFont( font,"y" );
  gStyle->SetTitleFont( font,"y" );
  gStyle->SetLabelFont( font,"z" );
  gStyle->SetTitleFont( font,"z" );
  gStyle->SetLabelSize( text_size,"x" );
  gStyle->SetTitleSize( text_size,"x" );
  gStyle->SetLabelSize( text_size,"y" );
  gStyle->SetTitleSize( text_size,"y" );
  gStyle->SetLabelSize( text_size,"z" );
  gStyle->SetTitleSize( text_size,"z" );

  // use bold lines and markers
  //atlasStyle->SetMarkerStyle( 20 );
  //atlasStyle->SetMarkerSize( 1 );
  gStyle->SetLineStyleString( 2,"[12 12]"); 

  // put tick marks on top and RHS of plots
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  gROOT->ForceStyle();

}


void style_mgr::load_styles(){
  if ( style_mgr_filename.empty() ){
    this->style_mgr_filename = std::getenv( "DEFAULT_STYLE_JSON" );
    std::cout << "No style json provided, selecting default for input: " << style_mgr_filename << ".json" << std::endl;
  }
  std::ifstream input_file_stream;

  input_file_stream.open( style_mgr_filename + ".json" );
  input_file_stream >> style_json;
  input_file_stream.close();
}

void style_mgr::write_styles(){

  std::ofstream output_file_stream;
  if ( style_mgr_filename.empty() ){
    this->style_mgr_filename = std::getenv( "DEFAULT_STYLE_JSON" );
    std::cout << "No style json provided, selecting default for output: " << style_mgr_filename << ".json" << std::endl;
  }
  output_file_stream.open( style_mgr_filename + ".json" );
  output_file_stream << std::setw( 4 ) << style_json << std::endl; 
  output_file_stream.close();

}

void style_mgr::add_style( std::string style_string, std::vector< float > & style_vector ){

  if ( style_json.contains( style_string ) ){ std::cout << "Style "<< style_string << " exists, replacing..." << std::endl; }
  if ( style_vector.size() != 11 ){ std::cout << "Incorrect length style_vector, ignoring" << std::endl; }

  style_json[ style_string ][ "name" ] = style_string;
  style_json[ style_string ][ "marker_style" ]  = style_vector.at( 0 );
  style_json[ style_string ][ "marker_color" ]  = style_vector.at( 1 );
  style_json[ style_string ][ "marker_alpha" ]  = style_vector.at( 2 );
  style_json[ style_string ][ "marker_size" ]   = style_vector.at( 3 );

  style_json[ style_string ][ "line_style" ]    = style_vector.at( 4 );
  style_json[ style_string ][ "line_color" ]    = style_vector.at( 5 );
  style_json[ style_string ][ "line_alpha" ]    = style_vector.at( 6 );
  style_json[ style_string ][ "line_width" ]    = style_vector.at( 7 );

  style_json[ style_string ][ "fill_color" ]    = style_vector.at( 8 );
  style_json[ style_string ][ "fill_alpha" ]    = style_vector.at( 9 );
  style_json[ style_string ][ "fill_style" ]    = style_vector.at( 10 );
}

void style_mgr::add_style( std::string style_string, TH1 * histogram, float fill_alpha, float marker_alpha, float line_alpha ){

  if ( style_json.contains( style_string ) ){ std::cout << "Style " << style_string << " exists, replacing..." << std::endl; }
  style_json[ style_string][ "name" ] = style_string;
  style_json[ style_string ][ "marker_style" ] = histogram->GetMarkerStyle();
  style_json[ style_string ][ "marker_color" ] = histogram->GetMarkerColor();
  style_json[ style_string ][ "marker_alpha" ] = marker_alpha;
  style_json[ style_string ][ "marker_size" ] = histogram->GetMarkerSize();

  style_json[ style_string ][ "line_style" ] = histogram->GetLineStyle();
  style_json[ style_string ][ "line_color" ] = histogram->GetLineColor();
  style_json[ style_string ][ "line_alpha" ] = line_alpha;
  style_json[ style_string ][ "line_width" ] = histogram->GetLineWidth();

  style_json[ style_string ][ "fill_style" ] = histogram->GetFillStyle();
  style_json[ style_string ][ "fill_color" ] = histogram->GetFillColor();
  style_json[ style_string ][ "fill_alpha" ] = fill_alpha;

}

std::vector< float > style_mgr::get_style_vector( std::string style_string, bool prefix ){

  if ( prefix && !this->stage_prefix.empty() ){ style_string = stage_prefix + "_" + style_string; }
  if ( !style_json.contains( style_string ) ){
    std::cout << "no style \"" << style_string << "\"  found, applying default" << std::endl;
    style_string = "default";
  }
  std::vector<float> style_vector;
  style_vector.reserve( 11 );

  style_vector.at( 0 ) = this->style_json[ style_string ][ "marker_style" ];
  style_vector.at( 1 ) = this->style_json[ style_string ][ "marker_color" ];
  style_vector.at( 2 ) = this->style_json[ style_string ][ "marker_alpha" ];
  style_vector.at( 3 ) = this->style_json[ style_string ][ "marker_size" ];
  style_vector.at( 4 ) = this->style_json[ style_string ][ "line_style" ];
  style_vector.at( 5 ) = this->style_json[ style_string ][ "line_color" ];
  style_vector.at( 6 ) = this->style_json[ style_string ][ "line_alpha" ];
  style_vector.at( 7 ) = this->style_json[ style_string ][ "line_width" ];
  style_vector.at( 8 ) = this->style_json[ style_string ][ "fill_color" ];
  style_vector.at( 9 ) = this->style_json[ style_string ][ "fill_alpha" ];
  style_vector.at( 10 )  = this->style_json[ style_string ][ "fill_style" ];

  return style_vector;
}

void style_mgr::set_stage( std::string stage_prefix ){
  this->stage_prefix = stage_prefix;
}

void style_mgr::style_histogram( TH1 * histogram, std::string style_string, bool prefix ){

  if ( prefix && !this->stage_prefix.empty() ){ style_string = stage_prefix + "_" + style_string; }

  if ( !style_json.contains( style_string ) ){
    std::cout << "no style \"" << style_string << "\"  found, applying default" << std::endl;
    style_string = "default";
  }

  nlohmann::json selected_style = style_json[ style_string ];

  if ( selected_style.contains( "set" ) ){
    if ( selected_style[ "set" ].get<bool>() ){
      std::cout << "Warning: Attempted to invoke a style set on a single histogram." << std::endl;
      std::cout << "         Either select a regular style, or extract a style_set" << std::endl;
      std::cout << "         object to use. Returning unchanged." << std::endl;
      return;
    }
  }

  histogram->SetMarkerStyle( selected_style[ "marker_style" ].get<int>() );
  histogram->SetLineStyle( selected_style[ "line_style" ].get<int>() );
  histogram->SetFillStyle( selected_style[ "fill_style" ].get<int>() );
  histogram->SetMarkerColorAlpha( selected_style[ "marker_color" ].get< float >(), selected_style[ "marker_alpha" ].get< float >() );
  histogram->SetLineColorAlpha( selected_style[ "line_color" ].get< float >(), selected_style[ "line_alpha" ].get< float >() );
  histogram->SetFillColorAlpha( selected_style[ "fill_color" ].get< float >(), selected_style[ "fill_alpha" ].get< float >() );
  histogram->SetLineWidth( selected_style[ "line_width" ].get< float >() );
  histogram->SetMarkerSize( selected_style[ "marker_size" ].get< float >() );

}

void style_mgr::style_function( TF1 * function, std::string style_string, bool prefix ){

  if ( prefix && !this->stage_prefix.empty() ){ style_string = stage_prefix + "_" + style_string; }

  if ( !style_json.contains( style_string ) ){
    std::cout << "no style \"" << style_string << "\" found, applying default" << std::endl;
    style_string = "default";
  }

  nlohmann::json selected_style = style_json[ style_string ];

  function->SetMarkerStyle( selected_style[ "marker_style" ].get<int>() );
  function->SetLineStyle( selected_style[ "line_style" ].get<int>() );
  function->SetFillStyle( selected_style[ "fill_style" ].get<int>() );

  function->SetMarkerColorAlpha( selected_style[ "marker_color" ].get< float >(), selected_style[ "marker_alpha" ].get< float >() );
  function->SetLineColorAlpha( selected_style[ "line_color" ].get< float >(), selected_style[ "line_alpha" ].get< float >() );
  function->SetFillColorAlpha( selected_style[ "fill_color" ].get< float >(), selected_style[ "fill_alpha" ].get< float >() );

  function->SetLineWidth( selected_style[ "line_width" ].get< float >() );
  function->SetMarkerSize( selected_style[ "marker_size" ].get< float >() );

}

style_set style_mgr::get_styleset( std::string style_string, bool prefix ){

  if ( prefix && !this->stage_prefix.empty() ){ style_string = stage_prefix + "_" + style_string; }

  if ( !style_json.contains( style_string ) ){
    std::cout << "WARNING: no style set \"" << style_string << "\"  found, returning empty" << std::endl;
    return style_set();
  }

  nlohmann::json selected_style = style_json[ style_string ];
  if ( !selected_style.contains( "set" ) ){ 
    std::cout << "Warning: style \"" << style_string << "\" not marked as style set, returning empty" << std::endl;
    return style_set();
  }

  if ( !selected_style.contains( "set" ) && selected_style[ "set" ].get<bool>() ){
    std::cout << "Warning: style \"" << style_string << "\" not marked as style set, returning empty" << std::endl;
    return style_set();
  }

  style_set set = style_set();
  set.progressive = selected_style[ "progressive" ].get<std::vector< bool > >();
  set.predefined_progression = selected_style[ "predefined_progression" ].get<std::vector< bool > >();
  set.step_size = selected_style[ "step_size" ].get<std::vector<float>>();
  set.step_threshold = selected_style[ "step_threshold" ].get<std::vector<int>>();
  set.initial_style_vector = this->get_style_vector(selected_style[ "initial_style" ].get<std::string>());
  set.current_style_vector = set.initial_style_vector;
  set.predefined_values = selected_style[ "predefined_values" ];

  return set;
}
