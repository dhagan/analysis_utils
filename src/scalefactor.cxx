#include <scalefactor.hxx>


int scalefactor::get_bin(double pt, double eta ) const {
  double abseta = fabs(eta); 
  if (pt <= m_ptmin) pt = m_ptmin + 0.1;
  if (pt >=  m_ptmax) pt = m_ptmax - 0.1;

  if (abseta <= m_etamin) abseta =  m_etamin + 0.01;
  if (abseta >=  m_etamax) abseta =  m_etamax - 0.01;

  if ( abseta >= m_crackmin && abseta <= m_crackmax ){
    //std::cerr << "In crack" << std::endl;
    abseta = m_crackmin - 0.01;     
  }

  int bin = m_h_id_tight_uncov->FindBin( pt, abseta );
  return bin;

}

double scalefactor::bounds_bloom( double pt, double eta ) const {

  if (pt <= m_bloom_ptmin || pt >=  m_bloom_ptmax ){ return m_error_bloom; }
  double abseta = fabs(eta); 
  if ( abseta <= m_bloom_etamin || abseta >=  m_bloom_etamax ){ return m_error_bloom; }
  if ( abseta >= m_crackmin && abseta <= m_crackmax ){ return m_error_bloom; }
  return 1.0;

}

double scalefactor::get_scaling( double pt ) const {
  if ( pt < m_bloom_ptmin ){ return ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling; } 
  else return 1.0;
}

double scalefactor::get_central_scaling( double pt ) const {
  if ( pt < m_bloom_ptmin ){ return ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_central_scaling; } 
  else return 1.0;
}

void scalefactor::set_linear_scaling( double scaling ){ m_error_scaling = scaling; }
void scalefactor::set_central_linear_scaling( double scaling ){ m_central_scaling = scaling; }

double scalefactor::photon_SF( double pt, double eta ) const { return m_h_id_tight_uncov->GetBinContent( get_bin(pt, eta) ); }

double scalefactor::photon_SF_upper( double pt, double eta ) const {
  double bloom_value = bounds_bloom( pt, eta );
  int bin = get_bin(pt, eta);
  return ( m_h_id_tight_uncov->GetBinContent(bin) + ( m_h_id_tight_uncov->GetBinError( bin ) * bloom_value ) );
}

double scalefactor::photon_SF_lower( double pt, double eta ) const {
  double bloom_value = bounds_bloom( pt, eta );
  int bin = get_bin(pt, eta);
  return ( m_h_id_tight_uncov->GetBinContent(bin) - ( m_h_id_tight_uncov->GetBinError( bin ) * bloom_value ) );
}

double scalefactor::photon_SF_uncertainty( double pt, double eta ) const {
  double bloom_value = bounds_bloom( pt, eta );
  int bin = get_bin( pt, eta );
  return m_h_id_tight_uncov->GetBinError( bin ) * bloom_value;
}

double scalefactor::photon_SF_linear( double pt, double eta ) const {
  int bin = get_bin(pt, eta);
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value -= ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_central_scaling ); } 
  return m_h_id_tight_uncov->GetBinContent(bin)*scale_value;
}

double scalefactor::photon_SF_upper_linear( double pt, double eta ) const {
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value = ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling ) + 1.0; } 
  int bin = get_bin(pt, eta);
  return ( m_h_id_tight_uncov->GetBinContent( bin ) + ( m_h_id_tight_uncov->GetBinError( bin ) * scale_value ) );
}

double scalefactor::photon_SF_lower_linear( double pt, double eta ) const {
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value = ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling ) + 1.0; } 
  int bin = get_bin(pt, eta);
  return ( m_h_id_tight_uncov->GetBinContent( bin ) - ( m_h_id_tight_uncov->GetBinError( bin ) * scale_value ) );
}

double scalefactor::photon_SF_upper_linear_central( double pt, double eta ) const {
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value = ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling ) + 1.0; } 
  int bin = get_bin(pt, eta);
  return ( photon_SF_linear( pt, eta ) + ( m_h_id_tight_uncov->GetBinError( bin ) * scale_value ) );
}

double scalefactor::photon_SF_lower_linear_central( double pt, double eta ) const {
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value = ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling ) + 1.0; } 
  int bin = get_bin(pt, eta);
  return ( photon_SF_linear( pt, eta ) - ( m_h_id_tight_uncov->GetBinError( bin ) * scale_value ) );
}

double scalefactor::photon_SF_uncertainty_linear( double pt, double eta ) const {
  double scale_value = 1.0;
  if ( pt < m_bloom_ptmin ){ scale_value = ( ( (m_bloom_ptmin - pt ) / 1.0e3 ) * m_error_scaling ) + 1.0; } 
  int bin = get_bin( pt, eta );
  return m_h_id_tight_uncov->GetBinError( bin ) * scale_value;
}

void scalefactor::load_photon_SF( const std::string & photon_efficiency_sf_path ){
  const char * sf_location = std::getenv( "SF_PATH" ); 
  if ( photon_efficiency_sf_path.empty() ){
    m_fii_id_tight_uncov = TFile::Open( Form( "%s/Summer2020_Rec_v1/efficiencySF.offline.Tight.13TeV.rel21.25ns.unc.v01.root", sf_location ), "READ" );
  } else {
    m_fii_id_tight_uncov = TFile::Open( photon_efficiency_sf_path.c_str(), "READ" );
  }
  m_h_id_tight_uncov   =  dynamic_cast<TH2*>(m_fii_id_tight_uncov->Get("0_99999999999/FullSim_sf"));
}

void scalefactor::load_muon_SF( const std::string & singleMuEffFileName, const std::string & drCorFileName ){

  m_singleMuEffFileName = singleMuEffFileName;
  m_drCorFileName = drCorFileName;

  const char * prefix = std::getenv( "SF_PATH" );

  TFile* singleMuEffFile = new TFile( Form( "%s/%s", prefix, m_singleMuEffFileName.c_str() ),"READ" );
  TFile* drCorFile = new TFile( Form( "%s/%s", prefix, m_drCorFileName.c_str() ), "READ" );
  
  // Initialize single mu SF map
  std::vector<dataset> datasets = { dataset::Data15, dataset::Data16 };
  std::vector<single_mu_leg> mulegs = { single_mu_leg::mu4, single_mu_leg::mu6 };
  for ( auto dataset : datasets ){
    for ( auto muleg : mulegs ){
      char error_name[150];
      const char * hist_name = Form( "SF_period%sAllYear_%s",dataset_c[dataset], single_mu_leg_c[muleg] );
      sprintf( error_name, "%s_stat", hist_name );
      if( !(TH2F*)( singleMuEffFile->Get( hist_name ) ) ){
        std::cout << "SingleMu SF histogram " << hist_name << " cannot be found in " << singleMuEffFile << std::endl;
      } else {
        singleMuSfMap[dataset][muleg] = (TH2F*)( singleMuEffFile->Get( hist_name ));
        singleMuSfErrMap[dataset][muleg] = (TH2F*)( singleMuEffFile->Get( error_name ));
      }
    }
  }
  
  // Initialize dimu dR correlation map
  datasets = { dataset::Data15, dataset::Data16, dataset::MC16a };
  std::vector< dimu_trig > dimutrigs = { dimu_trig::bDimu, dimu_trig::bDimu_noL2 };
  std::vector< eta_range > etaranges = { eta_range::Barrel, eta_range::Overlap, eta_range::EndCap };

  for( auto dataset : datasets) {
    for( auto dimutrig : dimutrigs) {
      for( auto etarange : etaranges) {

        const char * hist_name = Form( "%s_%s_%s", dataset_c[dataset], dimu_trig_c[dimutrig], eta_range_c[etarange] );
        const char * err_name = Form( "%s_err", hist_name );
        if( dataset == dataset::Data16 && dimutrig == dimu_trig::bDimu_noL2 ){
          continue; // no such triggers in 2016+
        }
        if( !(TF1*)( drCorFile->Get( hist_name ) ) ){
          std::cout << "Dimu dR correlation histogram " << hist_name << " cannot be found in " << drCorFile << std::endl;
        } else {
          dimuDrCorMap[dataset][dimutrig][etarange] = (TF1 *) (drCorFile->Get( hist_name ));
          dimuDrCorMap_err[dataset][dimutrig][etarange] = (TH1F *) (drCorFile->Get( err_name ));
        }
      }
    }
  }
}

void scalefactor::set_bounds_error_bloom( double bloom ){
  m_error_bloom = bloom;
}

void scalefactor::set_bloom_bounds( double pt_min, double pt_max, double eta_min, double eta_max ){
  m_bloom_ptmin = pt_min;
  m_bloom_ptmax = pt_max;
  m_bloom_etamin = eta_min;
  m_bloom_etamax = eta_max;
}

double scalefactor::single_muon_SF( double pT, double qEta, dataset datasetData, single_mu_leg muleg ){
  TH2F* hist = singleMuSfMap[datasetData][muleg];
  if( !hist ) {
    std::cout << "No singleMuLeg SF hist for " << dataset_c[datasetData] << ", " << single_mu_leg_c[muleg] << std::endl;
    return 1.;
  }
  if(fabs(qEta) > 2.4) {
    std::cout << "eta(mu) = " << fabs(qEta) << " is beyond 2.4, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT < 4.0) {
    std::cout << "pT(mu) = " << pT << " is below 4 GeV, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT > 40.) {
    pT = 39.9;
  }
  int ibin = hist->FindBin( pT,qEta );
  double sf = hist->GetBinContent( ibin );
  return sf;
}

// not this way, the error is huge
double scalefactor::single_muon_SF_error( double pT, double qEta, dataset datasetData, single_mu_leg muleg ){
  TH2F* hist = singleMuSfErrMap[datasetData][muleg];
  if( !hist ) {
    std::cout << "No singleMuLeg SF hist for " << dataset_c[datasetData] << ", " << single_mu_leg_c[muleg] << std::endl;
    return 1.;
  }
  if(fabs(qEta) > 2.4) {
    std::cout << "eta(mu) = " << fabs(qEta) << " is beyond 2.4, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT < 4.0) {
    std::cout << "pT(mu) = " << pT << " is below 4 GeV, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT > 40.) {
    pT = 39.9;
  }
  int ibin = hist->FindBin( pT,qEta );
  double sf_err = hist->GetBinContent( ibin );
  return sf_err;
}

double scalefactor::muon_dr_corr( double dR, double y, dataset dataset, dimu_trig dimutrig ) {
  eta_range etarange;
  if(fabs(y) > 2.3) {
    std::cout << "y(dimu) = " << y << " is beyond 2.3, will returt c_dn for 1.2--2.3 range." << std::endl;
    etarange = eta_range::EndCap;
  }
  else if(fabs(y) > 1.2 && fabs(y) <= 2.3) {
    etarange = eta_range::EndCap;
  }
  else if(fabs(y) > 1.0 && fabs(y) <= 1.2) {
    etarange = eta_range::Overlap;
  } else {
    etarange = eta_range::Barrel;
  }
  TF1 * erfun = dimuDrCorMap[dataset][dimutrig][etarange];
  double drcor = erfun->Eval(dR);
  double unphysNorm = erfun->GetParameter(2);
  drcor /= unphysNorm;
  return drcor;
}

double scalefactor::muon_dr_corr_error( double dR, double y, dataset dataset, dimu_trig dimutrig ) {
  eta_range etarange;
  if(fabs(y) > 2.3) {
    std::cout << "y(dimu) = " << y << " is beyond 2.3, will returt c_dn for 1.2--2.3 range." << std::endl;
    etarange = eta_range::EndCap;
  }
  else if(fabs(y) > 1.2 && fabs(y) <= 2.3) {
    etarange = eta_range::EndCap;
  }
  else if(fabs(y) > 1.0 && fabs(y) <= 1.2) {
    etarange = eta_range::Overlap;
  } else {
    etarange = eta_range::Barrel;
  }
  TF1 * erfun = dimuDrCorMap[dataset][dimutrig][etarange];
  TH1F * err = dimuDrCorMap_err[dataset][dimutrig][etarange];
  double drcor_err = err->GetBinError( err->GetBin( dR ) );
  //double drcor = erfun->Eval(dR);
  double unphysNorm = erfun->GetParameter(2);
  drcor_err /= unphysNorm;
  return drcor_err;
}



double scalefactor::muon_dr_SF( double dR, double y, dataset ds, dimu_trig dimutrig) {
  
  dataset dataset_data = ds;
  dataset dataset_mc;
  
  if( dataset_data == dataset::Data15 || dataset_data == dataset::Data16 ){
    dataset_mc = dataset::MC16a;
  } else if( dataset_data == dataset::Data17 ){
    dataset_mc = dataset::MC16d;
  } else if( dataset_data == dataset::Data18 ){
    dataset_mc = dataset::MC16d;
  } else {
    std::cout << "Dataset argument should be for data; will return SF = 1." << std::endl;
    return 1.;
  }

  double sf = muon_dr_corr( dR, y, dataset_data, dimutrig )/muon_dr_corr( dR, y, dataset_mc, dimutrig );
  return sf;

}


TH1F * scalefactor::error_muon_SF( TTree * predefined_tree, variable_set & variables, const std::string & variable, const std::string & mass ){

  const char * mass_c = ( mass.empty() ) ? "Q12" : mass.c_str();

  bound & variable_bound = variables.bound_manager->get_bound( variable );
  std::string mass_cut = variables.bound_manager->get_bound( mass_c ).get_cut();
  const char * mass_cut_c = mass_cut.c_str();

  std::vector< std::string > cut_series = variable_bound.get_cut_series();

  TH1F * sf_mean = variable_bound.get_hist( "sf_mean" );
  TH1F * sf_rms = variable_bound.get_hist( "sf_rms" );
  TH1F * sf_err = variable_bound.get_hist( "sf_err" );
    
  TH1F * temp_hist = variable_bound.get_hist( "temp" );
  for ( int bin = 1; bin < variable_bound.get_bins(); bin++ ){

    predefined_tree->Draw( Form( "SF>>temp" ), Form( "%s&&%s", mass_cut_c, cut_series[bin].c_str() ), "goff" );
    sf_mean->SetBinContent( bin, temp_hist->GetMean() );
    sf_rms->SetBinContent( bin, temp_hist->GetRMS() );
    temp_hist->Reset();

  }
  sf_err->Divide( sf_rms, sf_mean, 1.0, 1.0 );

  delete temp_hist;
  return sf_err;

}

TH1F * scalefactor::error_photon_SF( TTree * predefined_tree, variable_set & variables, const std::string & variable, const std::string & mass ){

  const char * mass_c = ( mass.empty() ) ? "Q12" : mass.c_str();

  bound & variable_bound = variables.bound_manager->get_bound( variable );
  std::string mass_cut = variables.bound_manager->get_bound( mass_c ).get_cut();
  const char * mass_cut_c = mass_cut.c_str();

  std::vector< std::string > cut_series = variable_bound.get_cut_series();

  TH1F * sf_mean = variable_bound.get_hist( "sf_mean" );
  TH1F * sf_rms = variable_bound.get_hist( "sf_rms" );
  TH1F * sf_err = variable_bound.get_hist( "sf_err" );
    
  TH1F * temp_hist = variable_bound.get_hist( "temp" );
  for ( int bin = 1; bin < variable_bound.get_bins(); bin++ ){

    predefined_tree->Draw( Form( "photon_id_sf>>temp" ), Form( "%s&&%s", mass_cut_c, cut_series[bin].c_str() ), "goff" );
    sf_mean->SetBinContent( bin, temp_hist->GetMean() );
    sf_rms->SetBinContent( bin, temp_hist->GetRMS() );
    temp_hist->Reset();

  }
  sf_err->Divide( sf_rms, sf_mean, 1.0, 1.0 );

  delete temp_hist;
  return sf_err;

}






